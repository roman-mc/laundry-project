-- phpMyAdmin SQL Dump
-- version 4.7.2
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Ноя 01 2017 г., 12:58
-- Версия сервера: 5.7.16-0ubuntu0.16.04.1-log
-- Версия PHP: 7.0.23-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `getairo`
--

-- --------------------------------------------------------

--
-- Структура таблицы `a-table-for-procedures`
--

CREATE TABLE `a-table-for-procedures` (
  `date` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Клиент` bigint(21) DEFAULT NULL,
  `Курьер` bigint(21) DEFAULT NULL,
  `Химчистка` bigint(21) DEFAULT NULL,
  `Прочее` bigint(21) DEFAULT NULL,
  `Общий итог` bigint(21) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

-- --------------------------------------------------------

--
-- Структура таблицы `account_target`
--

CREATE TABLE `account_target` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL COMMENT 'название цели для удобства поиска',
  `debet_goal_id` int(11) DEFAULT NULL COMMENT 'цель для дебетовых операций',
  `debet_category_id` int(11) DEFAULT NULL COMMENT 'категория для дебетовых операций',
  `debet_subgoal_id` int(11) DEFAULT NULL COMMENT 'подцель для дебетовых операций',
  `credit_goal_id` int(11) DEFAULT NULL COMMENT 'цель для кредитных операций',
  `credit_category_id` int(11) DEFAULT NULL COMMENT 'категория для кредитных операций',
  `credit_subgoal_id` int(11) DEFAULT NULL COMMENT 'цель для кредитных операций'
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COMMENT='набор целей для счета';

-- --------------------------------------------------------

--
-- Структура таблицы `address_book`
--

CREATE TABLE `address_book` (
  `id` int(11) NOT NULL,
  `client_id` int(11) DEFAULT NULL,
  `address` text,
  `comment` text,
  `date_` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint(4) DEFAULT '0',
  `details` text,
  `lng` float DEFAULT NULL,
  `lat` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `address_book_old`
--

CREATE TABLE `address_book_old` (
  `id` int(11) NOT NULL,
  `client_id` int(11) DEFAULT NULL,
  `address` text,
  `comment` text,
  `collection` tinyint(1) DEFAULT NULL,
  `createdon` int(11) DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `date_` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint(4) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `api_ats_order_status`
--

CREATE TABLE `api_ats_order_status` (
  `id` int(10) NOT NULL,
  `order_id` int(10) DEFAULT NULL,
  `order_status` tinyint(1) DEFAULT NULL,
  `order_start_date` datetime DEFAULT NULL,
  `order_done_date` datetime DEFAULT NULL,
  `carrier` int(10) DEFAULT NULL,
  `carrier_name` varchar(50) DEFAULT NULL,
  `carrier_phone` varchar(20) DEFAULT NULL,
  `carrier_start_pos` text,
  `carrier_done_pos` text,
  `carrier_now_pos` text,
  `carrier_list_pos` mediumtext,
  `insert_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AVG_ROW_LENGTH=1318 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `article_attachments`
--

CREATE TABLE `article_attachments` (
  `id` int(11) NOT NULL,
  `itemid` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `titleAttribute` text,
  `hits` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `article_categories`
--

CREATE TABLE `article_categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `description` text,
  `parentid` int(11) NOT NULL DEFAULT '0',
  `state` tinyint(1) NOT NULL DEFAULT '0',
  `access` varchar(64) NOT NULL,
  `language` char(7) NOT NULL,
  `theme` varchar(12) NOT NULL DEFAULT 'blog',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `image` text,
  `image_caption` varchar(255) DEFAULT NULL,
  `image_credits` varchar(255) DEFAULT NULL,
  `params` text,
  `metadesc` text,
  `metakey` text,
  `robots` varchar(20) DEFAULT NULL,
  `author` varchar(50) DEFAULT NULL,
  `copyright` varchar(50) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=8192 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `article_items`
--

CREATE TABLE `article_items` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `catid` int(11) NOT NULL DEFAULT '0',
  `userid` int(11) NOT NULL DEFAULT '0',
  `introtext` text,
  `fulltext` text,
  `state` tinyint(1) NOT NULL DEFAULT '0',
  `access` varchar(64) NOT NULL,
  `language` char(7) NOT NULL,
  `theme` varchar(12) NOT NULL DEFAULT 'blog',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `hits` int(11) NOT NULL DEFAULT '0',
  `image` text,
  `image_caption` varchar(255) DEFAULT NULL,
  `image_credits` varchar(255) DEFAULT NULL,
  `video` text,
  `video_type` varchar(20) DEFAULT NULL,
  `video_caption` varchar(255) DEFAULT NULL,
  `video_credits` varchar(255) DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) NOT NULL DEFAULT '0',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(11) NOT NULL DEFAULT '0',
  `params` text,
  `metadesc` text,
  `metakey` text,
  `robots` varchar(20) DEFAULT NULL,
  `author` varchar(50) DEFAULT NULL,
  `copyright` varchar(50) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=8192 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `ats_courier_schedule`
--

CREATE TABLE `ats_courier_schedule` (
  `courier_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `shift_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

-- --------------------------------------------------------

--
-- Структура таблицы `ats_data`
--

CREATE TABLE `ats_data` (
  `id` bigint(20) NOT NULL,
  `status_id` int(11) NOT NULL,
  `courier_id` int(11) NOT NULL,
  `ats_drive` int(11) DEFAULT NULL,
  `start` int(11) DEFAULT NULL,
  `done` int(11) DEFAULT NULL,
  `shift` int(11) DEFAULT NULL,
  `done_distance` double DEFAULT NULL,
  `done_accuracy` double DEFAULT NULL,
  `mileage` double DEFAULT NULL,
  `gas` double DEFAULT NULL,
  `gas_price` double DEFAULT NULL,
  `drycleaning` smallint(1) DEFAULT NULL,
  `cargo_list` text COLLATE utf8_unicode_ci,
  `payed` text COLLATE utf8_unicode_ci,
  `driver_region` int(11) DEFAULT NULL,
  `drive_id` int(11) DEFAULT NULL,
  `unique_key` bigint(20) NOT NULL,
  `date` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `auth_assignment`
--

CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=16384 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `auth_item`
--

CREATE TABLE `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=744 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `auth_item_child`
--

CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=496 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `auth_rule`
--

CREATE TABLE `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `calc_calls`
--

CREATE TABLE `calc_calls` (
  `list_id` int(11) DEFAULT NULL,
  `phone` bigint(20) DEFAULT NULL,
  `phone_sms` bigint(20) DEFAULT NULL,
  `caller` varchar(50) DEFAULT NULL,
  `should_call` bit(1) DEFAULT NULL,
  `why_not_call` varchar(50) DEFAULT NULL,
  `call_result` varchar(50) DEFAULT NULL,
  `fail_reason` varchar(50) DEFAULT NULL,
  `reject_reason` varchar(50) DEFAULT NULL,
  `opinion` varchar(50) DEFAULT NULL,
  `comm` varchar(2000) DEFAULT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

-- --------------------------------------------------------

--
-- Структура таблицы `calc_calls2`
--

CREATE TABLE `calc_calls2` (
  `list_id` int(11) DEFAULT NULL,
  `phone` bigint(20) DEFAULT NULL,
  `phone_sms` bigint(20) DEFAULT NULL,
  `caller` varchar(50) DEFAULT NULL,
  `should_call` bit(1) DEFAULT NULL,
  `why_not_call` varchar(50) DEFAULT NULL,
  `call_result` varchar(50) DEFAULT NULL,
  `fail_reason` varchar(50) DEFAULT NULL,
  `reject_reason` varchar(50) DEFAULT NULL,
  `opinion` varchar(50) DEFAULT NULL,
  `comm` varchar(2000) DEFAULT NULL,
  `id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

-- --------------------------------------------------------

--
-- Структура таблицы `calc_clients`
--

CREATE TABLE `calc_clients` (
  `client_id` bigint(20) DEFAULT NULL,
  `client_orig_id` int(11) NOT NULL,
  `client_name` char(100) DEFAULT NULL,
  `first_order_date` date DEFAULT NULL,
  `first_order_id` int(11) DEFAULT NULL,
  `cohort` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

-- --------------------------------------------------------

--
-- Структура таблицы `calc_cohorts`
--

CREATE TABLE `calc_cohorts` (
  `cohort` int(11) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `ord0` int(11) DEFAULT NULL,
  `ord1` int(11) DEFAULT NULL,
  `ord2` int(11) DEFAULT NULL,
  `ord3` int(11) DEFAULT NULL,
  `ord4` int(11) DEFAULT NULL,
  `ord5` int(11) DEFAULT NULL,
  `ord6` int(11) DEFAULT NULL,
  `ret0` int(11) DEFAULT NULL,
  `ret1` int(11) DEFAULT NULL,
  `ret2` int(11) DEFAULT NULL,
  `ret3` int(11) DEFAULT NULL,
  `ret4` int(11) DEFAULT NULL,
  `ret5` int(11) DEFAULT NULL,
  `ret6` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

-- --------------------------------------------------------

--
-- Структура таблицы `calc_drives`
--

CREATE TABLE `calc_drives` (
  `order_id` int(11) DEFAULT NULL,
  `drive_type_id` int(11) DEFAULT NULL,
  `courier_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

-- --------------------------------------------------------

--
-- Структура таблицы `calc_leads`
--

CREATE TABLE `calc_leads` (
  `tel` bigint(20) DEFAULT NULL,
  `called` date DEFAULT NULL,
  `orig_result_id` int(11) DEFAULT NULL,
  `group_name` char(50) DEFAULT NULL,
  `result_name` char(50) DEFAULT NULL,
  `source_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

-- --------------------------------------------------------

--
-- Структура таблицы `calc_leads_uq`
--

CREATE TABLE `calc_leads_uq` (
  `called` date DEFAULT NULL,
  `tel` bigint(20) DEFAULT NULL,
  `group_name` char(50) DEFAULT NULL,
  `source_id` int(11) DEFAULT NULL,
  `ordered` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

-- --------------------------------------------------------

--
-- Структура таблицы `calc_orders`
--

CREATE TABLE `calc_orders` (
  `order_id` int(11) NOT NULL,
  `client_id` bigint(20) NOT NULL,
  `client_orig_id` int(11) NOT NULL,
  `client_tel` varchar(255) DEFAULT NULL,
  `created` date NOT NULL,
  `gathered` date NOT NULL,
  `delivered` date NOT NULL,
  `order_cohort` int(11) DEFAULT NULL,
  `client_cohort` int(11) DEFAULT NULL,
  `client_age` int(11) DEFAULT NULL,
  `is_first_order` bit(1) NOT NULL,
  `is_first_request` bit(1) NOT NULL,
  `is_eco_order` bit(1) NOT NULL,
  `status_is_ok` bit(1) NOT NULL,
  `status_id` int(11) NOT NULL,
  `status_orig_id` int(11) NOT NULL,
  `status_name` varchar(255) DEFAULT NULL,
  `promo_id` int(11) NOT NULL,
  `promo_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

-- --------------------------------------------------------

--
-- Структура таблицы `calc_orders_eco`
--

CREATE TABLE `calc_orders_eco` (
  `order_id` int(11) DEFAULT NULL,
  `prev_order_id` int(11) DEFAULT NULL,
  `gathered` date DEFAULT NULL,
  `courier_id` int(11) DEFAULT NULL,
  `prev_courier_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

-- --------------------------------------------------------

--
-- Структура таблицы `calc_periods`
--

CREATE TABLE `calc_periods` (
  `period_id` int(11) NOT NULL,
  `period_type` int(11) DEFAULT NULL,
  `date1` date DEFAULT NULL,
  `date2` date DEFAULT NULL,
  `length` int(11) DEFAULT NULL,
  `requests` int(11) DEFAULT NULL,
  `orders` int(11) DEFAULT NULL,
  `first_requests` int(11) DEFAULT NULL,
  `first_orders` int(11) DEFAULT NULL,
  `repeat_orders` int(11) DEFAULT NULL,
  `promo_orders` int(11) DEFAULT NULL,
  `eco_orders` int(11) DEFAULT NULL,
  `speed` float DEFAULT NULL,
  `first_speed` float DEFAULT NULL,
  `repeat_speed` float DEFAULT NULL,
  `promo_speed` float DEFAULT NULL,
  `survival` int(11) DEFAULT NULL,
  `first_survival` int(11) DEFAULT NULL,
  `eco_rate` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

-- --------------------------------------------------------

--
-- Структура таблицы `calc_recalls`
--

CREATE TABLE `calc_recalls` (
  `client_id` bigint(20) NOT NULL,
  `last_lead` date DEFAULT NULL,
  `last_order_id` int(11) DEFAULT NULL,
  `delivered` date DEFAULT NULL,
  `is_first_order` bit(1) DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL,
  `phone_sms` bigint(20) DEFAULT NULL,
  `should_call` bit(1) DEFAULT NULL,
  `caller2` varchar(50) DEFAULT NULL,
  `call_result2` varchar(50) DEFAULT NULL,
  `fail_reason2` varchar(50) DEFAULT NULL,
  `reject_reason2` varchar(50) DEFAULT NULL,
  `opinion2` varchar(50) DEFAULT NULL,
  `comm2` varchar(2000) DEFAULT NULL,
  `caller1` varchar(50) DEFAULT NULL,
  `call_result1` varchar(50) DEFAULT NULL,
  `fail_reason1` varchar(50) DEFAULT NULL,
  `reject_reason1` varchar(50) DEFAULT NULL,
  `opinion1` varchar(50) DEFAULT NULL,
  `comm1` varchar(2000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

-- --------------------------------------------------------

--
-- Структура таблицы `category`
--

CREATE TABLE `category` (
  `category_id` int(11) NOT NULL,
  `category_razdel` varchar(50) NOT NULL DEFAULT 'admin' COMMENT 'Раздел',
  `category_title` varchar(50) NOT NULL DEFAULT '' COMMENT 'Заголовок',
  `category_description` varchar(50) NOT NULL DEFAULT '' COMMENT 'Описание',
  `category_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT 'Публиковать',
  `category_order` int(11) NOT NULL DEFAULT '0' COMMENT 'Порядок',
  `category_logo` varchar(255) DEFAULT '' COMMENT 'Лого',
  `category_created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Дата создания',
  `category_updated_at` timestamp NULL DEFAULT NULL COMMENT 'Дата изменения'
) ENGINE=InnoDB AVG_ROW_LENGTH=862 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `change_log`
--

CREATE TABLE `change_log` (
  `id` int(11) NOT NULL,
  `change_type` int(11) DEFAULT NULL,
  `object` varchar(100) DEFAULT NULL,
  `object_field` varchar(100) DEFAULT NULL,
  `object_pk` int(11) NOT NULL,
  `olddata` text,
  `newdata` text,
  `changedby` int(11) DEFAULT NULL,
  `changedon` int(11) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=96 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `change_log_client_data_field_types`
--

CREATE TABLE `change_log_client_data_field_types` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=1489 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `change_log_fields`
--

CREATE TABLE `change_log_fields` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `field` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=1489 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `change_object_types`
--

CREATE TABLE `change_object_types` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=5461 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `change_types`
--

CREATE TABLE `change_types` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=5461 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `checking_account`
--

CREATE TABLE `checking_account` (
  `id` int(11) NOT NULL,
  `number` varchar(20) NOT NULL,
  `target_id` int(11) DEFAULT NULL,
  `contractor_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COMMENT='банковские счета';

-- --------------------------------------------------------

--
-- Структура таблицы `client`
--

CREATE TABLE `client` (
  `client_id` int(10) NOT NULL,
  `client_promo_code` char(32) DEFAULT NULL,
  `client_name` char(100) DEFAULT NULL,
  `client_email` char(200) DEFAULT NULL,
  `client_tel` varchar(255) DEFAULT NULL,
  `client_sms_tel` varchar(255) DEFAULT NULL,
  `referral_id` int(11) DEFAULT NULL,
  `urgent_comment` text,
  `comment` text,
  `blacklist` tinyint(1) DEFAULT NULL,
  `sms_tel1_allowed` tinyint(1) DEFAULT '1',
  `createby` int(11) DEFAULT NULL,
  `createdon` int(11) DEFAULT NULL,
  `lastmodifiedby` int(11) DEFAULT NULL,
  `lastmodifiedon` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `email_allowed` tinyint(4) DEFAULT '1',
  `client_type` int(4) DEFAULT '0',
  `company_id` int(11) DEFAULT '0',
  `referral_link` int(4) DEFAULT '0',
  `auth_key` varchar(32) DEFAULT NULL,
  `password_hash` varchar(255) DEFAULT NULL,
  `last_auth_sms_time` int(11) DEFAULT '0' COMMENT 'Время последнего отправленного смс при авторизации'
) ENGINE=InnoDB AVG_ROW_LENGTH=684 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `client_card`
--

CREATE TABLE `client_card` (
  `id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL COMMENT 'Идентификатор клиента',
  `binding_id_hash` varchar(255) DEFAULT NULL COMMENT 'Содержит хэш от идентификатора карты в банке',
  `masked_pan` varchar(19) DEFAULT NULL COMMENT 'Маска номера карты клиента'
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

-- --------------------------------------------------------

--
-- Структура таблицы `client_data_change_log`
--

CREATE TABLE `client_data_change_log` (
  `id` int(11) NOT NULL,
  `changetype_id` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `field_id` int(11) DEFAULT NULL,
  `olddata_text` tinyint(1) DEFAULT NULL,
  `newdata_text` int(11) DEFAULT NULL,
  `changedby` int(11) DEFAULT NULL,
  `changedon` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `client_data_change_types`
--

CREATE TABLE `client_data_change_types` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=5461 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `client_device`
--

CREATE TABLE `client_device` (
  `id` int(11) NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `last_visit` int(11) DEFAULT NULL,
  `count_visit` int(11) DEFAULT NULL,
  `date_registration` int(11) DEFAULT NULL,
  `active` smallint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `client_push_notification`
--

CREATE TABLE `client_push_notification` (
  `id` int(11) NOT NULL,
  `device_id` int(11) DEFAULT NULL,
  `title` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `body` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `click_action` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_send` int(11) DEFAULT NULL,
  `status_code` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `visited` smallint(1) DEFAULT '0',
  `hash` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `target` smallint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `client_review`
--

CREATE TABLE `client_review` (
  `client_review_id` int(10) UNSIGNED NOT NULL COMMENT 'Код',
  `order_id` int(11) DEFAULT NULL COMMENT 'Код заказа',
  `client_id` int(11) DEFAULT NULL COMMENT 'Код клиента',
  `client_review_mark_1` int(11) DEFAULT NULL COMMENT 'Качество чистки/стирки',
  `client_review_mark_2` int(11) DEFAULT NULL COMMENT 'Качество глажки',
  `client_review_mark_3` int(11) DEFAULT NULL COMMENT 'Курьер сбор',
  `client_review_mark_4` int(11) DEFAULT NULL COMMENT 'Курьер возврат',
  `client_review_mark_5` int(11) DEFAULT NULL COMMENT 'Оператор',
  `client_review_comment` text COMMENT 'Комментарий',
  `client_review_created_at` int(11) NOT NULL COMMENT 'Оценка отзыва',
  `client_review_rating` int(11) NOT NULL DEFAULT '0' COMMENT '	Комментарий от директора',
  `client_review_comment_from_director` text
) ENGINE=InnoDB AVG_ROW_LENGTH=331 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `contractor`
--

CREATE TABLE `contractor` (
  `id` int(11) NOT NULL,
  `internal_id` int(11) NOT NULL COMMENT 'внутренний id из таблиц сущностей зависит от type_id',
  `type_id` int(11) NOT NULL COMMENT 'тип сущности на который указывает intermal_id'
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COMMENT='контрагенты';

-- --------------------------------------------------------

--
-- Структура таблицы `cookie_user`
--

CREATE TABLE `cookie_user` (
  `id` int(11) NOT NULL,
  `old_user` int(11) DEFAULT NULL,
  `new_user` int(11) DEFAULT NULL,
  `count` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `couriers`
--

CREATE TABLE `couriers` (
  `id` int(11) NOT NULL,
  `name` varchar(60) CHARACTER SET utf8 DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `homezone` text COLLATE utf8_unicode_ci,
  `fullday` tinyint(1) DEFAULT NULL,
  `card_number` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `card_pin` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `scheme_id` int(11) DEFAULT NULL,
  `hiredon` date DEFAULT NULL,
  `dismissedon` date DEFAULT NULL,
  `comment` text COLLATE utf8_unicode_ci,
  `deleted` int(11) NOT NULL DEFAULT '0',
  `snils` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `inn` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `os_id` int(4) DEFAULT NULL,
  `terminal` int(4) DEFAULT NULL,
  `weigher` int(4) DEFAULT NULL,
  `card_alp` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `instr` int(4) DEFAULT NULL,
  `schedule_id` int(11) DEFAULT NULL,
  `courier_type_id` int(11) DEFAULT NULL,
  `calculate_benzine` tinyint(1) DEFAULT '0',
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=655 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `courier_actions`
--

CREATE TABLE `courier_actions` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=2730 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `courier_card`
--

CREATE TABLE `courier_card` (
  `id` int(11) NOT NULL,
  `courier_id` int(11) NOT NULL,
  `pan` varchar(24) NOT NULL,
  `pin` varchar(5) DEFAULT NULL,
  `status` smallint(6) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

-- --------------------------------------------------------

--
-- Структура таблицы `courier_card_history`
--

CREATE TABLE `courier_card_history` (
  `id` int(11) NOT NULL,
  `courier_id` int(11) NOT NULL,
  `card_id` int(11) NOT NULL,
  `date_start` int(11) NOT NULL,
  `date_end` int(11) NOT NULL DEFAULT '2147385600',
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

-- --------------------------------------------------------

--
-- Структура таблицы `courier_credit_statement`
--

CREATE TABLE `courier_credit_statement` (
  `id` int(11) NOT NULL,
  `courier_id` int(11) DEFAULT NULL,
  `sum` float DEFAULT NULL,
  `hash` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Если хеш отсутствует, то перевод считается корректировочным',
  `date` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `courier_payment_schemes`
--

CREATE TABLE `courier_payment_schemes` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `min_amount_per_shift` int(11) DEFAULT NULL,
  `homezone_amount` int(11) DEFAULT NULL,
  `alienzone_amount` int(11) DEFAULT NULL,
  `max_checkin_distance` int(11) DEFAULT NULL,
  `home_zone_cost` float DEFAULT NULL,
  `alien_zone_cost` float DEFAULT NULL,
  `courier_drive_bonus` float DEFAULT NULL,
  `min_salary_per_shift` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `courier_schedules`
--

CREATE TABLE `courier_schedules` (
  `id` int(11) NOT NULL,
  `courier_id` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `couriershift_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `courier_selfie`
--

CREATE TABLE `courier_selfie` (
  `id` int(11) NOT NULL,
  `courier_id` int(11) NOT NULL,
  `created_at` int(11) NOT NULL,
  `verified_at` int(11) DEFAULT NULL,
  `image` varchar(255) NOT NULL,
  `status` int(11) DEFAULT '0',
  `punctuality` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

-- --------------------------------------------------------

--
-- Структура таблицы `courier_shifts`
--

CREATE TABLE `courier_shifts` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `scheme_id` int(11) DEFAULT NULL,
  `workingday` tinyint(1) DEFAULT NULL,
  `time_from` time DEFAULT NULL,
  `time_to` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `courier_to_region`
--

CREATE TABLE `courier_to_region` (
  `id` int(11) NOT NULL,
  `courier_id` int(11) DEFAULT NULL,
  `region_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

-- --------------------------------------------------------

--
-- Структура таблицы `drives`
--

CREATE TABLE `drives` (
  `id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `courier_id` int(11) DEFAULT NULL,
  `drive_type_id` int(11) DEFAULT NULL,
  `attempt_number` int(11) DEFAULT NULL,
  `address` text,
  `addressbook_id` int(11) DEFAULT NULL,
  `createdon` int(11) DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `homezone` tinyint(1) DEFAULT NULL,
  `actual_arrival` int(11) DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL,
  `result_id` int(11) DEFAULT NULL,
  `drycleaner_id` int(11) DEFAULT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `checkin_distance` float DEFAULT NULL,
  `comment` text,
  `courier_action_id` int(11) DEFAULT NULL,
  `planned_from` int(11) DEFAULT NULL,
  `planned_to` int(11) DEFAULT NULL,
  `time_slot` int(4) DEFAULT NULL,
  `drycleaningDriveUid` int(11) DEFAULT NULL,
  `shift_type_id` int(4) DEFAULT NULL,
  `same_district` int(4) DEFAULT NULL,
  `mileage` float DEFAULT NULL,
  `distance_diff` float DEFAULT NULL,
  `benzine` float DEFAULT NULL,
  `is_update` smallint(6) NOT NULL DEFAULT '0',
  `region_title` varchar(255) DEFAULT NULL,
  `cancel_reason` varchar(255) DEFAULT NULL,
  `start` int(11) DEFAULT NULL,
  `details` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `drive_results`
--

CREATE TABLE `drive_results` (
  `id` int(11) NOT NULL,
  `name` text
) ENGINE=InnoDB AVG_ROW_LENGTH=5461 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `drive_statuses`
--

CREATE TABLE `drive_statuses` (
  `id` int(11) NOT NULL,
  `name` text
) ENGINE=InnoDB AVG_ROW_LENGTH=3276 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `drive_types`
--

CREATE TABLE `drive_types` (
  `id` int(11) NOT NULL,
  `client` tinyint(1) DEFAULT NULL,
  `collection` tinyint(1) DEFAULT NULL,
  `name` text
) ENGINE=InnoDB AVG_ROW_LENGTH=4096 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `drycleaners`
--

CREATE TABLE `drycleaners` (
  `id` int(11) NOT NULL,
  `name` text,
  `phone` varchar(20) DEFAULT NULL,
  `contact_person_name` varchar(255) DEFAULT NULL,
  `working_hours` text,
  `address` text,
  `comment` varchar(255) DEFAULT NULL,
  `visible_courier` tinyint(1) DEFAULT '1' COMMENT 'видна ли фабрика в личном кабинете курьера'
) ENGINE=InnoDB AVG_ROW_LENGTH=3276 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `drycleaner_date_schedules`
--

CREATE TABLE `drycleaner_date_schedules` (
  `id` int(11) NOT NULL,
  `drycleaner_id` int(11) DEFAULT NULL,
  `open` tinyint(1) DEFAULT NULL,
  `type` int(4) DEFAULT '0',
  `date` int(11) DEFAULT '0',
  `time_from` varchar(10) DEFAULT '00:00',
  `time_to` varchar(10) DEFAULT '00:00'
) ENGINE=InnoDB AVG_ROW_LENGTH=372 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `drycleaner_orders`
--

CREATE TABLE `drycleaner_orders` (
  `id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `dry_order_id` int(11) NOT NULL COMMENT 'ID заказа в системе ХЧ',
  `dry_order_num` varchar(255) NOT NULL COMMENT 'Номер заказа в системе ХЧ',
  `status_id` int(11) DEFAULT NULL COMMENT 'Статус заказа в системе ХЧ',
  `date_in` date DEFAULT NULL COMMENT 'Дата принятия заказа',
  `date_out` datetime DEFAULT NULL COMMENT 'Дата готовности заказа'
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

-- --------------------------------------------------------

--
-- Структура таблицы `drycleaner_positions_in_orders`
--

CREATE TABLE `drycleaner_positions_in_orders` (
  `id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `position_id` int(11) DEFAULT NULL,
  `position_name` text,
  `position_price` float DEFAULT NULL,
  `unit` text,
  `airo_discount` float DEFAULT NULL,
  `airo_price` float DEFAULT NULL,
  `days_to_clean` int(11) DEFAULT NULL,
  `goods_id` int(11) DEFAULT NULL,
  `name` text,
  `price` float DEFAULT NULL,
  `qty` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `drycleaner_pricelists`
--

CREATE TABLE `drycleaner_pricelists` (
  `id` int(11) NOT NULL,
  `drycleaner_id` int(11) DEFAULT NULL,
  `drycleaner_name` text,
  `drycleaner_price` float DEFAULT NULL,
  `unit_id` int(11) DEFAULT NULL,
  `airo_discount` float DEFAULT NULL,
  `airo_price` float DEFAULT NULL,
  `goods_id` int(11) DEFAULT NULL,
  `days_to_clean` int(11) DEFAULT NULL,
  `position_type` int(4) DEFAULT '0',
  `term_type` int(4) DEFAULT '0',
  `dry_good_id` int(11) DEFAULT NULL COMMENT 'ID вещи в ХЧ',
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB AVG_ROW_LENGTH=163 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `dry_goods_in_orders`
--

CREATE TABLE `dry_goods_in_orders` (
  `id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `good_id` int(11) DEFAULT NULL COMMENT 'Поле содержит значение drycleaner_pricelists.id !!!',
  `name` text,
  `price` float DEFAULT NULL,
  `qty` float DEFAULT NULL,
  `dry_good_id` int(11) DEFAULT '0' COMMENT 'ID вещи в ХЧ',
  `dry_good_code` varchar(255) DEFAULT NULL COMMENT 'Code вещи из ХЧ',
  `dry_service_id` int(11) DEFAULT '0' COMMENT 'ID сервиса в ХЧ',
  `dry_order_id` int(11) DEFAULT '0' COMMENT 'ID заказа из ХЧ',
  `childrens` tinyint(1) DEFAULT '0' COMMENT 'Детское. Используется для скидки',
  `ironing` tinyint(1) DEFAULT '0' COMMENT 'Глажка. Используется для скидки'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `faq`
--

CREATE TABLE `faq` (
  `faq_id` int(11) NOT NULL COMMENT 'Код вопроса',
  `category_id` int(11) DEFAULT NULL COMMENT 'Код категории',
  `faq_question` varchar(255) DEFAULT NULL COMMENT 'Вопрос',
  `faq_answer` text COMMENT 'Ответ',
  `faq_order` int(11) DEFAULT '1' COMMENT 'Порядок',
  `faq_created_at` int(11) DEFAULT NULL COMMENT 'Дата создания\\\\последнего изменения'
) ENGINE=InnoDB AVG_ROW_LENGTH=682 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `faq_category`
--

CREATE TABLE `faq_category` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL COMMENT 'Название категории',
  `order_by` int(11) NOT NULL COMMENT 'Порядковый номер',
  `logo` varchar(255) DEFAULT NULL COMMENT 'Логотип категории'
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

-- --------------------------------------------------------

--
-- Структура таблицы `good`
--

CREATE TABLE `good` (
  `good_id` int(11) NOT NULL COMMENT 'Код',
  `category_id` int(11) DEFAULT '1' COMMENT 'Категория',
  `good_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Наименование',
  `good_price` decimal(10,0) NOT NULL COMMENT 'Цена 24 ч',
  `good_price2` decimal(10,0) DEFAULT NULL COMMENT 'Цена 72 ч',
  `good_short_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Короткое наименование',
  `good_description` text COLLATE utf8_unicode_ci COMMENT 'Тип',
  `good_sort` int(11) DEFAULT '0' COMMENT 'Порядок',
  `good_status` tinyint(1) DEFAULT '0' COMMENT 'Опубликовано',
  `good_created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Дата создания',
  `good_updated_at` timestamp NULL DEFAULT NULL COMMENT 'Дата изменения',
  `unit_id` tinyint(4) DEFAULT '0',
  `show_home` tinyint(4) DEFAULT '0',
  `show_calc` tinyint(4) DEFAULT '0',
  `days_to_clean` tinyint(4) DEFAULT '1',
  `good_logo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `good_sort_new` int(11) DEFAULT '0',
  `show_home_new` int(11) DEFAULT '0'
) ENGINE=InnoDB AVG_ROW_LENGTH=262 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `goods_in_orders`
--

CREATE TABLE `goods_in_orders` (
  `id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `good_id` int(11) DEFAULT NULL,
  `name` text,
  `price` float DEFAULT NULL,
  `qty` float DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT '0',
  `childrens` tinyint(1) DEFAULT '0' COMMENT 'Детское. Используется для скидки',
  `ironing` tinyint(1) DEFAULT '0' COMMENT 'Глажка. Используется для скидки'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `good_synonyms`
--

CREATE TABLE `good_synonyms` (
  `id` int(11) NOT NULL,
  `word` varchar(255) DEFAULT NULL,
  `good_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `holiday`
--

CREATE TABLE `holiday` (
  `holiday_id` int(11) NOT NULL,
  `holiday_date` date NOT NULL,
  `holiday_active` int(11) NOT NULL DEFAULT '1',
  `holiday_created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=16384 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `leads`
--

CREATE TABLE `leads` (
  `id` int(11) NOT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `createdon` int(11) DEFAULT NULL,
  `handledon` int(11) DEFAULT NULL,
  `handledby` int(11) DEFAULT NULL,
  `lead_result_id` int(11) DEFAULT NULL,
  `operator_id` int(11) DEFAULT NULL,
  `recall_date` datetime DEFAULT NULL,
  `type_id` int(11) NOT NULL COMMENT 'Тип лида из lead_type',
  `sms_sent` datetime DEFAULT NULL COMMENT 'Время отправки смс',
  `sort_date` datetime DEFAULT NULL COMMENT 'Время для сортировки по умолчанию',
  `hint` varchar(255) DEFAULT NULL COMMENT 'Подсказка к лиду',
  `opinion_id` int(11) DEFAULT '1' COMMENT 'Мнение клиента',
  `in_processing` tinyint(1) DEFAULT '0' COMMENT 'В обработке',
  `processing_at` int(11) DEFAULT NULL COMMENT 'Кем обрабатывается'
) ENGINE=InnoDB AVG_ROW_LENGTH=8192 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `lead_category`
--

CREATE TABLE `lead_category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

-- --------------------------------------------------------

--
-- Структура таблицы `lead_comment`
--

CREATE TABLE `lead_comment` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `comment` text,
  `date` int(11) DEFAULT NULL,
  `lead_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

-- --------------------------------------------------------

--
-- Структура таблицы `lead_history`
--

CREATE TABLE `lead_history` (
  `id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `type_id` int(11) NOT NULL,
  `result_id` int(11) NOT NULL,
  `recall_date` datetime DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `opinion_id` int(11) NOT NULL DEFAULT '1',
  `comment` text
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

-- --------------------------------------------------------

--
-- Структура таблицы `lead_opinion`
--

CREATE TABLE `lead_opinion` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `hint` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

-- --------------------------------------------------------

--
-- Структура таблицы `lead_results`
--

CREATE TABLE `lead_results` (
  `id` int(11) NOT NULL,
  `name` text,
  `label` varchar(100) DEFAULT NULL,
  `hint` varchar(255) DEFAULT NULL COMMENT 'Подсказка к полю комментарий',
  `category_id` int(11) DEFAULT NULL,
  `lead_type` int(11) NOT NULL DEFAULT '1' COMMENT 'Входящий = 1 или Исходящий = 2'
) ENGINE=InnoDB AVG_ROW_LENGTH=4096 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `lead_type`
--

CREATE TABLE `lead_type` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `hint` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

-- --------------------------------------------------------

--
-- Структура таблицы `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=248 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `order`
--

CREATE TABLE `order` (
  `order_id` int(10) NOT NULL,
  `client_id` int(10) DEFAULT NULL,
  `client_promo_code` varchar(32) DEFAULT NULL,
  `order_from` varchar(100) DEFAULT NULL,
  `order_to` varchar(100) DEFAULT NULL,
  `order_date_from` date DEFAULT NULL,
  `order_date_to` date DEFAULT NULL,
  `order_time_from` varchar(50) DEFAULT NULL,
  `order_time_to` varchar(50) DEFAULT NULL,
  `order_status` tinyint(1) DEFAULT NULL,
  `order_creation_date` datetime NOT NULL,
  `order_comment` text,
  `admin_comment` text,
  `order_result_price` int(11) DEFAULT '0',
  `order_ats_price` int(11) DEFAULT '0' COMMENT 'Сумма по вещам из АТС',
  `goods_result_price` int(11) DEFAULT '0' COMMENT 'Сумма по вещам Айро',
  `payed_sum` int(11) DEFAULT NULL COMMENT 'Фактически оплачено',
  `payed_date` int(11) DEFAULT NULL COMMENT 'Дата оплаты',
  `order_hash` varchar(255) NOT NULL,
  `order_address_data` text,
  `order_address2_data` text,
  `order_system_added` int(11) DEFAULT NULL,
  `order_review_at` int(11) DEFAULT NULL,
  `order_update_status` tinyint(1) DEFAULT NULL,
  `order_drive` int(11) NOT NULL,
  `order_sobral` int(11) DEFAULT NULL,
  `order_edit` varchar(5) NOT NULL DEFAULT 'N',
  `drycleaner_id` int(11) DEFAULT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `payment_type_id` int(11) DEFAULT NULL,
  `discount` float DEFAULT NULL,
  `comment_airo` text,
  `comment_for_drycleaner` text,
  `lead_id` int(11) DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT '0',
  `client_phone` varchar(20) DEFAULT NULL,
  `client_email` varchar(200) DEFAULT NULL,
  `order_courier_action` int(11) DEFAULT NULL,
  `approved` int(4) DEFAULT '0',
  `creator_id` int(11) DEFAULT NULL,
  `order_invoice_sent` tinyint(1) DEFAULT '0',
  `process` int(1) DEFAULT '0' COMMENT 'Флаг: Обработан ли заказ',
  `is_sms_sent` datetime DEFAULT NULL COMMENT 'Дата последней отправки sms клиенту о сумме заказа',
  `previous_status` int(11) DEFAULT NULL,
  `double_delivery` tinyint(1) DEFAULT NULL COMMENT 'Двойная доставка',
  `address_from_id` int(11) DEFAULT NULL,
  `address_to_id` int(11) DEFAULT NULL,
  `source` int(11) DEFAULT '0' COMMENT 'Источник',
  `client_signature` varchar(255) DEFAULT NULL COMMENT 'Изображение подписи клиента',
  `express` tinyint(1) DEFAULT '0' COMMENT 'экспресс доставка'
) ENGINE=InnoDB AVG_ROW_LENGTH=840 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `orders_x_drives`
--

CREATE TABLE `orders_x_drives` (
  `id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `drive_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `order_cancel_log`
--

CREATE TABLE `order_cancel_log` (
  `id` int(11) NOT NULL,
  `date` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `reason_id` int(11) DEFAULT NULL,
  `comment` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

-- --------------------------------------------------------

--
-- Структура таблицы `order_discount_history`
--

CREATE TABLE `order_discount_history` (
  `id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `discount_types` text COLLATE utf8_unicode_ci COMMENT 'Типы скидок по заказу',
  `sum` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `order_invoice`
--

CREATE TABLE `order_invoice` (
  `id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `value` varchar(20) DEFAULT NULL,
  `invoice_url` varchar(255) DEFAULT NULL COMMENT 'Ссылка на квитанцию'
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

-- --------------------------------------------------------

--
-- Структура таблицы `order_pays`
--

CREATE TABLE `order_pays` (
  `id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `drive_id` int(11) DEFAULT NULL,
  `payed` int(11) DEFAULT NULL,
  `cost` float DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `not_reason` varchar(255) DEFAULT NULL,
  `dif_reason` varchar(255) DEFAULT NULL,
  `date_done` datetime DEFAULT NULL,
  `corrected` tinyint(1) DEFAULT '0',
  `real_sum` float DEFAULT NULL,
  `invoice` varchar(30) DEFAULT NULL,
  `checked` tinyint(1) DEFAULT '0',
  `comment` text,
  `internal_order_number` varchar(64) DEFAULT NULL COMMENT 'внутренний номер заказа в платежной системе',
  `internal_status` varchar(16) DEFAULT NULL COMMENT 'статус в платежной системе',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `real_cost` float DEFAULT NULL COMMENT 'Поле не используется, просто для истории',
  `is_test` int(11) DEFAULT NULL COMMENT 'Флаг: тестовая оплата'
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `order_statuses`
--

CREATE TABLE `order_statuses` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `color` varchar(255) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1'
) ENGINE=InnoDB AVG_ROW_LENGTH=744 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `order_update_log`
--

CREATE TABLE `order_update_log` (
  `id` int(11) NOT NULL,
  `date` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `reason_id` int(11) DEFAULT NULL,
  `comment` varchar(200) DEFAULT NULL,
  `drive_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

-- --------------------------------------------------------

--
-- Структура таблицы `order_warehouse_history`
--

CREATE TABLE `order_warehouse_history` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `pick_warehouse_id` int(11) DEFAULT NULL,
  `delivery_warehouse_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COMMENT='хранит информациию с какого склада забрали заказ на фабрику и куда доставили после чистки';

-- --------------------------------------------------------

--
-- Структура таблицы `our_team`
--

CREATE TABLE `our_team` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `comment` text,
  `img` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `param`
--

CREATE TABLE `param` (
  `param_id` int(11) NOT NULL COMMENT 'Код',
  `param_razdel` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'admin' COMMENT 'Раздел',
  `param_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Имя',
  `param_value` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Значение',
  `param_default_value` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Значение по умолчанию',
  `param_label` varchar(64) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Заголок',
  `param_type` varchar(64) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Тип',
  `param_logo` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Лого',
  `param_sort` int(11) NOT NULL DEFAULT '0' COMMENT 'Порядок',
  `param_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Опубликовано',
  `param_created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Дата создания',
  `param_updated_at` timestamp NULL DEFAULT NULL COMMENT 'Дата изменения'
) ENGINE=MyISAM AVG_ROW_LENGTH=147 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `payment_types`
--

CREATE TABLE `payment_types` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=4096 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `photo`
--

CREATE TABLE `photo` (
  `photo_id` int(11) NOT NULL COMMENT 'Ид',
  `photo_class` varchar(128) NOT NULL COMMENT 'Родительский класс',
  `photo_item_id` int(11) NOT NULL COMMENT 'Ид родительского элемента',
  `photo_path` varchar(128) NOT NULL COMMENT 'Фото',
  `photo_orig_name` varchar(128) NOT NULL COMMENT 'Исходное имя файла',
  `photo_description` varchar(1024) NOT NULL COMMENT 'Описание',
  `photo_sort` int(11) NOT NULL DEFAULT '0' COMMENT 'Сортировка',
  `photo_created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Дата'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `profile`
--

CREATE TABLE `profile` (
  `user_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `public_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gravatar_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gravatar_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bio` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB AVG_ROW_LENGTH=16384 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `promo_code`
--

CREATE TABLE `promo_code` (
  `promo_code_id` int(11) NOT NULL COMMENT 'Номер',
  `promo_code_code` varchar(255) NOT NULL COMMENT 'Код',
  `promo_code_text` varchar(255) DEFAULT NULL COMMENT 'Описание',
  `promo_code_active` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Статус',
  `promo_code_discount` varchar(255) DEFAULT NULL COMMENT 'Скидка',
  `promo_code_type` int(11) NOT NULL DEFAULT '0' COMMENT 'Тип'
) ENGINE=InnoDB AVG_ROW_LENGTH=1365 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `promo_code_request`
--

CREATE TABLE `promo_code_request` (
  `id` int(10) NOT NULL,
  `client_name` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `promo_code_id` int(11) NOT NULL DEFAULT '0',
  `promo_code_text` varchar(255) DEFAULT NULL,
  `session_id` varchar(255) DEFAULT NULL,
  `insert_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AVG_ROW_LENGTH=140 DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `promo_code_requests`
--

CREATE TABLE `promo_code_requests` (
  `id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `promo_code_id` int(11) DEFAULT '0',
  `promo_code_text` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) DEFAULT '0',
  `created_on` int(11) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=118 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `region`
--

CREATE TABLE `region` (
  `uid` char(36) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `full_title` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

-- --------------------------------------------------------

--
-- Структура таблицы `report_cat_bank_accounts`
--

CREATE TABLE `report_cat_bank_accounts` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `report_cat_categories`
--

CREATE TABLE `report_cat_categories` (
  `id` int(11) NOT NULL,
  `name` varchar(30) DEFAULT NULL,
  `transaction_type_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `report_cat_client_types`
--

CREATE TABLE `report_cat_client_types` (
  `id` int(11) NOT NULL,
  `name` varchar(30) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `report_cat_companies`
--

CREATE TABLE `report_cat_companies` (
  `id` int(11) NOT NULL,
  `name` varchar(30) DEFAULT NULL,
  `contact_person` varchar(30) DEFAULT NULL,
  `phone` varchar(30) DEFAULT NULL,
  `type` int(4) DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `report_cat_employees`
--

CREATE TABLE `report_cat_employees` (
  `id` int(11) NOT NULL,
  `name` varchar(30) DEFAULT NULL,
  `position_id` int(11) DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `report_cat_founders`
--

CREATE TABLE `report_cat_founders` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `report_cat_goals`
--

CREATE TABLE `report_cat_goals` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `name` varchar(30) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `report_cat_partners`
--

CREATE TABLE `report_cat_partners` (
  `id` int(11) NOT NULL,
  `name` varchar(30) DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `inn` varchar(12) DEFAULT NULL,
  `kpp` varchar(9) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `report_cat_payment_types`
--

CREATE TABLE `report_cat_payment_types` (
  `id` int(11) NOT NULL,
  `name` varchar(30) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `report_cat_positions`
--

CREATE TABLE `report_cat_positions` (
  `id` int(11) NOT NULL,
  `position` varchar(30) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `report_cat_subgoals`
--

CREATE TABLE `report_cat_subgoals` (
  `id` int(11) NOT NULL,
  `goal_id` int(11) DEFAULT NULL,
  `name` varchar(30) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `report_cat_transactions`
--

CREATE TABLE `report_cat_transactions` (
  `id` int(11) NOT NULL,
  `type_id` int(4) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `goal_id` int(11) DEFAULT NULL,
  `subgoal_id` int(11) DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `report_cat_transaction_types`
--

CREATE TABLE `report_cat_transaction_types` (
  `id` int(11) NOT NULL,
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `report_courier_debt`
--

CREATE TABLE `report_courier_debt` (
  `id` int(11) NOT NULL,
  `date` int(11) DEFAULT NULL,
  `payment` double DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `name` varchar(30) DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `not_reason` varchar(255) DEFAULT NULL,
  `bill` varchar(10) DEFAULT NULL,
  `credited` double DEFAULT NULL,
  `credited_crm` decimal(18,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

-- --------------------------------------------------------

--
-- Дублирующая структура для представления `report_courier_salary`
-- (See below for the actual view)
--
CREATE TABLE `report_courier_salary` (
`date` datetime
,`credited` varchar(11)
,`payment` decimal(18,2)
,`id` int(11)
,`name` varchar(60)
,`shift` varchar(11)
,`bill` varchar(9)
);

-- --------------------------------------------------------

--
-- Структура таблицы `report_fin_account_cats`
--

CREATE TABLE `report_fin_account_cats` (
  `id` int(11) NOT NULL,
  `account_type_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `report_fin_costs`
--

CREATE TABLE `report_fin_costs` (
  `id` int(11) NOT NULL,
  `account_type_id` int(11) DEFAULT NULL,
  `date` int(11) DEFAULT NULL,
  `period` int(11) DEFAULT NULL,
  `sum` decimal(10,2) DEFAULT NULL,
  `transaction_type_id` int(4) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `goal_id` int(11) DEFAULT NULL,
  `subgoal_id` int(11) DEFAULT NULL,
  `contractor` int(11) DEFAULT NULL,
  `comment` text,
  `hash` varchar(255) DEFAULT NULL,
  `contractor_type` varchar(255) DEFAULT NULL,
  `verified` tinyint(1) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `report_fin_profit`
--

CREATE TABLE `report_fin_profit` (
  `id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `factory_id` int(11) DEFAULT NULL,
  `payment_type_id` int(4) DEFAULT NULL,
  `order_date` int(11) DEFAULT NULL,
  `delivery_date` int(11) DEFAULT NULL,
  `period` int(11) DEFAULT NULL,
  `courier_id` int(11) DEFAULT NULL,
  `customer_type_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `goods_sum` decimal(10,2) DEFAULT NULL,
  `delivery_sum` decimal(10,2) DEFAULT NULL,
  `total_sum` decimal(10,2) DEFAULT NULL,
  `payment_sum` decimal(10,2) DEFAULT NULL,
  `actual_sum` decimal(10,2) DEFAULT NULL,
  `payment_date` int(11) DEFAULT NULL,
  `bank_account` varchar(30) DEFAULT NULL,
  `dry_costs` decimal(10,2) DEFAULT NULL,
  `delivery_costs` decimal(10,2) DEFAULT NULL,
  `tucan_costs` decimal(10,2) DEFAULT NULL,
  `discount` decimal(10,2) DEFAULT NULL,
  `fact_sum` float DEFAULT NULL,
  `fact_sum_receipt` decimal(10,2) DEFAULT NULL COMMENT 'Фактически поступление',
  `order_pays_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `report_logistics`
--

CREATE TABLE `report_logistics` (
  `id` int(11) NOT NULL,
  `drive_id` int(11) DEFAULT NULL,
  `shift_id` int(11) DEFAULT NULL,
  `penalty_time` int(4) DEFAULT NULL,
  `penalty_distance` int(4) DEFAULT NULL,
  `penalty_another` int(4) DEFAULT NULL,
  `bonus_sum` int(11) DEFAULT NULL,
  `logistic_sum` int(11) DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `penalty` int(11) DEFAULT NULL,
  `benzine` int(11) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `slot` int(11) DEFAULT NULL,
  `type_drive` int(11) DEFAULT NULL,
  `courier_id` int(11) DEFAULT NULL,
  `date` int(11) DEFAULT NULL,
  `tariff` int(11) DEFAULT NULL,
  `base_sum` int(11) DEFAULT NULL,
  `after_work_time` int(11) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `report_logistics_shift`
--

CREATE TABLE `report_logistics_shift` (
  `id` int(11) NOT NULL,
  `courier_id` int(11) DEFAULT NULL,
  `drive_total_sum` int(11) DEFAULT NULL,
  `salary_shift` int(11) DEFAULT NULL,
  `penalty` int(11) DEFAULT NULL,
  `cost` int(11) DEFAULT NULL,
  `total_shift_sum` int(11) DEFAULT NULL,
  `drive_price` int(11) DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `drive_clients` int(11) DEFAULT NULL,
  `drive_factory` int(11) DEFAULT NULL,
  `drive_other` int(11) DEFAULT NULL,
  `drive_total` int(11) DEFAULT NULL,
  `no_drive` int(11) DEFAULT NULL,
  `extra_cash` int(11) DEFAULT NULL,
  `date` int(11) DEFAULT NULL,
  `bonus` int(11) DEFAULT NULL,
  `parking` int(11) DEFAULT NULL,
  `voice` int(11) DEFAULT NULL COMMENT 'телефонная связь',
  `collected_in_delivery` int(11) DEFAULT NULL COMMENT 'Сборы при доставке',
  `total_mileage` float DEFAULT NULL,
  `price_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `report_period`
--

CREATE TABLE `report_period` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

-- --------------------------------------------------------

--
-- Структура таблицы `report_pl`
--

CREATE TABLE `report_pl` (
  `id` int(11) NOT NULL,
  `period` varchar(255) DEFAULT NULL,
  `value` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

-- --------------------------------------------------------

--
-- Структура таблицы `review`
--

CREATE TABLE `review` (
  `review_id` int(11) NOT NULL COMMENT 'Код',
  `review_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Наименование',
  `review_phone` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Телефон',
  `review_content` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Отзыв',
  `review_social_link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `review_logo` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Лого',
  `review_sort` int(11) NOT NULL DEFAULT '0' COMMENT 'Порядок',
  `review_status` tinyint(1) DEFAULT '0' COMMENT 'Опубликовано',
  `review_created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Дата создания',
  `review_updated_at` timestamp NULL DEFAULT NULL COMMENT 'Дата изменения'
) ENGINE=InnoDB AVG_ROW_LENGTH=3276 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `reviews`
--

CREATE TABLE `reviews` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(11) NOT NULL,
  `mark_1` int(11) NOT NULL,
  `mark_2` int(11) NOT NULL,
  `mark_3` int(11) NOT NULL,
  `mark_4` int(11) NOT NULL,
  `mark_5` int(11) NOT NULL,
  `comment` text,
  `created_at` int(11) NOT NULL,
  `rating` int(11) NOT NULL DEFAULT '0',
  `comment_from_director` text
) ENGINE=MyISAM AVG_ROW_LENGTH=160 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `role`
--

CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `can_admin` smallint(6) NOT NULL DEFAULT '0',
  `can_courier` smallint(6) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AVG_ROW_LENGTH=2730 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `schedule_courier`
--

CREATE TABLE `schedule_courier` (
  `id` int(11) NOT NULL,
  `schedule` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

-- --------------------------------------------------------

--
-- Структура таблицы `share`
--

CREATE TABLE `share` (
  `share_id` int(10) NOT NULL,
  `client_id` int(10) DEFAULT NULL,
  `order_id` int(10) DEFAULT NULL,
  `share_promo_code` varchar(255) DEFAULT NULL,
  `share_hash` varchar(255) DEFAULT NULL,
  `share_insert` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AVG_ROW_LENGTH=1820 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `sms_notification`
--

CREATE TABLE `sms_notification` (
  `sms_id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT '0',
  `client_tel` varchar(255) DEFAULT NULL COMMENT 'Телефон',
  `sms_text` text COMMENT 'Сообщение',
  `sms_status` tinyint(1) DEFAULT '1' COMMENT 'Статус',
  `sms_insert_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Отправленно'
) ENGINE=InnoDB AVG_ROW_LENGTH=349 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `statuses`
--

CREATE TABLE `statuses` (
  `id` int(11) NOT NULL,
  `drive_status_id` int(11) DEFAULT NULL COMMENT 'Статус из Атс (1, 2, 3, 4, 5)',
  `drive_type_id` int(11) DEFAULT NULL COMMENT 'Тип выезда',
  `order_status_id` int(11) DEFAULT NULL COMMENT 'Статус заказа согласно drive_status_id и drive_type_id'
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COMMENT='\n            Таблица для сопоставления статуса и типа выезда\n            (при смене статуса выезда), согласно данным из атс.\n            Сопоставив статус и тип выезда с текущей таблицей, получаем статус заказа.\n        ';

-- --------------------------------------------------------

--
-- Структура таблицы `time_slot`
--

CREATE TABLE `time_slot` (
  `id` int(11) NOT NULL,
  `smena_id` int(11) DEFAULT NULL,
  `time` varchar(20) DEFAULT NULL,
  `day_type` int(11) DEFAULT NULL,
  `active` int(4) DEFAULT '1',
  `deleted` int(4) DEFAULT '0',
  `limit` int(4) DEFAULT '5',
  `from` int(11) DEFAULT NULL,
  `to` int(11) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=630 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `tucan_extract`
--

CREATE TABLE `tucan_extract` (
  `id` int(11) NOT NULL,
  `tucan_id` int(11) DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `rrn` bigint(20) DEFAULT NULL,
  `card_type` varchar(30) DEFAULT NULL,
  `tid` int(11) DEFAULT NULL,
  `mid` varchar(50) DEFAULT NULL,
  `card` varchar(60) DEFAULT NULL,
  `description` int(11) DEFAULT NULL,
  `auth_code` varchar(255) DEFAULT NULL,
  `status` varchar(30) DEFAULT NULL,
  `device_id` int(11) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `model` varchar(30) DEFAULT NULL,
  `courier_name` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `type_courier`
--

CREATE TABLE `type_courier` (
  `id` int(11) NOT NULL,
  `type` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

-- --------------------------------------------------------

--
-- Структура таблицы `units`
--

CREATE TABLE `units` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=4096 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `confirmed_at` int(11) DEFAULT NULL,
  `unconfirmed_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `blocked_at` int(11) DEFAULT NULL,
  `registration_ip` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `flags` int(11) NOT NULL DEFAULT '0',
  `last_login_at` int(11) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=16384 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `user_auth`
--

CREATE TABLE `user_auth` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `provider` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provider_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provider_attributes` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `user_old`
--

CREATE TABLE `user_old` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `status` smallint(6) NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `auth_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `access_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `logged_in_ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `logged_in_at` timestamp NULL DEFAULT NULL,
  `created_ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `banned_at` timestamp NULL DEFAULT NULL,
  `banned_reason` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=2730 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `user_token`
--

CREATE TABLE `user_token` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `type` smallint(6) NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `data` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `expired_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `warehouse`
--

CREATE TABLE `warehouse` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `phone` varchar(32) DEFAULT NULL,
  `contact_person` varchar(128) DEFAULT NULL,
  `working_hours` varchar(128) NOT NULL,
  `address` varchar(255) NOT NULL,
  `checking_account` varchar(25) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `visible_courier` tinyint(1) DEFAULT '1' COMMENT 'виден ли склад в личном кабинете курьера'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `working_time_courier`
--

CREATE TABLE `working_time_courier` (
  `id` int(11) NOT NULL,
  `type_id` int(11) DEFAULT NULL,
  `schedule_id` int(11) DEFAULT NULL,
  `time_from` varchar(20) DEFAULT NULL,
  `time_to` varchar(20) DEFAULT NULL,
  `price` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

-- --------------------------------------------------------

--
-- Структура таблицы `work_zone`
--

CREATE TABLE `work_zone` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `coordinate` polygon NOT NULL,
  `color` varchar(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `work_zone_drive_limit`
--

CREATE TABLE `work_zone_drive_limit` (
  `id` int(11) NOT NULL,
  `work_zone_id` int(11) NOT NULL,
  `timeslot_id` int(11) NOT NULL,
  `limit` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура для представления `report_courier_salary`
--
DROP TABLE IF EXISTS `report_courier_salary`;

CREATE ALGORITHM=UNDEFINED DEFINER=`airo`@`%` SQL SECURITY DEFINER VIEW `report_courier_salary`  AS  (select from_unixtime(`rf`.`date`) AS `date`,'' AS `credited`,`rf`.`sum` AS `payment`,`c`.`id` AS `id`,`c`.`name` AS `name`,'' AS `shift`,(case `rf`.`account_type_id` when 1 then 'ТКБ' when 2 then 'Промсвязь' when 3 then 'Альфабанк' when 5 then 'Сейф' end) AS `bill` from (`report_fin_costs` `rf` left join `couriers` `c` on((`c`.`id` = `rf`.`contractor`))) where ((`rf`.`account_type_id` in (1,2,3,5)) and (`rf`.`goal_id` = 6))) union all (select from_unixtime(`rl`.`date`) AS `date`,`rl`.`drive_total_sum` AS `credited`,0 AS `payment`,`c`.`id` AS `id`,`c`.`name` AS `name`,`rl`.`id` AS `shift`,'' AS `bill` from (`report_logistics_shift` `rl` left join `couriers` `c` on((`c`.`id` = `rl`.`courier_id`)))) order by `date` desc ;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `account_target`
--
ALTER TABLE `account_target`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_Account_target_Name` (`name`);

--
-- Индексы таблицы `address_book`
--
ALTER TABLE `address_book`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `address_book_old`
--
ALTER TABLE `address_book_old`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `api_ats_order_status`
--
ALTER TABLE `api_ats_order_status`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `article_attachments`
--
ALTER TABLE `article_attachments`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `article_categories`
--
ALTER TABLE `article_categories`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `article_items`
--
ALTER TABLE `article_items`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `ats_data`
--
ALTER TABLE `ats_data`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ats_data_unique_key_uindex` (`unique_key`),
  ADD UNIQUE KEY `ats_data_composite_uindex` (`courier_id`,`ats_drive`,`status_id`),
  ADD KEY `ats_data_composite_index` (`courier_id`,`drive_id`),
  ADD KEY `fk-ats_data-drive_id-ref-drive-id` (`drive_id`),
  ADD KEY `fk-ats_data-status_id-ref-status-id` (`status_id`);

--
-- Индексы таблицы `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD PRIMARY KEY (`item_name`,`user_id`);

--
-- Индексы таблицы `auth_item`
--
ALTER TABLE `auth_item`
  ADD PRIMARY KEY (`name`),
  ADD KEY `idx-auth_item-type` (`type`),
  ADD KEY `rule_name` (`rule_name`);

--
-- Индексы таблицы `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `child` (`child`);

--
-- Индексы таблицы `auth_rule`
--
ALTER TABLE `auth_rule`
  ADD PRIMARY KEY (`name`);

--
-- Индексы таблицы `calc_calls`
--
ALTER TABLE `calc_calls`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `calc_clients`
--
ALTER TABLE `calc_clients`
  ADD PRIMARY KEY (`client_orig_id`);

--
-- Индексы таблицы `calc_leads`
--
ALTER TABLE `calc_leads`
  ADD KEY `index_tel` (`tel`),
  ADD KEY `index_called` (`called`),
  ADD KEY `index_orig_result_id` (`orig_result_id`),
  ADD KEY `index_group_name` (`group_name`),
  ADD KEY `index_result_name` (`result_name`);

--
-- Индексы таблицы `calc_orders`
--
ALTER TABLE `calc_orders`
  ADD PRIMARY KEY (`order_id`),
  ADD KEY `index_client_id` (`client_id`),
  ADD KEY `index_client_orig_id` (`client_orig_id`),
  ADD KEY `index_created` (`created`),
  ADD KEY `index_status_is_ok` (`status_is_ok`),
  ADD KEY `index_is_first_order` (`is_first_order`);

--
-- Индексы таблицы `calc_periods`
--
ALTER TABLE `calc_periods`
  ADD PRIMARY KEY (`period_id`),
  ADD KEY `index_date1` (`date1`),
  ADD KEY `index_date2` (`date2`);

--
-- Индексы таблицы `calc_recalls`
--
ALTER TABLE `calc_recalls`
  ADD PRIMARY KEY (`client_id`);

--
-- Индексы таблицы `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`category_id`);

--
-- Индексы таблицы `change_log`
--
ALTER TABLE `change_log`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `change_log_client_data_field_types`
--
ALTER TABLE `change_log_client_data_field_types`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `change_log_fields`
--
ALTER TABLE `change_log_fields`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `change_object_types`
--
ALTER TABLE `change_object_types`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `change_types`
--
ALTER TABLE `change_types`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `checking_account`
--
ALTER TABLE `checking_account`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_Сhecking_account_Number` (`number`),
  ADD KEY `fk_Сhecking_account_Contractor_id` (`contractor_id`);

--
-- Индексы таблицы `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`client_id`);

--
-- Индексы таблицы `client_card`
--
ALTER TABLE `client_card`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_client_id` (`client_id`);

--
-- Индексы таблицы `client_data_change_log`
--
ALTER TABLE `client_data_change_log`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `client_data_change_types`
--
ALTER TABLE `client_data_change_types`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `client_device`
--
ALTER TABLE `client_device`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `client_device_token_uindex` (`token`),
  ADD KEY `client_device_client_id_fk` (`client_id`);

--
-- Индексы таблицы `client_push_notification`
--
ALTER TABLE `client_push_notification`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `client_push_notification_hash__uindex` (`hash`),
  ADD KEY `client_push_notification_device_id_fk` (`device_id`);

--
-- Индексы таблицы `client_review`
--
ALTER TABLE `client_review`
  ADD PRIMARY KEY (`client_review_id`);

--
-- Индексы таблицы `contractor`
--
ALTER TABLE `contractor`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_Contractor_Internal_id` (`internal_id`,`type_id`);

--
-- Индексы таблицы `cookie_user`
--
ALTER TABLE `cookie_user`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `couriers`
--
ALTER TABLE `couriers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_couriers_phone` (`phone`);

--
-- Индексы таблицы `courier_actions`
--
ALTER TABLE `courier_actions`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `courier_card`
--
ALTER TABLE `courier_card`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pan` (`pan`),
  ADD KEY `idx_courier_card_courier_id` (`courier_id`),
  ADD KEY `idx_courier_card_pan` (`pan`);

--
-- Индексы таблицы `courier_card_history`
--
ALTER TABLE `courier_card_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-courier_card_history-id-date_start-date_end` (`id`,`date_start`,`date_end`),
  ADD KEY `idx-courier_card_history-courier_id` (`courier_id`),
  ADD KEY `idx-courier_card_history-card_id` (`card_id`);

--
-- Индексы таблицы `courier_credit_statement`
--
ALTER TABLE `courier_credit_statement`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `courier_payment_schemes`
--
ALTER TABLE `courier_payment_schemes`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `courier_schedules`
--
ALTER TABLE `courier_schedules`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `courier_selfie`
--
ALTER TABLE `courier_selfie`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_Courier_selfie_Courier_id__Couriers_Id` (`courier_id`);

--
-- Индексы таблицы `courier_shifts`
--
ALTER TABLE `courier_shifts`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `courier_to_region`
--
ALTER TABLE `courier_to_region`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk1_courier_to_region` (`courier_id`),
  ADD KEY `fk2_courier_to_region` (`region_id`);

--
-- Индексы таблицы `drives`
--
ALTER TABLE `drives`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_drives_warehouse_id__warehouse_id` (`warehouse_id`),
  ADD KEY `planned_from_index` (`planned_from`);

--
-- Индексы таблицы `drive_results`
--
ALTER TABLE `drive_results`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `drive_statuses`
--
ALTER TABLE `drive_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `drive_types`
--
ALTER TABLE `drive_types`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `drycleaners`
--
ALTER TABLE `drycleaners`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `drycleaner_date_schedules`
--
ALTER TABLE `drycleaner_date_schedules`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `drycleaner_orders`
--
ALTER TABLE `drycleaner_orders`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `drycleaner_positions_in_orders`
--
ALTER TABLE `drycleaner_positions_in_orders`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `drycleaner_pricelists`
--
ALTER TABLE `drycleaner_pricelists`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `dry_goods_in_orders`
--
ALTER TABLE `dry_goods_in_orders`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `faq`
--
ALTER TABLE `faq`
  ADD PRIMARY KEY (`faq_id`),
  ADD KEY `FK_faq_category` (`category_id`);

--
-- Индексы таблицы `faq_category`
--
ALTER TABLE `faq_category`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `good`
--
ALTER TABLE `good`
  ADD PRIMARY KEY (`good_id`),
  ADD KEY `FK_good_category` (`category_id`);

--
-- Индексы таблицы `goods_in_orders`
--
ALTER TABLE `goods_in_orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_order_good` (`order_id`,`good_id`);

--
-- Индексы таблицы `good_synonyms`
--
ALTER TABLE `good_synonyms`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `holiday`
--
ALTER TABLE `holiday`
  ADD PRIMARY KEY (`holiday_id`);

--
-- Индексы таблицы `leads`
--
ALTER TABLE `leads`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_type_id` (`type_id`),
  ADD KEY `idx_opinion_id` (`opinion_id`),
  ADD KEY `idx_client_id` (`client_id`),
  ADD KEY `idx_phone` (`phone`),
  ADD KEY `idx_processing_at` (`processing_at`);

--
-- Индексы таблицы `lead_category`
--
ALTER TABLE `lead_category`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `lead_comment`
--
ALTER TABLE `lead_comment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk-lead_comment-lead_id-ref-leads-id` (`lead_id`);

--
-- Индексы таблицы `lead_history`
--
ALTER TABLE `lead_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_client_id` (`client_id`),
  ADD KEY `idx_phone` (`phone`),
  ADD KEY `idx_type_id` (`type_id`),
  ADD KEY `idx_result_id` (`result_id`),
  ADD KEY `idx_created_at` (`created_at`),
  ADD KEY `idx_created_by` (`created_by`),
  ADD KEY `idx_opinion_id` (`opinion_id`);

--
-- Индексы таблицы `lead_opinion`
--
ALTER TABLE `lead_opinion`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `lead_results`
--
ALTER TABLE `lead_results`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_lead_results_to_category` (`category_id`);

--
-- Индексы таблицы `lead_type`
--
ALTER TABLE `lead_type`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_lead_type_id_parent_id` (`parent_id`);

--
-- Индексы таблицы `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Индексы таблицы `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`order_id`),
  ADD KEY `fk_order_warehouse_id__warehouse_id` (`warehouse_id`),
  ADD KEY `idx_client_id` (`client_id`);

--
-- Индексы таблицы `orders_x_drives`
--
ALTER TABLE `orders_x_drives`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `order_cancel_log`
--
ALTER TABLE `order_cancel_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_cancel_log_user_id` (`user_id`),
  ADD KEY `order_cancel_log_order_id` (`order_id`);

--
-- Индексы таблицы `order_discount_history`
--
ALTER TABLE `order_discount_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_discount_order_fk` (`order_id`);

--
-- Индексы таблицы `order_invoice`
--
ALTER TABLE `order_invoice`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_order_id` (`order_id`);

--
-- Индексы таблицы `order_pays`
--
ALTER TABLE `order_pays`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_pays_order` (`order_id`),
  ADD KEY `order_pays_drive` (`drive_id`);

--
-- Индексы таблицы `order_statuses`
--
ALTER TABLE `order_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `order_update_log`
--
ALTER TABLE `order_update_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_update_log_user_id` (`user_id`),
  ADD KEY `order_update_log_order_id` (`order_id`);

--
-- Индексы таблицы `order_warehouse_history`
--
ALTER TABLE `order_warehouse_history`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `our_team`
--
ALTER TABLE `our_team`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `param`
--
ALTER TABLE `param`
  ADD PRIMARY KEY (`param_id`);

--
-- Индексы таблицы `payment_types`
--
ALTER TABLE `payment_types`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `photo`
--
ALTER TABLE `photo`
  ADD PRIMARY KEY (`photo_id`),
  ADD KEY `model_item` (`photo_class`,`photo_item_id`);

--
-- Индексы таблицы `profile`
--
ALTER TABLE `profile`
  ADD PRIMARY KEY (`user_id`);

--
-- Индексы таблицы `promo_code`
--
ALTER TABLE `promo_code`
  ADD PRIMARY KEY (`promo_code_id`),
  ADD UNIQUE KEY `promo_code_code` (`promo_code_code`);

--
-- Индексы таблицы `promo_code_request`
--
ALTER TABLE `promo_code_request`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `promo_code_requests`
--
ALTER TABLE `promo_code_requests`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `region`
--
ALTER TABLE `region`
  ADD PRIMARY KEY (`uid`);

--
-- Индексы таблицы `report_cat_bank_accounts`
--
ALTER TABLE `report_cat_bank_accounts`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `report_cat_categories`
--
ALTER TABLE `report_cat_categories`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `report_cat_client_types`
--
ALTER TABLE `report_cat_client_types`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `report_cat_companies`
--
ALTER TABLE `report_cat_companies`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `report_cat_employees`
--
ALTER TABLE `report_cat_employees`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `report_cat_founders`
--
ALTER TABLE `report_cat_founders`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `report_cat_goals`
--
ALTER TABLE `report_cat_goals`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `report_cat_partners`
--
ALTER TABLE `report_cat_partners`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `report_cat_payment_types`
--
ALTER TABLE `report_cat_payment_types`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `report_cat_positions`
--
ALTER TABLE `report_cat_positions`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `report_cat_subgoals`
--
ALTER TABLE `report_cat_subgoals`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `report_cat_transactions`
--
ALTER TABLE `report_cat_transactions`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `report_cat_transaction_types`
--
ALTER TABLE `report_cat_transaction_types`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `report_courier_debt`
--
ALTER TABLE `report_courier_debt`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `report_fin_account_cats`
--
ALTER TABLE `report_fin_account_cats`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `report_fin_costs`
--
ALTER TABLE `report_fin_costs`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `report_fin_profit`
--
ALTER TABLE `report_fin_profit`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `report_logistics`
--
ALTER TABLE `report_logistics`
  ADD PRIMARY KEY (`id`),
  ADD KEY `shift_index` (`shift_id`),
  ADD KEY `courier_index` (`courier_id`),
  ADD KEY `drive_index` (`drive_id`);

--
-- Индексы таблицы `report_logistics_shift`
--
ALTER TABLE `report_logistics_shift`
  ADD PRIMARY KEY (`id`),
  ADD KEY `courier_index` (`courier_id`);

--
-- Индексы таблицы `report_period`
--
ALTER TABLE `report_period`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `report_pl`
--
ALTER TABLE `report_pl`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `review`
--
ALTER TABLE `review`
  ADD PRIMARY KEY (`review_id`);

--
-- Индексы таблицы `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `schedule_courier`
--
ALTER TABLE `schedule_courier`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `share`
--
ALTER TABLE `share`
  ADD PRIMARY KEY (`share_id`);

--
-- Индексы таблицы `sms_notification`
--
ALTER TABLE `sms_notification`
  ADD PRIMARY KEY (`sms_id`);

--
-- Индексы таблицы `statuses`
--
ALTER TABLE `statuses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `statuses_drive_type_fk` (`drive_type_id`),
  ADD KEY `statuses_drive_status_fk` (`drive_status_id`),
  ADD KEY `statuses_order_status_fk` (`order_status_id`);

--
-- Индексы таблицы `time_slot`
--
ALTER TABLE `time_slot`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tucan_extract`
--
ALTER TABLE `tucan_extract`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `type_courier`
--
ALTER TABLE `type_courier`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `units`
--
ALTER TABLE `units`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_unique_email` (`email`),
  ADD UNIQUE KEY `user_unique_username` (`username`);

--
-- Индексы таблицы `user_auth`
--
ALTER TABLE `user_auth`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_auth_provider_id` (`provider_id`),
  ADD KEY `user_auth_user_id` (`user_id`);

--
-- Индексы таблицы `user_old`
--
ALTER TABLE `user_old`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_email` (`email`),
  ADD UNIQUE KEY `user_username` (`username`),
  ADD KEY `user_role_id` (`role_id`);

--
-- Индексы таблицы `user_token`
--
ALTER TABLE `user_token`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_token_token` (`token`),
  ADD KEY `user_token_user_id` (`user_id`);

--
-- Индексы таблицы `warehouse`
--
ALTER TABLE `warehouse`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `working_time_courier`
--
ALTER TABLE `working_time_courier`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `work_zone`
--
ALTER TABLE `work_zone`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `work_zone_drive_limit`
--
ALTER TABLE `work_zone_drive_limit`
  ADD PRIMARY KEY (`id`),
  ADD KEY `work_zone_drive_limit_timeslot_fk` (`timeslot_id`),
  ADD KEY `work_zone_drive_limit_workzone_fk` (`work_zone_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `account_target`
--
ALTER TABLE `account_target`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;
--
-- AUTO_INCREMENT для таблицы `address_book`
--
ALTER TABLE `address_book`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5415;
--
-- AUTO_INCREMENT для таблицы `address_book_old`
--
ALTER TABLE `address_book_old`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=155;
--
-- AUTO_INCREMENT для таблицы `api_ats_order_status`
--
ALTER TABLE `api_ats_order_status`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1266;
--
-- AUTO_INCREMENT для таблицы `article_attachments`
--
ALTER TABLE `article_attachments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `article_categories`
--
ALTER TABLE `article_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `article_items`
--
ALTER TABLE `article_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=121;
--
-- AUTO_INCREMENT для таблицы `ats_data`
--
ALTER TABLE `ats_data`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40294;
--
-- AUTO_INCREMENT для таблицы `calc_calls`
--
ALTER TABLE `calc_calls`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2660;
--
-- AUTO_INCREMENT для таблицы `calc_periods`
--
ALTER TABLE `calc_periods`
  MODIFY `period_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=351;
--
-- AUTO_INCREMENT для таблицы `category`
--
ALTER TABLE `category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT для таблицы `change_log`
--
ALTER TABLE `change_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `change_log_client_data_field_types`
--
ALTER TABLE `change_log_client_data_field_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT для таблицы `change_log_fields`
--
ALTER TABLE `change_log_fields`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `change_object_types`
--
ALTER TABLE `change_object_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `change_types`
--
ALTER TABLE `change_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `checking_account`
--
ALTER TABLE `checking_account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=187;
--
-- AUTO_INCREMENT для таблицы `client`
--
ALTER TABLE `client`
  MODIFY `client_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13573;
--
-- AUTO_INCREMENT для таблицы `client_card`
--
ALTER TABLE `client_card`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=162;
--
-- AUTO_INCREMENT для таблицы `client_data_change_log`
--
ALTER TABLE `client_data_change_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `client_data_change_types`
--
ALTER TABLE `client_data_change_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `client_device`
--
ALTER TABLE `client_device`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=973;
--
-- AUTO_INCREMENT для таблицы `client_push_notification`
--
ALTER TABLE `client_push_notification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `client_review`
--
ALTER TABLE `client_review`
  MODIFY `client_review_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Код', AUTO_INCREMENT=1010;
--
-- AUTO_INCREMENT для таблицы `contractor`
--
ALTER TABLE `contractor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=265;
--
-- AUTO_INCREMENT для таблицы `cookie_user`
--
ALTER TABLE `cookie_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `couriers`
--
ALTER TABLE `couriers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=110;
--
-- AUTO_INCREMENT для таблицы `courier_actions`
--
ALTER TABLE `courier_actions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `courier_card`
--
ALTER TABLE `courier_card`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;
--
-- AUTO_INCREMENT для таблицы `courier_card_history`
--
ALTER TABLE `courier_card_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=126;
--
-- AUTO_INCREMENT для таблицы `courier_credit_statement`
--
ALTER TABLE `courier_credit_statement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=571;
--
-- AUTO_INCREMENT для таблицы `courier_payment_schemes`
--
ALTER TABLE `courier_payment_schemes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `courier_schedules`
--
ALTER TABLE `courier_schedules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `courier_selfie`
--
ALTER TABLE `courier_selfie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=142;
--
-- AUTO_INCREMENT для таблицы `courier_shifts`
--
ALTER TABLE `courier_shifts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `courier_to_region`
--
ALTER TABLE `courier_to_region`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=139;
--
-- AUTO_INCREMENT для таблицы `drives`
--
ALTER TABLE `drives`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32297;
--
-- AUTO_INCREMENT для таблицы `drive_results`
--
ALTER TABLE `drive_results`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `drive_statuses`
--
ALTER TABLE `drive_statuses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `drive_types`
--
ALTER TABLE `drive_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT для таблицы `drycleaners`
--
ALTER TABLE `drycleaners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT для таблицы `drycleaner_date_schedules`
--
ALTER TABLE `drycleaner_date_schedules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=178;
--
-- AUTO_INCREMENT для таблицы `drycleaner_orders`
--
ALTER TABLE `drycleaner_orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2975;
--
-- AUTO_INCREMENT для таблицы `drycleaner_positions_in_orders`
--
ALTER TABLE `drycleaner_positions_in_orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `drycleaner_pricelists`
--
ALTER TABLE `drycleaner_pricelists`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5481;
--
-- AUTO_INCREMENT для таблицы `dry_goods_in_orders`
--
ALTER TABLE `dry_goods_in_orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31532;
--
-- AUTO_INCREMENT для таблицы `faq`
--
ALTER TABLE `faq`
  MODIFY `faq_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Код вопроса', AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT для таблицы `faq_category`
--
ALTER TABLE `faq_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `good`
--
ALTER TABLE `good`
  MODIFY `good_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Код', AUTO_INCREMENT=315;
--
-- AUTO_INCREMENT для таблицы `goods_in_orders`
--
ALTER TABLE `goods_in_orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=235261;
--
-- AUTO_INCREMENT для таблицы `good_synonyms`
--
ALTER TABLE `good_synonyms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=115;
--
-- AUTO_INCREMENT для таблицы `holiday`
--
ALTER TABLE `holiday`
  MODIFY `holiday_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT для таблицы `leads`
--
ALTER TABLE `leads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=245801;
--
-- AUTO_INCREMENT для таблицы `lead_category`
--
ALTER TABLE `lead_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `lead_comment`
--
ALTER TABLE `lead_comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7945;
--
-- AUTO_INCREMENT для таблицы `lead_history`
--
ALTER TABLE `lead_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18469;
--
-- AUTO_INCREMENT для таблицы `lead_opinion`
--
ALTER TABLE `lead_opinion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `lead_results`
--
ALTER TABLE `lead_results`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT для таблицы `lead_type`
--
ALTER TABLE `lead_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT для таблицы `order`
--
ALTER TABLE `order`
  MODIFY `order_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14059;
--
-- AUTO_INCREMENT для таблицы `orders_x_drives`
--
ALTER TABLE `orders_x_drives`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=189319;
--
-- AUTO_INCREMENT для таблицы `order_cancel_log`
--
ALTER TABLE `order_cancel_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=392;
--
-- AUTO_INCREMENT для таблицы `order_discount_history`
--
ALTER TABLE `order_discount_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4175;
--
-- AUTO_INCREMENT для таблицы `order_invoice`
--
ALTER TABLE `order_invoice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6558;
--
-- AUTO_INCREMENT для таблицы `order_pays`
--
ALTER TABLE `order_pays`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9357;
--
-- AUTO_INCREMENT для таблицы `order_statuses`
--
ALTER TABLE `order_statuses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT для таблицы `order_update_log`
--
ALTER TABLE `order_update_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3555;
--
-- AUTO_INCREMENT для таблицы `order_warehouse_history`
--
ALTER TABLE `order_warehouse_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2722;
--
-- AUTO_INCREMENT для таблицы `our_team`
--
ALTER TABLE `our_team`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `param`
--
ALTER TABLE `param`
  MODIFY `param_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Код', AUTO_INCREMENT=152;
--
-- AUTO_INCREMENT для таблицы `payment_types`
--
ALTER TABLE `payment_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `photo`
--
ALTER TABLE `photo`
  MODIFY `photo_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Ид';
--
-- AUTO_INCREMENT для таблицы `promo_code`
--
ALTER TABLE `promo_code`
  MODIFY `promo_code_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Номер', AUTO_INCREMENT=63;
--
-- AUTO_INCREMENT для таблицы `promo_code_request`
--
ALTER TABLE `promo_code_request`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=126;
--
-- AUTO_INCREMENT для таблицы `promo_code_requests`
--
ALTER TABLE `promo_code_requests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4094;
--
-- AUTO_INCREMENT для таблицы `report_cat_bank_accounts`
--
ALTER TABLE `report_cat_bank_accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT для таблицы `report_cat_categories`
--
ALTER TABLE `report_cat_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT для таблицы `report_cat_client_types`
--
ALTER TABLE `report_cat_client_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `report_cat_companies`
--
ALTER TABLE `report_cat_companies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;
--
-- AUTO_INCREMENT для таблицы `report_cat_employees`
--
ALTER TABLE `report_cat_employees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT для таблицы `report_cat_founders`
--
ALTER TABLE `report_cat_founders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `report_cat_goals`
--
ALTER TABLE `report_cat_goals`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT для таблицы `report_cat_partners`
--
ALTER TABLE `report_cat_partners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=174;
--
-- AUTO_INCREMENT для таблицы `report_cat_payment_types`
--
ALTER TABLE `report_cat_payment_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `report_cat_positions`
--
ALTER TABLE `report_cat_positions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT для таблицы `report_cat_subgoals`
--
ALTER TABLE `report_cat_subgoals`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=87;
--
-- AUTO_INCREMENT для таблицы `report_cat_transactions`
--
ALTER TABLE `report_cat_transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `report_cat_transaction_types`
--
ALTER TABLE `report_cat_transaction_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `report_courier_debt`
--
ALTER TABLE `report_courier_debt`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6503;
--
-- AUTO_INCREMENT для таблицы `report_fin_account_cats`
--
ALTER TABLE `report_fin_account_cats`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT для таблицы `report_fin_costs`
--
ALTER TABLE `report_fin_costs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10983;
--
-- AUTO_INCREMENT для таблицы `report_fin_profit`
--
ALTER TABLE `report_fin_profit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6433;
--
-- AUTO_INCREMENT для таблицы `report_logistics`
--
ALTER TABLE `report_logistics`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41434;
--
-- AUTO_INCREMENT для таблицы `report_logistics_shift`
--
ALTER TABLE `report_logistics_shift`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=109311018;
--
-- AUTO_INCREMENT для таблицы `report_period`
--
ALTER TABLE `report_period`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT для таблицы `report_pl`
--
ALTER TABLE `report_pl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT для таблицы `review`
--
ALTER TABLE `review`
  MODIFY `review_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Код', AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT для таблицы `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=178;
--
-- AUTO_INCREMENT для таблицы `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `schedule_courier`
--
ALTER TABLE `schedule_courier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `share`
--
ALTER TABLE `share`
  MODIFY `share_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT для таблицы `sms_notification`
--
ALTER TABLE `sms_notification`
  MODIFY `sms_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40006;
--
-- AUTO_INCREMENT для таблицы `statuses`
--
ALTER TABLE `statuses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT для таблицы `time_slot`
--
ALTER TABLE `time_slot`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT для таблицы `tucan_extract`
--
ALTER TABLE `tucan_extract`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1983;
--
-- AUTO_INCREMENT для таблицы `type_courier`
--
ALTER TABLE `type_courier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `units`
--
ALTER TABLE `units`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT для таблицы `user_auth`
--
ALTER TABLE `user_auth`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `user_old`
--
ALTER TABLE `user_old`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT для таблицы `user_token`
--
ALTER TABLE `user_token`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `warehouse`
--
ALTER TABLE `warehouse`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT для таблицы `working_time_courier`
--
ALTER TABLE `working_time_courier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `work_zone`
--
ALTER TABLE `work_zone`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT для таблицы `work_zone_drive_limit`
--
ALTER TABLE `work_zone_drive_limit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=114;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `checking_account`
--
ALTER TABLE `checking_account`
  ADD CONSTRAINT `fk_Сhecking_account_Contractor_id` FOREIGN KEY (`contractor_id`) REFERENCES `contractor` (`id`);

--
-- Ограничения внешнего ключа таблицы `client_device`
--
ALTER TABLE `client_device`
  ADD CONSTRAINT `client_device_client_id_fk` FOREIGN KEY (`client_id`) REFERENCES `client` (`client_id`);

--
-- Ограничения внешнего ключа таблицы `client_push_notification`
--
ALTER TABLE `client_push_notification`
  ADD CONSTRAINT `client_push_notification_device_id_fk` FOREIGN KEY (`device_id`) REFERENCES `client_device` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `courier_card_history`
--
ALTER TABLE `courier_card_history`
  ADD CONSTRAINT `fk-courier_card_history-card_id` FOREIGN KEY (`card_id`) REFERENCES `courier_card` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk-courier_card_history-courier_id` FOREIGN KEY (`courier_id`) REFERENCES `couriers` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `courier_selfie`
--
ALTER TABLE `courier_selfie`
  ADD CONSTRAINT `fk_Courier_selfie_Courier_id__Couriers_Id` FOREIGN KEY (`courier_id`) REFERENCES `couriers` (`id`);

--
-- Ограничения внешнего ключа таблицы `courier_to_region`
--
ALTER TABLE `courier_to_region`
  ADD CONSTRAINT `fk_courier_id_ref_courier_id` FOREIGN KEY (`courier_id`) REFERENCES `couriers` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_region_id_ref_work_zone_id` FOREIGN KEY (`region_id`) REFERENCES `work_zone` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `leads`
--
ALTER TABLE `leads`
  ADD CONSTRAINT `fk_leads_opinion_id_id` FOREIGN KEY (`opinion_id`) REFERENCES `lead_opinion` (`id`),
  ADD CONSTRAINT `fk_leads_type_id_id` FOREIGN KEY (`type_id`) REFERENCES `lead_type` (`id`),
  ADD CONSTRAINT `fk_leads_user_id` FOREIGN KEY (`processing_at`) REFERENCES `user` (`id`);

--
-- Ограничения внешнего ключа таблицы `lead_history`
--
ALTER TABLE `lead_history`
  ADD CONSTRAINT `fk_lead_history_client_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`client_id`),
  ADD CONSTRAINT `fk_lead_history_lead_opinion_opinion_id` FOREIGN KEY (`opinion_id`) REFERENCES `lead_opinion` (`id`),
  ADD CONSTRAINT `fk_lead_history_lead_results_result_id` FOREIGN KEY (`result_id`) REFERENCES `lead_results` (`id`),
  ADD CONSTRAINT `fk_lead_history_lead_type_type_id` FOREIGN KEY (`type_id`) REFERENCES `lead_type` (`id`),
  ADD CONSTRAINT `fk_lead_history_user_created_by` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`);

--
-- Ограничения внешнего ключа таблицы `lead_results`
--
ALTER TABLE `lead_results`
  ADD CONSTRAINT `fk_lead_results_to_category` FOREIGN KEY (`category_id`) REFERENCES `lead_category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `lead_type`
--
ALTER TABLE `lead_type`
  ADD CONSTRAINT `fk_lead_type_id_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `lead_type` (`id`);

--
-- Ограничения внешнего ключа таблицы `order_discount_history`
--
ALTER TABLE `order_discount_history`
  ADD CONSTRAINT `order_discount_order_fk` FOREIGN KEY (`order_id`) REFERENCES `order` (`order_id`);

--
-- Ограничения внешнего ключа таблицы `report_logistics`
--
ALTER TABLE `report_logistics`
  ADD CONSTRAINT `courier_reports_logistic_fk` FOREIGN KEY (`courier_id`) REFERENCES `couriers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `drive_reports_logistic_fk` FOREIGN KEY (`drive_id`) REFERENCES `drives` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `report_logistics_shift`
--
ALTER TABLE `report_logistics_shift`
  ADD CONSTRAINT `courier_reports_logistic_shift_fk` FOREIGN KEY (`courier_id`) REFERENCES `couriers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `work_zone_drive_limit`
--
ALTER TABLE `work_zone_drive_limit`
  ADD CONSTRAINT `work_zone_drive_limit_timeslot_fk` FOREIGN KEY (`timeslot_id`) REFERENCES `time_slot` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `work_zone_drive_limit_workzone_fk` FOREIGN KEY (`work_zone_id`) REFERENCES `work_zone` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
