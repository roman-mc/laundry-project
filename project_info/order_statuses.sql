-- phpMyAdmin SQL Dump
-- version 4.7.2
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Ноя 01 2017 г., 14:15
-- Версия сервера: 5.7.16-0ubuntu0.16.04.1-log
-- Версия PHP: 7.0.23-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `getairo`
--

-- --------------------------------------------------------

--
-- Структура таблицы `order_statuses`
--

CREATE TABLE `order_statuses` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `color` varchar(255) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1'
) ENGINE=InnoDB AVG_ROW_LENGTH=744 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `order_statuses`
--

INSERT INTO `order_statuses` (`id`, `name`, `color`, `status`) VALUES
(1, 'Новый', 'label-primary', 1),
(2, 'Сбор', 'label-info', 0),
(3, 'Доставка', 'label-warning', 0),
(4, 'Сделано', 'label-success', 0),
(5, 'В процессе', 'label-info', 0),
(12, 'Запланирован', 'label-info', 1),
(13, 'Сбор', 'label-info', 1),
(14, 'Собран', 'label-info', 1),
(15, 'На фабрике', 'label-info', 1),
(16, 'Исполняется на фабрике', 'label-info', 1),
(17, 'Ожидает сбора с фабрики', 'label-info', 1),
(18, 'Сбор с фабрики', 'label-info', 1),
(19, 'Доставка клиенту', 'label-warning', 1),
(20, 'Завершен', 'label-success', 1),
(21, 'Отменен', 'label-danger', 1),
(22, 'Черновик', 'label-default', 1),
(23, 'Распределен курьеру', 'label-info', 1),
(24, 'Отмена при сборе', 'label-danger', 1),
(25, 'Отмена при доставке', 'label-danger', 1),
(26, 'Доставка на фабрику', 'label-info', 1),
(27, 'Отмена на фабрике', 'label-danger', 1),
(28, 'Собран с фабрики', 'label-info', 1),
(29, 'Собран частично', 'label-info', 1),
(30, 'Доставлен частично', 'label-info', 1),
(31, 'Сбор со Склада', 'label-info', 1),
(32, 'Доставка на Склад', 'label-info', 1),
(33, 'На складе', 'label-info', 1),
(34, 'Собран со Склада', 'label-info', 1),
(35, 'Выдан на фабрике', 'label-info', 1),
(36, 'Ожидает оплаты', 'label-warning', 1);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `order_statuses`
--
ALTER TABLE `order_statuses`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `order_statuses`
--
ALTER TABLE `order_statuses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
