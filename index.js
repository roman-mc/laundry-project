import {AppRegistry, Platform} from 'react-native';
import App from './App';

// TODO: Fix app key
AppRegistry.registerComponent(Platform.OS === 'ios' ? 'AiroClient' : 'Airo', () => App);
