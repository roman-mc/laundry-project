/* @flow */
/**
 *
 * Anything here is for suggestion addresses and PickAddress container
 */

export class Address {
  address: string;

  details: {
    kvartira: string,
    podezd: string,
    etaj: string,
    domofon: string
  };
  comment: string;

  constructor (address:string, kvartira?:string, podezd?:string, etaj?:string, domofon?:string, comment?: string) {
    this.address = address;
    this.comment = comment || '';

    this.details = {
      kvartira: kvartira || '',
      podezd:   podezd || '',
      etaj:     etaj || '',
      domofon:  domofon || '',
    };

  }
}

/**
 * @param  {Array<string>} addresses
 * @return {Array<Address>}
 */
export const convertStringAddresses = (addresses: Array<string>):Array<Address> =>
  addresses.map(address => new Address(address));

export const convertServerAddress = (address: Object):Address =>
  new Address(
    address.address || '',
    address['кв/офис'],
    address['подъезд'],
    address['этаж'],
    address['домофон'],
    address.comment
  );

/**
 * @param  {Array<{address: string, details: Object?}>} addresses
 * @return {Array<Address>}
 */
export const convertServerAddresses = (addresses: Array<Object>):Array<Address> =>
  addresses.map(convertServerAddress);
