/**
 * Parses suggested addresses returned by yandex api
 * @param  {String} string
 * @return {Array<String>}
 */
export const parseSuggestedAddresses = string => {
  const addresses = [];

  if (typeof string !== 'string') {
    console.error('This is not string!', string);
    return addresses;
  }

  // TODO: make sure that this response will have always such structure
  try {
    const pureJsonableString = string.slice(string.indexOf('(') + 1, string.lastIndexOf(')'));
    const arrays = JSON.parse(pureJsonableString)[1];

    for (const o of arrays) {
      addresses.push(o[2].replace('Россия, ', ''));
    }
  }
  catch (e) {
    console.error(e);
  }

  return addresses;
};
