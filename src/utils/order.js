const blue = '#007bff';
const indigo = '#6610f2';
const purple = '#6f42c1';
const pink = '#e83e8c';
const red = '#dc3545';
const orange = '#fd7e14';
const yellow = '#ffc107';
const green = '#28a745';
const teal = '#20c997';
const cyan = '#17a2b8';
const white = '#fff';
const gray = '#868e96';
const grayDark = '#343a40';
const primary = '#89d0ff';
const secondary = '#868e96';
const success = '#823aff';
const info = '#17a2b8';
const warning = '#ffc107';
const danger = '#c33737';
const light = '#f8f9fa';
const dark = '#343a40';
const black = '#000000';

const statuses = [
  [1, 'Новый', 'label-primary'],
  [2, 'Сбор', 'label-info'],
  [3, 'Доставка', 'label-warning'],
  [4, 'Сделано', 'label-success'],
  [5, 'В процессе', 'label-info'],
  [12, 'Запланирован', 'label-info'],
  [13, 'Едем за вещами', 'label-info'],
  [14, 'Вещи собраны', 'label-info'],
  [15, 'На фабрике', 'label-info'],
  [16, 'Исполняется на фабрике', 'label-info'],
  [17, 'Ожидает сбора с фабрики', 'label-info'],
  [18, 'Сбор с фабрики', 'label-info'],
  [19, 'Доставляем вещи', 'label-warning'],
  [20, 'Отмена', 'label-success'],
  [21, 'Отменен', 'label-danger'],
  [22, 'Черновик', 'label-default'],
  [23, 'Распределен курьеру', 'label-info'],
  [24, 'Отмена', 'label-danger'],
  [25, 'Отмена при доставке', 'label-danger'],
  [26, 'Доставка на фабрику', 'label-info'],
  [27, 'Отмена на фабрике', 'label-danger'],
  [28, 'Собран с фабрики', 'label-info'],
  [29, 'Собран частично', 'label-info'],
  [30, 'Доставлен частично', 'label-info'],
  [31, 'Сбор со Склада', 'label-info'],
  [32, 'Доставка на Склад', 'label-info'],
  [33, 'На складе', 'label-info'],
  [34, 'Собран со Склада', 'label-info'],
  [35, 'Выдан на фабрике', 'label-info'],
  [36, 'Ожидает оплаты', 'label-warning']
];

const getColorByLabel = label => {
  switch (label) {
    case 'label-primary':
      return primary;

    case 'label-success':
      return success;

    case 'label-danger':
      return danger;

    case 'label-warning':
      return warning;

    case 'label-info':
      return info;

    case 'label-default':
      return gray;

    // TEMP: on developemnt stage
    // TODO: remove me later and set some default color
    default:
      return white;

  }
};

export const getPickedDay = dates =>
  dates.find(d => d.selected);


export const getPickedTime = date => {
  if (!date.selected) throw new Error('date is not selected!');

  const selectedTime = date.times.find(t => t.checked);

  return selectedTime.time;
};

/**
 * @param  {string} statusType
 * @return {string} color, e.g. #fooba5
 */
export const getColorByStatusType = statusType => getColorByLabel(statusType);

const closedOrderStatusRegExp = /отмен|завершен/i;

/**
 * Checks if status is closed
 * @param  {string}  status
 * @return {boolean}
 */
export const isClosedOrderStatus = status =>
  closedOrderStatusRegExp.test(status);
