// @flow
const capitalize = (str: string) =>
  str[0].toUpperCase() + str.slice(1);


export const toLocaleString = (date: Date): string => {
  const timeStr = date.toLocaleDateString('ru', {
    weekday: 'long',
    day:     '2-digit',
    month:   'long'
  });

  return timeStr.split(' ')
                .map(capitalize)
                .join(' ');

};

export const weekDays = [
  'Воскресенье',
  'Понедельник',
  'Вторник',
  'Среда',
  'Четверг',
  'Пятница',
  'Суббота'
];

export const months = [
  'Января',
  'Февраля',
  'Марта',
  'Апреля',
  'Мая',
  'Июня',
  'Июля',
  'Августа',
  'Сентября',
  'Октября',
  'Ноября',
  'Декабря',
];

/**
 * @param  {Date} date
 * @return {string}
 */
export const toLocaleString2 = (date) => {
  const month = date.getMonth();
  const monthDay = date.getDate();
  const weekDay = date.getDay();

  return `${weekDays[weekDay]}, ${monthDay} ${months[month]}`;
};

export const shortWeekDays = [
  'вс',
  'пн',
  'вт',
  'ср',
  'чт',
  'пт',
  'сб'
];

export const shortMonths = [
  'янв',
  'фев',
  'мрт',
  'апр',
  'май',
  'июн',
  'июл',
  'авг',
  'сен',
  'окт',
  'нбр',
  'дек',
];

/**
 * @param  {Date} date
 * @param  {string} time
 * @return {string} in format 'вт, 22 окт 2017 с 20:00 до 21:00'
 */
export const getShortDateTime = (date, time) => {
  const weekDay = date.getDay();
  const monthDay = date.getDate();
  const month = date.getMonth();

  const [startTime, endTime] = time.split(' - ');

  const monthString = shortMonths[month];

  return `${shortWeekDays[weekDay]}, ${monthDay} ${monthString}, с ${startTime} до ${endTime}`;
};

/**
 * @param  {String} string
 * @return {Date}
 */
export const convertFromServerDate = (string) => {
  const [day, month, year] = string.split('-');

  return new Date(year, month - 1, day);
};

const precedeZero = s => s.length === 1 ? `0${s}` : s;

/**
 * @param  {Date} date
 * @return {string} return `22.10.2017`
 */
export const getShortDate = date => {
  const day = date.getDate();
  const month = date.getMonth();
  const year = date.getFullYear();

  return `${precedeZero(String(day))}.${precedeZero(String(month + 1))}.${precedeZero(String(year))}`;
};
