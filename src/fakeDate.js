export const ableTimeslots = {
  "collected": {
    "21-09-2017": "disabled-all",
    "22-09-2017": {
      "0": {
        "time":     "10:00 - 14:00",
        "disabled": false,
        "checked":  true,
        "shift":    2,
        "day_type": 1,
        "loading":  "unknown"
      },
      "1": {
        "time":     "15:00 - 19:00",
        "disabled": false,
        "checked":  false,
        "shift":    3,
        "day_type": 1,
        "loading":  "unknown"
      },
      "2": {
        "time":     "20:00 - 21:00",
        "disabled": false,
        "checked":  false,
        "shift":    4,
        "day_type": 1,
        "loading":  "unknown"
      },
      "3": {
        "time":     "21:00 - 22:00",
        "disabled": false,
        "checked":  false,
        "shift":    4,
        "day_type": 1,
        "loading":  "unknown"
      },
      "4": {
        "time":     "22:00 - 23:00",
        "disabled": false,
        "checked":  false,
        "shift":    4,
        "day_type": 1,
        "loading":  "unknown"
      },
      "selected": true
    },
    "23-09-2017": {
      "0": {
        "time":     "10:00 - 14:00",
        "disabled": false,
        "checked":  false,
        "shift":    2,
        "day_type": 2,
        "loading":  "unknown"
      },
      "1": {
        "time":     "15:00 - 19:00",
        "disabled": false,
        "checked":  false,
        "shift":    3,
        "day_type": 2,
        "loading":  "unknown"
      },
      "2": {
        "time":     "20:00 - 21:00",
        "disabled": false,
        "checked":  false,
        "shift":    4,
        "day_type": 2,
        "loading":  "unknown"
      },
      "3": {
        "time":     "21:00 - 22:00",
        "disabled": false,
        "checked":  false,
        "shift":    4,
        "day_type": 2,
        "loading":  "unknown"
      },
      "4": {
        "time":     "22:00 - 23:00",
        "disabled": false,
        "checked":  false,
        "shift":    4,
        "day_type": 2,
        "loading":  "unknown"
      }
    },
    "24-09-2017": {
      "0": {
        "time":     "10:00 - 14:00",
        "disabled": false,
        "checked":  false,
        "shift":    2,
        "day_type": 2,
        "loading":  "unknown"
      },
      "1": {
        "time":     "15:00 - 19:00",
        "disabled": false,
        "checked":  false,
        "shift":    3,
        "day_type": 2,
        "loading":  "unknown"
      },
      "2": {
        "time":     "20:00 - 21:00",
        "disabled": false,
        "checked":  false,
        "shift":    4,
        "day_type": 2,
        "loading":  "unknown"
      },
      "3": {
        "time":     "21:00 - 22:00",
        "disabled": false,
        "checked":  false,
        "shift":    4,
        "day_type": 2,
        "loading":  "unknown"
      },
      "4": {
        "time":     "22:00 - 23:00",
        "disabled": false,
        "checked":  false,
        "shift":    4,
        "day_type": 2,
        "loading":  "unknown"
      }
    },
    "25-09-2017": {
      "0": {
        "time":     "10:00 - 14:00",
        "disabled": false,
        "checked":  false,
        "shift":    2,
        "day_type": 1,
        "loading":  "unknown"
      },
      "1": {
        "time":     "15:00 - 19:00",
        "disabled": false,
        "checked":  false,
        "shift":    3,
        "day_type": 1,
        "loading":  "unknown"
      },
      "2": {
        "time":     "20:00 - 21:00",
        "disabled": false,
        "checked":  false,
        "shift":    4,
        "day_type": 1,
        "loading":  "unknown"
      },
      "3": {
        "time":     "21:00 - 22:00",
        "disabled": false,
        "checked":  false,
        "shift":    4,
        "day_type": 1,
        "loading":  "unknown"
      },
      "4": {
        "time":     "22:00 - 23:00",
        "disabled": false,
        "checked":  false,
        "shift":    4,
        "day_type": 1,
        "loading":  "unknown"
      }
    },
    "26-09-2017": {
      "0": {
        "time":     "10:00 - 14:00",
        "disabled": false,
        "checked":  false,
        "shift":    2,
        "day_type": 1,
        "loading":  "unknown"
      },
      "1": {
        "time":     "15:00 - 19:00",
        "disabled": false,
        "checked":  false,
        "shift":    3,
        "day_type": 1,
        "loading":  "unknown"
      },
      "2": {
        "time":     "20:00 - 21:00",
        "disabled": false,
        "checked":  false,
        "shift":    4,
        "day_type": 1,
        "loading":  "unknown"
      },
      "3": {
        "time":     "21:00 - 22:00",
        "disabled": false,
        "checked":  false,
        "shift":    4,
        "day_type": 1,
        "loading":  "unknown"
      },
      "4": {
        "time":     "22:00 - 23:00",
        "disabled": false,
        "checked":  false,
        "shift":    4,
        "day_type": 1,
        "loading":  "unknown"
      }
    },
    "27-09-2017": {
      "0": {
        "time":     "10:00 - 14:00",
        "disabled": false,
        "checked":  false,
        "shift":    2,
        "day_type": 1,
        "loading":  "unknown"
      },
      "1": {
        "time":     "15:00 - 19:00",
        "disabled": false,
        "checked":  false,
        "shift":    3,
        "day_type": 1,
        "loading":  "unknown"
      },
      "2": {
        "time":     "20:00 - 21:00",
        "disabled": false,
        "checked":  false,
        "shift":    4,
        "day_type": 1,
        "loading":  "unknown"
      },
      "3": {
        "time":     "21:00 - 22:00",
        "disabled": false,
        "checked":  false,
        "shift":    4,
        "day_type": 1,
        "loading":  "unknown"
      },
      "4": {
        "time":     "22:00 - 23:00",
        "disabled": false,
        "checked":  false,
        "shift":    4,
        "day_type": 1,
        "loading":  "unknown"
      }
    }
  },
  "delivery": {
    "24-09-2017": {
      "0": {
        "time":     "10:00 - 14:00",
        "disabled": false,
        "checked":  true,
        "shift":    2,
        "day_type": 2,
        "loading":  "unknown"
      },
      "1": {
        "time":     "15:00 - 19:00",
        "disabled": false,
        "checked":  false,
        "shift":    3,
        "day_type": 2,
        "loading":  "unknown"
      },
      "2": {
        "time":     "20:00 - 21:00",
        "disabled": false,
        "checked":  false,
        "shift":    4,
        "day_type": 2,
        "loading":  "unknown"
      },
      "3": {
        "time":     "21:00 - 22:00",
        "disabled": false,
        "checked":  false,
        "shift":    4,
        "day_type": 2,
        "loading":  "unknown"
      },
      "4": {
        "time":     "22:00 - 23:00",
        "disabled": false,
        "checked":  false,
        "shift":    4,
        "day_type": 2,
        "loading":  "unknown"
      },
      "selected": true
    },
    "25-09-2017": {
      "0": {
        "time":     "10:00 - 14:00",
        "disabled": false,
        "checked":  false,
        "shift":    2,
        "day_type": 1,
        "loading":  "unknown"
      },
      "1": {
        "time":     "15:00 - 19:00",
        "disabled": false,
        "checked":  false,
        "shift":    3,
        "day_type": 1,
        "loading":  "unknown"
      },
      "2": {
        "time":     "20:00 - 21:00",
        "disabled": false,
        "checked":  false,
        "shift":    4,
        "day_type": 1,
        "loading":  "unknown"
      },
      "3": {
        "time":     "21:00 - 22:00",
        "disabled": false,
        "checked":  false,
        "shift":    4,
        "day_type": 1,
        "loading":  "unknown"
      },
      "4": {
        "time":     "22:00 - 23:00",
        "disabled": false,
        "checked":  false,
        "shift":    4,
        "day_type": 1,
        "loading":  "unknown"
      }
    },
    "26-09-2017": {
      "0": {
        "time":     "10:00 - 14:00",
        "disabled": false,
        "checked":  false,
        "shift":    2,
        "day_type": 1,
        "loading":  "unknown"
      },
      "1": {
        "time":     "15:00 - 19:00",
        "disabled": false,
        "checked":  false,
        "shift":    3,
        "day_type": 1,
        "loading":  "unknown"
      },
      "2": {
        "time":     "20:00 - 21:00",
        "disabled": false,
        "checked":  false,
        "shift":    4,
        "day_type": 1,
        "loading":  "unknown"
      },
      "3": {
        "time":     "21:00 - 22:00",
        "disabled": false,
        "checked":  false,
        "shift":    4,
        "day_type": 1,
        "loading":  "unknown"
      },
      "4": {
        "time":     "22:00 - 23:00",
        "disabled": false,
        "checked":  false,
        "shift":    4,
        "day_type": 1,
        "loading":  "unknown"
      }
    },
    "27-09-2017": {
      "0": {
        "time":     "10:00 - 14:00",
        "disabled": false,
        "checked":  false,
        "shift":    2,
        "day_type": 1,
        "loading":  "unknown"
      },
      "1": {
        "time":     "15:00 - 19:00",
        "disabled": false,
        "checked":  false,
        "shift":    3,
        "day_type": 1,
        "loading":  "unknown"
      },
      "2": {
        "time":     "20:00 - 21:00",
        "disabled": false,
        "checked":  false,
        "shift":    4,
        "day_type": 1,
        "loading":  "unknown"
      },
      "3": {
        "time":     "21:00 - 22:00",
        "disabled": false,
        "checked":  false,
        "shift":    4,
        "day_type": 1,
        "loading":  "unknown"
      },
      "4": {
        "time":     "22:00 - 23:00",
        "disabled": false,
        "checked":  false,
        "shift":    4,
        "day_type": 1,
        "loading":  "unknown"
      }
    },
    "28-09-2017": {
      "0": {
        "time":     "10:00 - 14:00",
        "disabled": false,
        "checked":  false,
        "shift":    2,
        "day_type": 1,
        "loading":  "unknown"
      },
      "1": {
        "time":     "15:00 - 19:00",
        "disabled": false,
        "checked":  false,
        "shift":    3,
        "day_type": 1,
        "loading":  "unknown"
      },
      "2": {
        "time":     "20:00 - 21:00",
        "disabled": false,
        "checked":  false,
        "shift":    4,
        "day_type": 1,
        "loading":  "unknown"
      },
      "3": {
        "time":     "21:00 - 22:00",
        "disabled": false,
        "checked":  false,
        "shift":    4,
        "day_type": 1,
        "loading":  "unknown"
      },
      "4": {
        "time":     "22:00 - 23:00",
        "disabled": false,
        "checked":  false,
        "shift":    4,
        "day_type": 1,
        "loading":  "unknown"
      }
    },
    "29-09-2017": {
      "0": {
        "time":     "10:00 - 14:00",
        "disabled": false,
        "checked":  false,
        "shift":    2,
        "day_type": 1,
        "loading":  "unknown"
      },
      "1": {
        "time":     "15:00 - 19:00",
        "disabled": false,
        "checked":  false,
        "shift":    3,
        "day_type": 1,
        "loading":  "unknown"
      },
      "2": {
        "time":     "20:00 - 21:00",
        "disabled": false,
        "checked":  false,
        "shift":    4,
        "day_type": 1,
        "loading":  "unknown"
      },
      "3": {
        "time":     "21:00 - 22:00",
        "disabled": false,
        "checked":  false,
        "shift":    4,
        "day_type": 1,
        "loading":  "unknown"
      },
      "4": {
        "time":     "22:00 - 23:00",
        "disabled": false,
        "checked":  false,
        "shift":    4,
        "day_type": 1,
        "loading":  "unknown"
      }
    },
    "30-09-2017": {
      "0": {
        "time":     "10:00 - 14:00",
        "disabled": false,
        "checked":  false,
        "shift":    2,
        "day_type": 2,
        "loading":  "unknown"
      },
      "1": {
        "time":     "15:00 - 19:00",
        "disabled": false,
        "checked":  false,
        "shift":    3,
        "day_type": 2,
        "loading":  "unknown"
      },
      "2": {
        "time":     "20:00 - 21:00",
        "disabled": false,
        "checked":  false,
        "shift":    4,
        "day_type": 2,
        "loading":  "unknown"
      },
      "3": {
        "time":     "21:00 - 22:00",
        "disabled": false,
        "checked":  false,
        "shift":    4,
        "day_type": 2,
        "loading":  "unknown"
      },
      "4": {
        "time":     "22:00 - 23:00",
        "disabled": false,
        "checked":  false,
        "shift":    4,
        "day_type": 2,
        "loading":  "unknown"
      }
    }
  }
};
