import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet
} from 'react-native';
import PropTypes from 'prop-types';

import {color} from '../styles';

class Tabs extends Component {
  render () {
    if (this.props.tabIndex > this.props.tabs.length || this.props.tabIndex < 0) {
      throw RangeError(`${this.props.tabIndex}`);
    }
    const {tabIndex, style} = this.props;

    return (
      <View style={[styles.tabs, style]}>
        {this.props.tabs.map((tab, i) => (
          <TouchableOpacity
            style={[styles.tab, i === tabIndex ? styles.tabSelected : null]}
            onPress={() => this.props.onTabIndexChange(i)}
            key={tab}
          >
            <Text style={styles.tabText}>{tab}</Text>
          </TouchableOpacity>
        ))}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  tabs: {
    backgroundColor:   color.white,
    flexDirection:     'row',
    width:             '100%',
    justifyContent:    'space-around',
    borderTopColor:    '#d5d5d5',
    borderBottomColor: '#d5d5d5',
    borderTopWidth:    1,
    borderBottomWidth: 1
  },
  tab: {
    flexBasis: '50%'
  },
  tabSelected: {
    borderBottomColor: color.airo,
    borderBottomWidth: 2
  },
  tabText: {
    paddingTop:    10,
    paddingBottom: 10,
    textAlign:     'center'
  }
});

Tabs.propTypes = {
  tabs: PropTypes.arrayOf(
    PropTypes.string.isRequired
  ).isRequired,
  tabIndex:         PropTypes.number.isRequired,
  onTabIndexChange: PropTypes.func.isRequired,
  style:            PropTypes.any
};

export default Tabs;
