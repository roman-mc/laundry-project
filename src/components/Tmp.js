import React from 'react';
import { StyleSheet, Text, View, Alert, TouchableOpacity } from 'react-native';
// import { BarCodeScanner, Permissions } from 'expo';
// import {Octicons} from '@expo/vector-icons';

import AutoSuggest from '../containers/PickAddress/AutoSuggest';
import Header from './Header';

export default class BarcodeScannerExample extends React.Component {
  render () {
    return (
      <View style={{flex: 1}}>
        <Header
          left={
            <TouchableOpacity style={{padding: 10, marginLeft: 5}}>
              {/* <Octicons style={[{fontSize: 20, color: '#00adf5'}]} name='x' /> */}
              <Text>123123</Text>
            </TouchableOpacity>
          }
          // onLeftPress={() => undefined}
          title='Укажите адрес'
        />
        <AutoSuggest
          // onPress={this.onAddressPress}
          data={[]}
          // inputValue={}
          // onChangeText={this.state.modal.onChangeText}
          // hintShowed={this.state.modal.hintShowed}
        />
      </View>
    );
  }
}
