import React from 'react';
import {
  Text,
  StyleSheet,
  ActivityIndicator
} from 'react-native';

const TextHint = props =>
  <Text {...props} style={[styles.layout, styles.text, props.style]}>
    {props.children}
  </Text>;

const styles = StyleSheet.create({
  text: {
    textAlign: 'center',
    color:     'rgb(232, 51, 51)',
    fontSize:  14,
  },
  layout: {
    marginBottom: 8,
    marginTop:    5,
  }
});

TextHint.propTypes = {
  ...Text.propTypes,
};

export default TextHint;
