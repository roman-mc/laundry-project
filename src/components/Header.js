// TODO: publish this component to npm and name it react-native-screen-header
import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    Platform,
    TouchableOpacity
} from 'react-native';
import PropTypes from 'prop-types';
const APPBAR_HEIGHT = Platform.OS === 'ios' ? 44 : 56;
const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : 0;
const TITLE_OFFSET = Platform.OS === 'ios' ? 70 : 40;


class Header extends Component {
  constructor (props) {
    super(props);

    this.state = {
    };
  }

  renderLeft () {
    if (!this.props.left) return null;

    return (
      <View style={[styles.item, styles.left]}>
        {this.props.left}
      </View>
    );
  }

  renderTitle () {
    const style = {};

    if (Platform.OS === 'android') {
      if (!this.props.left) {
        style.left = 0;
      }
      if (!this.props.right) {
        style.right = 0;
      }
    }

    return (
      <View style={[styles.item, styles.title]}>
        <Text
          style={[style.titleText, style, {fontSize: 18}]}
          numberOfLines={1}
        >
          {this.props.title}
        </Text>
      </View>
    );
  }

  renderRight () {
    if (!this.props.right) return null;

    return (
      <View style={[styles.item, styles.right]}>
        {this.props.right}
      </View>
    );
  }

  renderHeader () {
    const title = this.renderTitle();
    const left = this.renderLeft();
    const right = this.renderRight();

    return (
      <View style={[StyleSheet.absoluteFill, styles.header]}>
        {left}
        {title}
        {right}
      </View>
    );
  }

  render () {
    return (
      <View style={styles.container}>
        <View style={styles.appBar}>
          {this.renderHeader()}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: '#d7d7dc',
    paddingTop:        STATUSBAR_HEIGHT,
    backgroundColor:   Platform.OS === 'ios' ? '#EFEFF2' : '#FFF',
    height:            STATUSBAR_HEIGHT + APPBAR_HEIGHT,
    shadowColor:       'black',
    shadowOpacity:     0.1,
    shadowRadius:      StyleSheet.hairlineWidth,
    shadowOffset:      {
      height: StyleSheet.hairlineWidth,
    },
    elevation: 4
  },
  title: {
    bottom:     0,
    left:       TITLE_OFFSET,
    right:      TITLE_OFFSET,
    top:        0,
    position:   'absolute',
    alignItems: Platform.OS === 'ios' ? 'center' : 'flex-start',
  },
  left: {
    left:     0,
    bottom:   0,
    top:      0,
    position: 'absolute',
  },
  right: {
    right:    0,
    bottom:   0,
    top:      0,
    position: 'absolute',
  },
  item: {
    justifyContent:  'center',
    alignItems:      'center',
    backgroundColor: 'transparent',
  },
  appBar: {
    flex: 1,
  },
  header: {
    flexDirection: 'row',
  },
  titleText: {
    fontSize:         Platform.OS === 'ios' ? 17 : 18,
    fontWeight:       Platform.OS === 'ios' ? '600' : '500',
    color:            'rgba(0, 0, 0, .9)',
    textAlign:        Platform.OS === 'ios' ? 'center' : 'left',
    marginHorizontal: 16,
  }
});

// TODO: propTypes
Header.propTypes = {
  title: PropTypes.string,
  right: PropTypes.any,
  left:  PropTypes.any
};

export default Header;
