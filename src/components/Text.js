import React from 'react';
import {
  Text,
  StyleSheet,
} from 'react-native';
import PropTypes from 'prop-types';

const AText = props => {
  const {style, ...otherProps} = props;

  return (
    <Text
      style={[styles.text, style]}
      {...otherProps}
    />
  );
};

AText.propTypes = {
  style: Text.propTypes.style
};

const styles = StyleSheet.create({
  text: {
    fontFamily: 'FSElliotPro'
  }
});

export default AText;
