import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  StyleSheet,
  ViewPropTypes
} from 'react-native';

class Hr extends Component {
  render () {
    return (
      <View style={{
        flexDirection: 'row',
        alignItems:    'center',
      }}>
        <View style={[styles.line, this.props.style]}
        >

        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  line: {
    flex:            1,
    height:          StyleSheet.hairlineWidth,
    backgroundColor: '#000',
    marginTop:       10,
    marginBottom:    10,
    width:           '100%'
  }
});

Hr.propTypes = {
  ...ViewPropTypes
};
export default Hr;
