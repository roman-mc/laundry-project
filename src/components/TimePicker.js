import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    ScrollView
} from 'react-native';
import PropTypes from 'prop-types';

import Button from './Button';
import {width, button, buttonDefault} from '../styles';
import Header from './Header';
import {getPickedDay} from '../utils';

class TimePicker extends Component {
  constructor (props) {
    super(props);

  }

  render () {
    const dayTimes = [];
    const eveningTimes = [];

    const date = getPickedDay(this.props.data);

    for (const time of date.times) {
      if (time.disabled) continue;
      const timeDisabled = time.loading === 'maximum';

      const button = (
        <Button
          style={[
            styles.button,
            time.checked ? styles.buttonSelected
              : timeDisabled ? styles.buttonDisabled
              : null,
          ]}
          textStyle={[
            styles.buttonText,
            time.checked ? styles.buttonSelectedText
              : timeDisabled ? styles.buttonDisabledText
              : null
          ]}
          onPress={() => this.props.onPress(time.time)}
          isDisabled={timeDisabled}
          key={time.time}
        >
          {time.time}
        </Button>
      );

      if (time.shift < 4) {
        dayTimes.push(button);
      }
      else {
        eveningTimes.push(button);
      }
    }

    return (
      <ScrollView contentContainerStyle={styles.container}>
        {
          dayTimes.length > 0
          ? <View style={styles.timeSection}>
              <Text style={styles.title}>Днем</Text>
              <View style={styles.buttons}>
                {dayTimes}
              </View>
            </View>
          : null
        }
        {
          eveningTimes.length > 0
          ? <View style={[styles.timeSection, styles.secondSection]}>
              <Text style={styles.title}>Вечером</Text>
              <View style={styles.buttons}>
                {eveningTimes}
              </View>
            </View>
          : null
        }
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    // flex:       1,
    // alignItems: 'center',
    // paddingTop: 30
  },
  timeSection: {
    alignItems: 'center'
  },
  secondSection: {
    // marginTop: 10
  },
  button: {
    ...buttonDefault.container,
    width:     '60%',
    marginTop: 5
  },
  title: {
    marginTop:    15,
    marginBottom: 15,
    fontSize:     19
  },
  buttonSelected: {
    ...button.container
  },
  buttonSelectedText: {
    ...button.text
  },
  buttonDisabled: {
    // backgroundColor: 'rgb(180, 180, 180)'
  },
  buttonDisabledText: {

  },
  buttonText: {
    ...buttonDefault.text
  }
});

TimePicker.propTypes = {
  onPress: PropTypes.func.isRequired,
  data:    PropTypes.array.isRequired
};

export default TimePicker;
