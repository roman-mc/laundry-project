import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Platform,
  Image,
  Alert,
  TouchableHighlight,
  StatusBar
} from 'react-native';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';

import {resetAuth} from '../store/actions';
import {color} from '../styles';

const exitNavigation = {
  text:       'Выход',
  iconSource: require('../icons/exit.png')
};

const Hr = () => (
  <View
    style={{
      height:          StyleSheet.hairlineWidth,
      width:           '100%',
      marginTop:       5,
      marginBottom:    10,
      backgroundColor: '#BDBDBD'
    }}
  />
);

const phoneRegExp = /\+7(\d{3})(\d{3})(\d{2})(\d{2})/;

class DrawerContent extends Component {
  constructor (props) {
    super(props);

    this.state = {};
  }

  /**
   * @param  {Object} item
   * @param  {string} item.text
   * @param  {string} item.href
   * @param  {number} item.iconSource
   * @return {Element}
   */
  renderNavigationItem (item) {
    return (
      <TouchableOpacity
        key={item.href}
        onPress={() =>
          this.props.navigation.navigate(item.href)
        }
        style={styles.linkContainer}
      >
        <Image source={item.iconSource}/>
        <Text style={styles.linkText}>{item.text}</Text>
      </TouchableOpacity>
    );
  }
  renderNavigationItems () {
    const navItems = [
      // {text: 'Логин', href: 'Auth', iconSource: require('../icons/delivery_icon.png')},
      {text: 'Новый заказ', href: 'MakeOrder', iconSource: require('../icons/plus33.png')},
      {text: 'Мои заказы', href: 'OrdersStack', iconSource: require('../icons/hanger.png')},
      {text: 'Мои карты', href: 'CardsStack', iconSource: require('../icons/card.png')},
      {text: 'Мои адреса', href: 'AddressesStack', iconSource: require('../icons/place.png')},
      // {text: 'Профиль', href: 'ProfileStack', iconSource: require('../icons/profile_icon.png')},
    ];

    return navItems.map(item =>
      this.renderNavigationItem(item)
    );
  }

  render () {
    let phone = this.props.auth.phone;
    if (this.props.auth.isAuthenticated) {
      /**
       * res[0] = matched string
       * res[1] = ddd
       * res[2] = ddd
       * res[3] = dd
       * res[4] = dd
       * @type {Array}
       */
      const res = phoneRegExp.exec(phone);
      phone = `+7 (${res[1]}) ${res[2]}-${res[3]}-${res[4]}`;
    }

    return (
      <View style={styles.container}>
        {
          this.props.nav.index === 1
          ? <StatusBar barStyle='light-content' />
          : null
        }
        <View
          // underlayColor='#477aaf'
          style={[
            styles.content,
            styles.header,
            {
              backgroundColor: color.airo,
            }
          ]}
        >
          <Image source={require('../icons/airo_logo_72x32.png')} style={{
          }}/>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('ProfileStack')}
            style={{
              flexDirection:  'row',
              alignItems:     'center',
              justifyContent: 'space-between',
              paddingRight:   20,
              width:          '100%'
            }}
          >
            {/* <Text style={styles.logoText}>airo</Text> */}
              <View
                // activeOpacity={0.85}
                style={{
                  marginTop: 16,
                  // marginLeft:  5,
                  // marginRight: 5,
                  // flexBasis:   '60%'
                }}
              >
                <Text style={{
                  fontSize: 20,
                  color:    '#fff',
                  fontFamily: 'FSElliotPro'
                }}
                numberOfLines={1}
                >{this.props.auth.name}</Text>
                <Text style={{
                  // marginTop: 2,
                  fontSize:  15,
                  color:     '#fff',
                  fontFamily: 'FSElliotPro',
                  marginTop: 4
                }}
                numberOfLines={1}>
                  {phone}
                </Text>
                <Text style={{
                  // marginTop: 2,
                  fontSize: 15,
                  color:    '#fff',
                  fontFamily: 'FSElliotPro',
                  marginTop: 4
                }}
                numberOfLines={1}
                >
                  {this.props.auth.email}
                </Text>
              </View>
              <Image source={require('../icons/shevron_white_right.png')} />
          </TouchableOpacity>
        </View>

        <View style={[
          styles.content
        ]}>
          {this.renderNavigationItems()}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // paddingTop: 20,
    // paddingLeft: 40
  },
  content: {
    paddingLeft: 25
  },
  header: {
    paddingTop:    20 + (Platform.OS === 'ios' ? 20 : 0),
    alignItems:    'flex-start',
    paddingBottom: 16,
    paddingLeft:   25,
    flexDirection: 'column'
  },
  logoText: {
    fontFamily: 'Poiret One',
    fontSize:   30
  },
  linkContainer: {
    marginTop:     25,
    flexDirection: 'row',
    alignItems:    'center'
  },
  linkText: {
    fontSize:   17,
    marginLeft: 15,
    color:      color.black
  },
  footer: {
    flex:           1,
    justifyContent: 'flex-end',
    paddingBottom:  50
  }
});

DrawerContent.propTypes = {
  navigation: PropTypes.object.isRequired,
  auth:       PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  nav:  state.nav
});

export default connect(mapStateToProps)(DrawerContent);
