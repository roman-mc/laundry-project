import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Text,
  StyleSheet
} from 'react-native';
import Button from 'apsl-react-native-button';
import {width} from '../styles';

class DateTimePickerButtons extends Component {
  render () {
    return (
      null
    );
  }
}

const styles = StyleSheet.create({
  button: {
    width:        width.bigContainer,
    alignSelf:    'center',
    alignItems:   'center',
    borderWidth:  1,
    borderColor:  '#0c0d0e',
    padding:      10,
    borderRadius: 5
  },
});

DateTimePickerButtons.propTypes = {
  // date:        PropTypes.string.isRequired,
  // time:        PropTypes.string.isRequired,
  onDatePress: PropTypes.func.isRequired,
  onTimePress: PropTypes.func.isRequired
};

export default DateTimePickerButtons;
