import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView
} from 'react-native';
import PropTypes from 'prop-types';

import Button from './Button';
import Input from './Input';
import Hr from './Hr';
import {width, button, buttonDefault} from '../styles';
import {toLocaleString2 as toLocaleString} from '../utils';

class DatePicker extends Component {
  constructor (props) {
    super(props);

    this.nowDate = new Date;

    this.renderDateButton = this.renderDateButton.bind(this);
  }

  renderDateButton (deliveryDate) {
    const nowDate = new Date;

    const dateDifference = Math.abs(nowDate.getDate() - deliveryDate.date.getDate());
    let text = null;

    if (dateDifference === 0) {
      text = <Text style={[styles.buttonSubText, deliveryDate.selected ? styles.buttonSelectedText : null]}>Сегодня</Text>;
    }
    else if (dateDifference === 1) {
      text = <Text style={[styles.buttonSubText, deliveryDate.selected ? styles.buttonSelectedText : null]}>Завтра</Text>;
    }
    const dateString = toLocaleString(deliveryDate.date);

    return (
      <Button
        key={dateString}
        style={[styles.button, deliveryDate.selected ? styles.buttonSelected : null]}
        textStyle={[styles.buttonText, styles.buttonSelectedText]}
        onPress={() => this.props.onPress(deliveryDate)}
      >
        <Text style={[styles.buttonText, deliveryDate.selected ? styles.buttonSelectedText: null]}>{dateString}</Text>

        {text}
      </Button>
    );

  }


  render () {
    return (
      <ScrollView contentContainerStyle={styles.container}>
        {
          this.props.data.map(this.renderDateButton)
        }
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    // flex:           1,
    justifyContent: 'center',
    width:          '100%',
    paddingTop:     40
    // marginTop:  '15%'
  },
  button: {
    ...buttonDefault.container,
    width:          '80%',
    marginTop:      5,
    flexDirection:  'column',
    alignItems:     'center',
    justifyContent: 'center',
    // padding:        10,
    height:         46
  },
  buttonSelected: {
    ...button.container,
    paddingBottom: 0
  },
  buttonSelectedText: {
    ...button.text,
  },
  buttonText: {
    ...buttonDefault.text,
    justifyContent:    'center',
    alignSelf:         'center',
    textAlignVertical: 'center',
    fontSize:          17,
    // marginTop:         5
  },
  buttonSubText: {

    fontSize:  15,
    marginTop: 1.6
  }
});

// TODO: propTypes
DatePicker.propTypes = {
  data:    PropTypes.array.isRequired,
  onPress: PropTypes.func.isRequired,
};

export default DatePicker;
