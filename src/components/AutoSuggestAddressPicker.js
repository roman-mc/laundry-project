import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {
  View,
  TouchableOpacity,
  FlatList,
  Text,
  Image,
  StyleSheet,
  Platform
} from 'react-native';
import KeyboardSpacer from 'react-native-keyboard-spacer';

import Hr from './Hr';
import Input from './Input';
import TextHint from './TextHint';

import {suggestAddresses} from '../api';

class AutoSuggestAddressPicker extends Component {
  constructor (props) {
    super(props);

    this.state = {
      addresses:  [],
      hintShowed: false
    };

    this.renderItem = this.renderItem.bind(this);
  }

  onSuccess (address) {
    // TODO
    // this.props.navigation.state.params.onAddressPick(address);
  }

  getSuggestedAddresses (text) {
    suggestAddresses(text)
      .then(addresses => {
        this.setState({addresses});
      });
  }

  onAddressPress (address) {
    // Address without country and city
    const validAddress = address.replace('Москва, ', '');
    const numberOfAddress = validAddress
      .slice(validAddress.lastIndexOf(',') + 1)
      .trim(); // Check if it is number


    if (numberOfAddress.search(/^\d|\d$/) !== -1 && numberOfAddress.search(/улица/) === -1) {
      this.onSuccess(validAddress);
    }
    else {
      this.setState({
        inputValue: validAddress,
        hintShowed: true
      });

      this.getSuggestedAddresses(address);
    }

  }

  renderItem ({item, index}) {
    return (
      <View
        style={styles.listItemContainer}
        key={item}
      >
        <TouchableOpacity
          onPress={() => this.onAddressPress(item)}
          style={styles.listItem}
        >
          <Text numberOfLines={1}>{item}</Text>
        </TouchableOpacity>
      </View>
    );
  }

  keyExtractor (item) {
    return item;
  }

  render () {
    return (
      <View style={styles.container}>
        <Input
          onChangeText={this.onChangeText}
          style={styles.input}
          placeholder='Улица и дом'
          autoFocus
          autoCorrect={false}
          value={this.state.inputValue}
        />
        <Image
          source={require('../icons/Search_Icon.png')}
          style={styles.inputIcon}
        />
        <FlatList
          style={{flex: 1}}
          data={this.props.data}
          renderItem={this.renderItem}
          // ItemSeparatorComponent={Hr}
          ref={(ref) => this.flat = ref}
          keyExtractor={this.keyExtractor}
          keyboardShouldPersistTaps='handled'
          ListHeaderComponent={
            this.props.hintShowed ?
              <View style={styles.hintContainer}>
                <Text style={styles.hintText}>
                  Выберите дом!
                </Text>
              </View>
            : null
          }
        />

      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    width:           '100%',
    backgroundColor: '#f0f0f0',
    flex:            1
  },
  input: {
    width:             '100%',
    textAlign:         'left',
    backgroundColor:   '#fff',
    borderBottomWidth: 0,
    marginBottom:      0,
    paddingLeft:       50
  },
  inputIcon: {
    position: 'absolute',
    top:      10,
    left:     15,
  },
  hintContainer: {
    backgroundColor: '#E7E7E7'
  },
  hintText: {
    marginVertical: 8,
    fontSize:       12,
    color:          '#000',
    textAlign:      'center'
  },
  listItem: {
    alignItems:     'center',
    justifyContent: 'center',
    padding:        10,
    paddingTop:     20,
    paddingBottom:  20,
  },
  listItemContainer: {
    borderBottomColor: '#d6d6d6',
    borderBottomWidth: 1
  }
});

AutoSuggestAddressPicker.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default AutoSuggestAddressPicker;
