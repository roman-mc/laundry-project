// TODO: refactor styles
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Text,
  ViewPropTypes
} from 'react-native';
import Input from './Input';
import {width, color} from '../styles';
import InputArea from './InputArea';

class AddressInputs extends Component {
  render () {
    return (
      <View style={[{alignItems: 'center', width: width.bigContainer}, this.props.style]}>
        <TouchableOpacity style={styles.addressContainer}
        onPress={this.props.onAddressPress}
        >
          <Text style={{
            color:    this.props.address ? '#000' : '#b6b6c1',
            fontSize: 16
          }}>{this.props.address || 'Улица и дом'}</Text>
        </TouchableOpacity>
        <View style={styles.inputsContainer}>
          <View style={styles.littleInputContainer}>
            <Input
              style={styles.littleInput}
              onChangeText={this.props.onChangeKvartira}
              value={this.props.kvartira}
              keyboardType='numbers-and-punctuation'
              ref={ref => this._kvartira = ref}
              onFocus={this.props.onFocus}
              autoCorrect={false}
              multiline={false}
            />
            <Text style={styles.underLittleInput}>
              Кв/офис
            </Text>
          </View>
          <View style={styles.littleInputContainer}>

            <Input
              style={styles.littleInput}
              onChangeText={this.props.onChangePodezd}
              value={this.props.podezd}
              keyboardType='numbers-and-punctuation'
              ref={ref => this._podezd = ref}
              onFocus={this.props.onFocus}
              autoCorrect={false}
              multiline={false}
            />
            <Text style={styles.underLittleInput}>
              Подъезд
            </Text>

          </View>

          <View style={styles.littleInputContainer}>

            <Input
              style={styles.littleInput}
              onChangeText={this.props.onChangeEtaj}
              value={this.props.etaj}
              keyboardType='numbers-and-punctuation'
              ref={ref => this._etaj = ref}
              onFocus={this.props.onFocus}
              autoCorrect={false}
              multiline={false}
            />
            <Text style={styles.underLittleInput}>
              Этаж
            </Text>

          </View>
          <View style={styles.littleInputContainer}>

            <Input
              style={styles.littleInput}
              onChangeText={this.props.onChangeDomofon}
              value={this.props.domofon}
              keyboardType='numbers-and-punctuation'
              ref={ref => this._domofon = ref}
              onFocus={this.props.onFocus}
              autoCorrect={false}
              multiline={false}
            />
            <Text style={styles.underLittleInput}>
              Домофон
            </Text>
          </View>
        </View>
        {/* <View style={styles.inputsContainer}>


        </View> */}
        <InputArea
          placeholder='Комментарии к адресу'
          value={this.props.comment}
          style={[styles.inputArea, {fontSize: 16}]}
          multiline
          onChangeText={this.props.onChangeComment}
          ref={ref => this._comment = ref}
          onFocus={this.props.onCommentFocus || this.props.onFocus}
          autoCorrect={false}
          autoGrow
          maxHeight={116}
          numberOfLines={6}
        />

      </View>
    );
  }
}

const styles = StyleSheet.create({
  addressContainer: {
    borderBottomWidth: 1,
    alignItems:        'center',
    justifyContent:    'center',
    width:             '100%',
    paddingBottom:     10,
    paddingTop:        15,
    borderColor:       color.inputBorders
  },
  inputArea: {
    fontSize:    14,
    // height:    100,
    flexWrap:    'wrap',
    marginTop:   25,
    width:       width.bigContainer,
    borderColor: color.inputBorders,
    minHeight:   72,
    height:      'auto'

  },
  inputsContainer: {
    flexDirection:  'row',
    width:          '100%',
    justifyContent: 'space-between',
    marginTop:      16
    // maxWidth:      width.bigContainer,
    // flexWrap:      'wrap'
  },
  littleInputContainer: {
    flexBasis:  '22%',
    alignItems: 'center'
  },
  littleInput: {
    marginLeft:        0,
    marginRight:       0,
    marginBottom:      0,
    fontSize:          12,
    paddingBottom:     4,
    width:             '100%',
    borderBottomWidth: 1
  },
  underLittleInput: {
    marginTop: 4,
    color:     '#b6b6c1'
  }
});

// TODO: refactor names
AddressInputs.propTypes = {
  address:          PropTypes.string.isRequired,
  onAddressPress:   PropTypes.func.isRequired,
  kvartira:         PropTypes.string.isRequired,
  onChangeKvartira: PropTypes.func.isRequired,
  podezd:           PropTypes.string.isRequired,
  onChangePodezd:   PropTypes.func.isRequired,
  etaj:             PropTypes.string.isRequired,
  onChangeEtaj:     PropTypes.func.isRequired,
  domofon:          PropTypes.string.isRequired,
  onChangeDomofon:  PropTypes.func.isRequired,
  comment:          PropTypes.string.isRequired,
  onChangeComment:  PropTypes.func.isRequired,
  style:            ViewPropTypes.style,
  onFocus:          PropTypes.func.isRequired,
  onCommentFocus:   PropTypes.func
};

AddressInputs.defaultProps = {
  onFocus: () => undefined
};

export default AddressInputs;
