import React, { Component } from 'react';
import {
  View,
  ActivityIndicator,
  StyleSheet,
  Text
} from 'react-native';

import {color} from '../styles';

export default class LoadingScreen extends Component {
  render () {
    return (
      <View style={styles.container}>
        <Text style={{
          fontSize:     17,
          color:        color.disabled,
          marginBottom: 25
        }}>Подключение к интернету...</Text>
        <ActivityIndicator animating />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex:           1,
    alignItems:     'center',
    justifyContent: 'center'
  }
});
