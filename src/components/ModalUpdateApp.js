import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Image
} from 'react-native';
import {
  connect
} from 'react-redux';
import PropTypes from 'prop-types';

import {
  hideModal
} from '../store/actions';
import {
  color
} from '../styles';

const ModalUpdateApp = props => (
  <View style={styles.container}>
    <View style={styles.modalBox}>
      <TouchableOpacity
        onPress={props.onClose}
        style={styles.closeButton}
        hitSlop={{
          top:    4,
          left:   4,
          right:  4,
          bottom: 4
        }}
      >
        <Image
          source={require('../icons/closeModalButton.png')}
        />
      </TouchableOpacity>
      <Text
        style={styles.title}
      >
        Вышла новая версия!
      </Text>
      <Text
        style={styles.contentText}
      >
        Пожалуйста обновите приложение для доступа ко всем функциям, это займет не более минуты
      </Text>
      <TouchableOpacity
        onPress={props.onAccept}
        style={styles.button}
        activeOpacity={0.8}
      >
        <Text
          style={styles.buttonText}
        >
          Обновить приложение
        </Text>
      </TouchableOpacity>
    </View>
  </View>
);

const styles = StyleSheet.create({
  container: {
    flex:            1,
    backgroundColor: 'rgba(0,0,0,0.5)',
    justifyContent:  'center',
    alignItems:      'center'
  },
  modalBox: {
    backgroundColor: 'white',
    marginLeft:      15,
    marginRight:     15,
    alignItems:      'center',
    width:           '85%',
    shadowColor:     'rgba(0,0,0,0.5)',
    shadowOffset:    {width: 0,height: 2},
    shadowRadius:    6,
    shadowOpacity:   0.5,
    elevation:       2
  },
  closeButton: {
    alignSelf:    'flex-end',
    paddingRight: 8,
    paddingTop:   8
  },
  title: {
    fontSize:    21,
    fontWeight:  '500',
    marginLeft:  26,
    marginRight: 26,
    color:       color.black
  },
  contentText: {
    marginTop:    12,
    marginRight:  26,
    marginLeft:   26,
    marginBottom: 20,
    fontSize:     16,
    textAlign:    'center',
    color:        color.black
  },
  button: {
    width:           '100%',
    backgroundColor: '#38A4F4',
    alignItems:      'center'
  },
  buttonText: {
    color:        'white',
    fontSize:     16,
    textAlign:    'center',
    fontWeight:   '600',
    marginTop:    15,
    marginBottom: 15
  }
});

ModalUpdateApp.propTypes = {
  onClose:  PropTypes.func.isRequired,
  onAccept: PropTypes.func.isRequired
};

export default ModalUpdateApp;
