import React, { Component } from 'react';
import {
  StyleSheet
} from 'react-native';
import {default as InitButton} from 'apsl-react-native-button';
import {color, width, button, buttonDisabled} from '../styles';

class Button extends Component {
  render () {
    // const buttonStyles = this.props.isDisabled ? styles.buttonDisabled : styles.button;
    // const textStyles = this.props.isDisabled ? styles.textDisabled : styles.text;

    return (
      <InitButton
        {...this.props}
        style={[
          styles.button,
          this.props.isDisabled ? styles.buttonDisabled : null,
          this.props.style
        ]}
        textStyle={[
          styles.text,
          this.props.isDisabled ? styles.textDisabled : null,
          this.props.textStyle
        ]}
      />
    );
  }
}

const styles = StyleSheet.create({
  button: {
    marginBottom: 10,
    borderRadius: 10,
    width:        width.bigContainer,
    alignSelf:    'center',
    ...button.container
  },
  buttonDisabled: buttonDisabled.container,
  text:           button.text,
  textDisabled:   buttonDisabled.text

});

Button.propTypes = {
  ...Button.propTypes
};

export default Button;
