import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  TextInput,
  StyleSheet,
  Platform
} from 'react-native';
import { color, width } from '../styles';

const defaultMultiline = Platform.OS !== 'ios';

class Input extends Component {
  measure (cb) {
    return this.input.measure(cb);
  }

  measureInWindow (cb) {
    return this.input.measureInWindow(cb);
  }

  render () {
    return (
      <TextInput
        {...this.props}
        style={[styles.input, this.props.style]}
        autoCorrect={this.props.autoCorrect !== undefined ? this.props.autoCorrect : false}  // TODO: remove it ?
        underlineColorAndroid='rgba(0,0,0,0)'
        // TODO: remove this crap
        ref={component => {
          this.input = component;
          this.props.inputRef && this.props.inputRef(component);
        }}
        placeholder={this.props.placeholder}
        multiline={this.props.multiline !== undefined ? this.props.multiline : defaultMultiline} // TODO: disable on ios ?
        numberOfLines={this.props.numberOfLines || 1}
        blurOnSubmit={this.props.blurOnSubmit !== undefined ? this.props.blurOnSubmit : true}
        placeholderTextColor='#b6b6c1'
      />
    );
  }
}

const styles = StyleSheet.create({
  input: {
    borderBottomWidth: 1,
    borderBottomColor: color.inputBorders,
    alignSelf:         'center',
    textAlign:         'center',
    width:             width.bigContainer,
    paddingTop:        10,
    paddingBottom:     10,
    marginLeft:        10,
    marginRight:       10,
    marginBottom:      10,
    fontSize:          16
  }
});

Input.propTypes = {
  // onChangeText: PropTypes.func.isRequired,
  // style:        PropTypes.oneOfType([PropTypes.number, PropTypes.object, PropTypes.array]),
  // value:        PropTypes.string,
  // placeholder:  PropTypes.string,
  // keyboardType: PropTypes.string,
  ...TextInput.propTypes
};

export default Input;
