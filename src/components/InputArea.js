// <textarea> for react native
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  TextInput,
  StyleSheet
} from 'react-native';
import { color, width } from '../styles';

class InputArea extends Component {
  measure (cb) {
    return this.input.measure(cb);
  }

  measureInWindow (cb) {
    return this.input.measureInWindow(cb);
  }

  render () {
    return (
      <TextInput
        {...this.props}
        style={[styles.input, this.props.style]}
        numberOfLines={3}
        underlineColorAndroid='rgba(0,0,0,0)'
        ref={ref => this.input = ref}
        // multiline
      />
    );
  }
}

const styles = StyleSheet.create({
  input: {
    borderWidth:       1,
    textAlign:         'left',
    textAlignVertical: 'top',
    padding:           10,
    paddingTop:        10,
    fontSize:          16,
    height:            64,
    alignItems:        'flex-start',
    justifyContent:    'flex-start',
    flexWrap:          'wrap',

  }
});

InputArea.propTypes = {
  ...TextInput.propTypes
};

export default InputArea;
