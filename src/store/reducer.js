// NOTE: tmp, then just use combineReducers
import {combineReducers} from 'redux';

import auth from './auth/reducer';
import nav from './router/reducer';
import order from './order/reducer';
import app from './app/reducer';
import addresses from './addresses/reducer';
import cards from './cards/reducer';
import modal from './modal/reducer';

export default combineReducers({
  auth,
  nav,
  order,
  app,
  addresses,
  cards,
  modal
});
