import React from 'react';
import {
  AsyncStorage,
  Linking
} from 'react-native';
import {createStore, applyMiddleware, compose} from 'redux';
import thunkMiddleware from 'redux-thunk';
import {persistStore, autoRehydrate} from 'redux-persist';
import logger from 'redux-logger';
import {NavigationActions} from 'react-navigation';

import reducer from './reducer';
import analytics from '../modules/analytics';
import crashlytics from '../modules/crashlytics';
import {appUrlInStore} from '../config';
import {
  showModal, hideModal,
  validateToken,
  routePicked,
  navigate,
  getAddresses,
  getCards
} from './actions';
import {
  getLatestAppVersion,
  getOrders
} from '../api';
import {isClosedOrderStatus} from '../utils';

import ModalUpdateApp from '../components/ModalUpdateApp';

const DeviceInfo = require('react-native-device-info');

const store = createStore(
  reducer,
  undefined,
  compose(
    applyMiddleware(thunkMiddleware, logger),
    autoRehydrate()
  )
);

/**
 * Checks latest app version and shows modal if versions mismatch
 * TODO: add script to update app versions at server
 */
const checkLatestAppVersion = () => {
  getLatestAppVersion()
    .then(latestVersion => {
      const usingVersion = DeviceInfo.getVersion();

      if (latestVersion !== usingVersion) {
        store.dispatch(showModal(
          <ModalUpdateApp
            onClose={() => store.dispatch(hideModal())}
            onAccept={() => {
              Linking.canOpenURL(appUrlInStore)
                .then(supported => {
                  if (supported) {
                    Linking.openURL(appUrlInStore);
                  }
                })
                .catch(err => console.error('An error occurred', err));

              store.dispatch(hideModal());
            }}
          />
        ));
      }
    })
    .catch(err => {
      console.dir(err);
    });
};

/**
 * After login hook
 * @param  {Object} auth
 */
const postLogin  = (auth) => {
  store.dispatch(getCards());
  store.dispatch(getAddresses());
  analytics.setUserId(auth.token);
  analytics.setUserProperty('phone_number', auth.phone);
  analytics.logEvent('user_logged', {when: 'repeated'});

  crashlytics.setUserName(auth.name);
  crashlytics.setUserIdentifier(auth.phone);
};


persistStore(
  store,
  {
    whitelist: ['auth'],
    storage:   AsyncStorage
  },
  (err, restoredStates, ...a) => {
    // TODO: error checking
    const {auth} = restoredStates;

    if (err !== null || auth === undefined) {
      store.dispatch(routePicked());
      store.dispatch(navigate('Auth'));

      return checkLatestAppVersion();
    }

    if (auth.token.length !== 0) {
      store.dispatch(validateToken(auth.token))
        .then((profile) => {
          getOrders(['limit=10'], profile.token)
            .then((ordersResponse) => {
              if (ordersResponse.totalCount === 0) {
                store.dispatch(navigate('Auth'));
              }
              else {
                const activeOrder = ordersResponse.orders.find(order =>
                  order.statusText !== '' && (isClosedOrderStatus(order.statusText) === false)
                );
                if (activeOrder === undefined) {
                  return store.dispatch(navigate('MakeOrder'));
                }

                const navigationToOrder = NavigationActions.navigate({
                  routeName: 'OrdersStack',

                  params: {},
                  action: NavigationActions.navigate(
                    {routeName: 'Order', params: {orderId: activeOrder.id}}
                  ),
                });
                store.dispatch(navigationToOrder);
              }
            });

          postLogin(profile);
        })
        .catch(err => {
          store.dispatch(navigate('Auth'));
        })
        .finally(() => {
          store.dispatch(routePicked());
          checkLatestAppVersion();
        });
    }
    else {
      store.dispatch(routePicked());
      store.dispatch(navigate('Auth'));
      checkLatestAppVersion();
    }
  }
);
// ).purge(); // TODO: COMMENT ME, UNCOMMENT LINE ABOVE

export default store;
