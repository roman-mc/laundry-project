import {
  GET_CARDS, GET_CARDS_SUCCESS, GET_CARDS_FAILURE,
  LOADING, LOADING_STOPPED,
  ACTIVATE_CARD, DEACTIVATE_CARD
} from './actions';

const initialState = {
  cards:     [],
  isLoading: false
};

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_CARDS:
      return {...state, isLoading: true};

    case GET_CARDS_SUCCESS:
      return {
        ...state,
        cards:     action.payload,
        isLoading: false
      };

    case GET_CARDS_FAILURE:
      return {
        ...state,
        isLoading: false
      };

    case ACTIVATE_CARD: {
      const cards = state.cards.map(card => {
        if (card !== action.payload) {
          return card;
        }

        return {
          ...card,
          active: !card.active
        };
      });

      return {
        ...state,
        cards,
        isLoading: true
      };
    }

    case DEACTIVATE_CARD: {
      const cards = state.cards.map(card => {
        if (card !== action.payload) {
          return card;
        }

        return {
          ...card,
          active: !card.active
        };
      });

      return {
        ...state,
        cards,
        isLoading: true
      };
    }

    case LOADING:
      return {
        ...state,
        isLoading: true
      };

    case LOADING_STOPPED:
      return {
        ...state,
        isLoading: false
      };

    default:
      return state;
  }
};
