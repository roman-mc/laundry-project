import {
  getCards as _getCards,
  deleteCard as _deleteCard,
  activateCard as _activateCard,
  deactivateCard as _deactivateCard
} from '../../api';

export const GET_CARDS = 'GET_CARDS';
export const GET_CARDS_SUCCESS = 'GET_CARDS_SUCCESS';
export const GET_CARDS_FAILURE= 'GET_CARDS_FAILURE';

export const ACTIVATE_CARD = 'ACTIVATE_CARD';
export const DEACTIVATE_CARD = 'DEACTIVATE_CARD';

// Common action that prohibit any manupulations on task,
// because there is some manlipulation already exist (e.g. deleting, activating, deactivating)
export const LOADING = 'CARDS_LOADING';
export const LOADING_STOPPED = 'CARDS_LOADING_STOPPED';

export const getCards = () => async (dispatch, getState) => {
  // TODO: Add cards fetching
  dispatch({
    type: GET_CARDS
  });

  const {auth} = getState();

  try {
    const resultCards = await _getCards(auth.token);

    const localCards = resultCards.map(card => {
      const pan = card.maskedPan;

      return {
        pan,
        bindingHash: card.bindingIdHash,
        active:      card.active,
        expiryDate:  card.expiryDate
      };
    });
    localCards.sort((a, b) =>
      a.pan > b.pan ? 1 :
      a.pan < b.pan ? -1 :
      0
    );
    dispatch({
      type:    GET_CARDS_SUCCESS,
      payload: localCards
    });

    return localCards;
  }
  catch (e) {
    dispatch({
      type:    GET_CARDS_FAILURE,
      payload: e
    });

    throw e;
  }

};

export const deleteCard = (bindingHash) => (dispatch, getState) => {
  dispatch({type: LOADING});

  const {auth} = getState();

  _deleteCard(bindingHash, auth.token)
    .then(() => {
      dispatch(getCards());
    })
    .catch(() => {
      dispatch({type: LOADING_STOPPED});
    });
};

/**
 * @param  {Object} card object, card of array cards
 * @return {Function}
 */
export const activateCard = (card) => (dispatch, getState) => {
  dispatch({
    type:    ACTIVATE_CARD,
    payload: card
  });

  const {auth} = getState();
  _activateCard(card.bindingHash, auth.token)
    .then(() => {
      dispatch(getCards());
    })
    .catch(() => {
      dispatch({type: LOADING_STOPPED});
    });
};

/**
 * @param  {Object} card object, card of array cards
 * @return {Function}
 */
export const deactivateCard = (card) => (dispatch, getState) => {
  dispatch({
    type:    DEACTIVATE_CARD,
    payload: card
  });

  const {auth} = getState();

  _deactivateCard(card.bindingHash, auth.token)
    .then(() => {
      dispatch(getCards());
    })
    .catch((e) => {
      console.log(e);
      dispatch({type: LOADING_STOPPED});
    });

};
