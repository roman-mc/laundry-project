import React from 'react';
import {
  Keyboard, BackHandler,
  Alert
} from 'react-native';
import {NavigationActions} from 'react-navigation';

import Navigator from '../../router';
import {
  REQUEST_SMS_SUCCESS,
  SEND_CODE_SUCCESS
} from '../auth/actions';


const initialRouteName = 'BlankStack';

const initialState = Navigator.router.getStateForAction(
  Navigator.router.getActionForPathAndParams(initialRouteName)
);

// Remove multiple initial routes, dunno why they are multiple
{
  const route = initialState.routes[0].routes.find(r => r.routeName === initialRouteName);

  while (route.routes.length !== 1) {
    route.routes.pop();
    route.index--;
  }
}


const reducer = (state = initialState, action) => {
  let nextState;

  if (action.type === NavigationActions.BACK) {
    const routes = state.routes[0].routes[state.routes[0].index];

    if (routes.index === 0) {
      Alert.alert(
        'Выход из приложения',
        'Действительно выйти?',
        [
          {
            text:    'Нет', onPress: () => undefined
          },
          {
            text:    'Да', onPress: () => {
              BackHandler.exitApp();
            }
          }
        ]
      );

      return state;
    }

    const routeName = routes.routes[routes.index].routeName;

    switch (routeName) {
      // Routes that have feature goBack
      case 'InsertCode':
      case 'PickTime':
      case 'Order':
      case 'AutoSuggestAddressPicker':
      case 'AddAddress':
      case 'AddCreditCard':
      case 'ProfileEditing':
        break;

      default: {
        BackHandler.exitApp();
        return state;
      }
    }

  }

  // TODO: replace this into components
  switch (action.type) {
    case REQUEST_SMS_SUCCESS: {
      const additionalAction = NavigationActions.navigate({
        routeName: 'InsertCode'
      });

      nextState = Navigator.router.getStateForAction(additionalAction);
      break;
    }

    default:
      nextState = Navigator.router.getStateForAction(action, state);
  }

  // Fucking android
  if (nextState !== null && nextState !== state) {
    Keyboard.dismiss();
  }

  return nextState || state;
};

export default reducer;
