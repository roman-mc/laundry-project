import { NavigationActions } from 'react-navigation';

export const resetNavigation = routeName => NavigationActions.reset({
  index:   0,
  actions: [
    NavigationActions.navigate({routeName})
  ]
});

export const navigate = (routeName, params) => NavigationActions.navigate({
  routeName,
  params
  // action: NavigationActions.navigate({routeName: 'SubRoute'})
});
