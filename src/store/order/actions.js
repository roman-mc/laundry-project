const uniqBy = require('lodash/uniqBy');
import { ableTimeslots } from '../../fakeDate';
import {
  convertServerTimeslots,
  convertToServerDate
} from './utils';
import {
  getPickedDay, getPickedTime,
  convertServerAddresses, convertStringAddresses
} from '../../utils';

import {
  suggestAddresses as requestSuggestAddresses,
  getLatLongForAddresses,
  selectTimeslots,
  sendOrder as requestOrderSend,
  checkPromocode as requestCheckPromocode,
  getAddresses
} from '../../api';


// TEMP: development
const orderProp = ['from', 'to'];

// TEMP: development
const orderOptions = ['urgentCollect', 'urgentDelivery'];


const INITIAL_TIME = '10:00 - 12:00';

export const SEND_ORDER = 'SEND_ORDER';
export const SEND_ORDER_SUCCESS = 'SEND_ORDER_SUCCESS';
export const SEND_ORDER_FAILURE = 'SEND_ORDER_FAILURE';

export const SUGGEST_ADDRESSES = 'SUGGEST_ADDRESSES';
export const SUGGEST_ADDRESSES_SUCCESS = 'SUGGEST_ADDRESSES_SUCCESS';
export const SUGGEST_ADDRESSES_FAILURE = 'SUGGEST_ADDRESSES_FAILURE';

export const CHANGE = 'CHANGE';
export const CHANGE_FROM = 'CHANGE_FROM';
export const CHANGE_TO = 'CHANGE_TO';

export const SELECT_TIMESLOTS = 'SELECT_TIMESLOTS';
export const SELECT_TIMESLOTS_SUCCESS = 'SELECT_TIMESLOTS_SUCCESS';
export const SELECT_TIMESLOTS_FAILURE = 'SELECT_TIMESLOTS_FAILURE';

export const GET_LAT_LONG = 'GET_LAT_LONG';
export const GET_LAT_LONG_SUCCESS = 'GET_LAT_LONG_SUCCESS';
export const GET_LAT_LONG_FAILURE = 'GET_LAT_LONG_FAILURE';

export const CHANGE_PROMOCODE = 'CHANGE_PROMOCODE';
export const CHANGE_PROMOCODE_SUCCESS = 'CHANGE_PROMOCODE_SUCCESS';
export const CHANGE_PROMOCODE_FAILURE = 'CHANGE_PROMOCODE_FAILURE';

export const CHANGE_ADDRESS_FROM = 'CHANGE_ADDRESS_FROM';
export const CHANGE_ADDRESS_TO = 'CHANGE_ADDRESS_TO';

export const RESET_ORDER = 'RESET_ORDER';

/**
 * @param  {String?} address
 */
export const suggestAddresses = (address: string = '') => (dispatch, getState) => {
  dispatch({
    type: SUGGEST_ADDRESSES
  });

  const {auth} = getState();

  return new Promise((resolve, reject) => {
    Promise.all([getAddresses(address, auth.token), requestSuggestAddresses(address)])
      .then(values => {
        const [userAddresses, apiData] = values;
        const suggestedAddresses = uniqBy(
          convertServerAddresses(userAddresses).concat(convertStringAddresses(apiData)),
          'address'
        );

        dispatch({
          type:    SUGGEST_ADDRESSES_SUCCESS,
          payload: suggestedAddresses
        });

        resolve(suggestedAddresses);
      })
      .catch(e => {
        reject(e);
        dispatch({
          type:    SUGGEST_ADDRESSES_FAILURE,
          payload: e
        });
      });
  });
};


export const getAbleTimeslots = cb => (dispatch, getState) => {
  dispatch({
    type: SELECT_TIMESLOTS
  });

  const {order} = getState();

  const addresses = [];
  if (order.dataFrom.isAddressCorrect) {
    addresses.push(order.dataFrom.address);

    if (!order.sameDeliveryAddress) {
      addresses.push(order.dataTo.address);
    }
  }
  else {
    addresses.push('');
  }

  getLatLongForAddresses(addresses)
    .then(latLongs => {
      const latFrom = latLongs[0];
      const latTo = order.sameDeliveryAddress ? latLongs[0] : latLongs[1];

      dispatch({
        type:    GET_LAT_LONG_SUCCESS, // TODO: reducer handler
        payload: [latFrom, latTo]
      });

      const collectDate = new Date;
      const deliveryDate = new Date;
      deliveryDate.setDate(deliveryDate.getDate() + 2);

      const requestData = {
        collect: {
          time:    INITIAL_TIME,
          date:    convertToServerDate(collectDate),
          latLong: latFrom
        },
        delivery: {
          time:        INITIAL_TIME,
          date:        convertToServerDate(deliveryDate),
          latLong:     latTo,
          sameAddress: order.sameDeliveryAddress
        },
        options: {
          urgent_collect:      order.ableTimeslots.urgentCollect.checked,
          "next-day-delivery": order.ableTimeslots.urgentDelivery.checked
        }
      };

      selectTimeslots(requestData)
        .then(response => {
          const timeslots = convertServerTimeslots(response.data);
          console.log(response);
          console.log(timeslots);

          dispatch({
            type:    SELECT_TIMESLOTS_SUCCESS,
            payload: timeslots
          });

          cb(null);
        })
        .catch(e => {
          dispatch({
            type:    SELECT_TIMESLOTS_FAILURE,
            payload: e
          });
          cb(e);
        });


    })
    .catch(err => {
      dispatch({
        type:    GET_LAT_LONG_FAILURE,
        payload: err
      });
      cb(err);
    });
};

/**
 * @param  {String} property
 * @param  {Object} day      see in reducer date day
 */
export const selectDate = (property, day) => (dispatch, getState) => {
  if (orderProp.indexOf(property) === -1) throw new Error(`Property must be from or to, but got: ${property}`); // TEMP

  const {order} = getState();


  const collectDay = getPickedDay(order.ableTimeslots.collect);
  const deliveryDay = getPickedDay(order.ableTimeslots.delivery);
  const collectTime = getPickedTime(collectDay);
  const deliveryTime = getPickedTime(deliveryDay);

  dispatch({
    type: SELECT_TIMESLOTS
  });

  const requestData = {
    collect: {
      time:    collectTime,
      date:    convertToServerDate(property === 'from' ? day.date : collectDay.date),
      latLong: order.dataFrom.latLong
    },
    delivery: {
      time:        deliveryTime,
      date:        convertToServerDate(property === 'to' ? day.date : deliveryDay.date),
      latLong:     order.dataTo.latLong,
      sameAddress: order.sameDeliveryAddress
    },
    options: {
      urgent_collect:      order.ableTimeslots.urgentCollect.checked,
      "next-day-delivery": order.ableTimeslots.urgentDelivery.checked
    }
  };

  selectTimeslots(requestData)
    .then(response => {
      const timeslots = convertServerTimeslots(response.data);
      // console.log(JSON.stringify(timeslots));

      dispatch({
        type:    SELECT_TIMESLOTS_SUCCESS,
        payload: timeslots
      });
    })
    .catch(e => {
      dispatch({
        type:    SELECT_TIMESLOTS_FAILURE,
        payload: e
      });
    });
};

/**
 * @param  {String} property
 * @param  {String} time      see in reducer date day
 */
export const selectTime = (property, time) => (dispatch, getState) => {
  if (orderProp.indexOf(property) === -1) throw new Error(`Property must be from or to, but got: ${property}`); // TEMP

  const {order} = getState();

  dispatch({
    type: SELECT_TIMESLOTS
  });

  const collectDay = getPickedDay(order.ableTimeslots.collect);
  const deliveryDay = getPickedDay(order.ableTimeslots.delivery);
  const collectTime = getPickedTime(collectDay);
  const deliveryTime = getPickedTime(deliveryDay);
  // TEMP
  const collectDate = collectDay.date instanceof Date ? collectDay.date : new Date(collectDay.date);
  const deliveryDate = deliveryDay.date instanceof Date ? deliveryDay.date : new Date(deliveryDay.date);
  // TEMP

  const requestData = {
    collect: {
      time:    property === 'from' ? time : collectTime,
      date:    convertToServerDate(collectDate),
      latLong: order.dataFrom.latLong
    },
    delivery: {
      time:        property === 'to' ? time : deliveryTime,
      date:        convertToServerDate(deliveryDate),
      latLong:     order.dataTo.latLong,
      sameAddress: order.sameDeliveryAddress
    },
    options: {
      urgent_collect:      order.ableTimeslots.urgentCollect.checked,
      "next-day-delivery": order.ableTimeslots.urgentDelivery.checked
    }
  };

  selectTimeslots(requestData)
    .then(response => {
      const timeslots = convertServerTimeslots(response.data);

      dispatch({
        type:    SELECT_TIMESLOTS_SUCCESS,
        payload: timeslots
      });
    })
    .catch(e => {
      dispatch({
        type:    SELECT_TIMESLOTS_FAILURE,
        payload: e
      });
    });
};

export const selectOption = (option, value) => (dispatch, getState) => {
  if (orderOptions.indexOf(option) === -1) throw new Error(`Property must be from or to, but got: ${option}`); // TEMP

  const {order} = getState();

  dispatch({
    type: SELECT_TIMESLOTS
  });

  const collectDay = getPickedDay(order.ableTimeslots.collect);
  const deliveryDay = getPickedDay(order.ableTimeslots.delivery);
  const collectTime = getPickedTime(collectDay);
  const deliveryTime = getPickedTime(deliveryDay);
  // TEMP
  const collectDate = collectDay.date instanceof Date ? collectDay.date : new Date(collectDay.date);
  const deliveryDate = deliveryDay.date instanceof Date ? deliveryDay.date : new Date(deliveryDay.date);
  // TEMP

  const requestData = {
    collect: {
      time:    collectTime,
      date:    convertToServerDate(collectDate),
      latLong: order.dataFrom.latLong
    },
    delivery: {
      time:        deliveryTime,
      date:        convertToServerDate(deliveryDate),
      latLong:     order.dataTo.latLong,
      sameAddress: order.sameDeliveryAddress
    },
    options: {
      urgent_collect:      option === orderOptions[0] ? value : order.ableTimeslots.urgentCollect.checked,
      "next-day-delivery": option === orderOptions[1] ? value : order.ableTimeslots.urgentDelivery.checked
    }
  };

  selectTimeslots(requestData)
    .then(response => {
      const timeslots = convertServerTimeslots(response.data);
      dispatch({
        type:    SELECT_TIMESLOTS_SUCCESS,
        payload: timeslots
      });
    })
    .catch(e => {
      dispatch({
        type:    SELECT_TIMESLOTS_FAILURE,
        payload: e
      });
    });
};

export const sendOrder = () => (dispatch, getState) => {
  dispatch({
    type: SEND_ORDER
  });

  const {order, auth: {token}} = getState();
  // Just cache references
  const {dataFrom, dataTo} = order;

  const collectDay = getPickedDay(order.ableTimeslots.collect);
  const deliveryDay = getPickedDay(order.ableTimeslots.delivery);
  const collectTime = getPickedTime(collectDay);
  const deliveryTime = getPickedTime(deliveryDay);


  const requestData = {
    collect: {
      time:           collectTime,
      date:           convertToServerDate(collectDay.date),
      address:        dataFrom.address,
      addressDetails: {
        'кв/офис': dataFrom.kvartira,
        'этаж':    dataFrom.etaj,
        'подъезд': dataFrom.podezd,
        'домофон': dataFrom.domofon
      },
      addressComment: dataFrom.addressComment
    },
    delivery: {
      time:           deliveryTime,
      date:           convertToServerDate(deliveryDay.date),
      address:        dataTo.address,
      addressDetails: {
        'кв/офис': dataTo.kvartira,
        'этаж':    dataTo.etaj,
        'подъезд': dataTo.podezd,
        'домофон': dataTo.domofon
      },
      addressComment: dataTo.addressComment
    },
    sameAddress:         order.sameDeliveryAddress,
    promocode:           order.promocode.value,
    comment:             order.comment,
    // urgent-collect only in sendAddress , fucking monkeys
    "urgent_collect":    order.ableTimeslots.urgentCollect.checked,
    "next-day-delivery": order.ableTimeslots.urgentDelivery.checked
  };

  requestOrderSend(requestData, token)
    .then(orderID => {

      dispatch({
        type:    SEND_ORDER_SUCCESS,
        payload: orderID
      });
    })
    .catch(err => {
      // TODO: error handling example
      //       if (error.response) {
      //   // The request was made and the server responded with a status code
      //   // that falls out of the range of 2xx
      //   console.log(error.response.data);
      //   console.log(error.response.status);
      //   console.log(error.response.headers);
      // } else if (error.request) {
      //   // The request was made but no response was received
      //   // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
      //   // http.ClientRequest in node.js
      //   console.log(error.request);
      // } else {
      //   // Something happened in setting up the request that triggered an Error
      //   console.log('Error', error.message);
      // }

      console.log(err);
      // console.log(err.response.status);
      // console.log(err.response.status);
      // console.log(err.response.status);
      dispatch({
        type:    SEND_ORDER_FAILURE,
        payload: err
      });
    });


};

export const changePromocode = promocode => (dispatch, getState) => {
  dispatch({
    type:    CHANGE_PROMOCODE,
    payload: promocode
  });

  if (promocode.length === 0) return;

  const {auth} = getState();

  requestCheckPromocode(promocode, auth.token)
    .then(data => {
      dispatch({
        type:    CHANGE_PROMOCODE_SUCCESS,
        payload: data
      });
    })
    .catch(err => {
      dispatch({
        type:    CHANGE_PROMOCODE_FAILURE,
        payload: err
      });
    });
};


/*                                                                                 */
export const resetOrder = () => ({
  type: RESET_ORDER
});

export const change = (prop, value) => ({
  type:    CHANGE,
  payload: {
    prop,
    value
  }
});

export const changeFrom = (prop, value) => ({
  type:    CHANGE_FROM,
  payload: {
    prop,
    value
  }
});

export const changeTo = (prop, value) => ({
  type:    CHANGE_TO,
  payload: {
    prop,
    value
  }
});

/**
 * @param  {string} prop           enum prop of orderProp
 * @param  {Object} value {address, details:{..}}
 * @param  {boolean} isAddressCorrect
 * @return {void}
 */
export const changeAddress = (prop, value, isAddressCorrect) => {
  if (orderProp.indexOf(prop) === -1) {
    throw new Error(`This is neither 'from' or 'to': ${prop}`);
  }

  return ({
    type:    prop === 'from' ? CHANGE_ADDRESS_FROM : CHANGE_ADDRESS_TO,
    payload: {
      address: value,
      isAddressCorrect
    }
  });
};
/*                                                                                 */
