/**
 * @param  {String} string
 * @return {Date}
 */
const convertFromServerDate = (string) => {
  const [day, month, year] = string.split('-');

  return new Date(year, month - 1, day);
};

/**
 * @param  {Date} date
 * @return {String}
 */
export const convertToServerDate = (date) => {
  if (date instanceof Date === false) {
    date = new Date(date);
  }
  const day = date.getDate();
  const month = date.getMonth() + 1;
  const year = date.getFullYear();

  return `${day}-${month}-${year}`;
};

const extractDates = (dates) => {
  const newDates = dates.map((date) => {
    if (date['disabled-all'] === true) {
      return null;
    }

    const dateObj = convertFromServerDate(date.date);


    const newDate = {
      date:     dateObj,
      times:    null,
      selected: date.selected
    };

    const times = date.times.map(time => {
      if (time.disabled) {
        return null;
      }

      return time;
    }).filter(o => o !== null);

    newDate.times = times;

    return newDate;
  }).filter(o => o !== null);

  return newDates;
};

// Conversation server response data to my local not shitty strong structures
export const convertServerTimeslots = (data) => {
  const timeslots = {};
  const collectDates = extractDates(data.collect);
  const deliveryDates = extractDates(data.delivery);

  timeslots.collect = collectDates;
  timeslots.delivery = deliveryDates;

  timeslots.urgentCollect = data.options.urgent_collect;
  timeslots.urgentDelivery = data.options['next-day-delivery'];

  return timeslots;
};

/**
 * Parses suggested addresses returned by yandex api
 * @param  {String} string
 * @return {Array<String>}
 */
export const parseSuggestedAddresses = string => {
  const addresses = [];

  if (typeof string !== 'string') {
    console.error('This is not string!', string);
    return addresses;
  }

  // TODO: make sure that this response will have always such structure
  try {
    const pureJsonableString = string.slice(string.indexOf('(') + 1, string.lastIndexOf(')'));
    const arrays = JSON.parse(pureJsonableString)[1];

    for (const o of arrays) {
      addresses.push(o[2].replace('Россия, ', ''));
    }
  }
  catch (e) {
    console.error(e);
  }

  return addresses;
};
