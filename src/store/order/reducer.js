import {
  CHANGE, CHANGE_FROM, CHANGE_TO,
  SELECT_TIMESLOTS, SELECT_TIMESLOTS_SUCCESS, SELECT_TIMESLOTS_FAILURE,
  SUGGEST_ADDRESSES, SUGGEST_ADDRESSES_SUCCESS, SUGGEST_ADDRESSES_FAILURE,
  GET_LAT_LONG_FAILURE, GET_LAT_LONG_SUCCESS,
  SEND_ORDER, SEND_ORDER_SUCCESS, SEND_ORDER_FAILURE,
  RESET_ORDER,
  CHANGE_PROMOCODE, CHANGE_PROMOCODE_SUCCESS, CHANGE_PROMOCODE_FAILURE,
  CHANGE_ADDRESS_FROM, CHANGE_ADDRESS_TO
} from './actions';
import {
  RESET_AUTH
} from '../auth/actions';

const initialState = {
  sameDeliveryAddress: true,
  isLoading:           false,
  // Order id is received, when order is successfully sent, gj
  orderID:             null,
  // values for address from
  dataFrom:            {
    date:             null,
    time:             '',
    address:          '',
    isAddressCorrect: false,
    kvartira:         '',
    podezd:           '',
    etaj:             '',
    domofon:          '',
    addressComment:   '',
    latLong:          ''
  },
  // values for address to
  dataTo: {
    date:             null,
    time:             '',
    address:          '',
    isAddressCorrect: false,
    kvartira:         '',
    podezd:           '',
    etaj:             '',
    domofon:          '',
    addressComment:   '',
    latLong:          ''
  },
  suggestedAddresses: [],
  promocode:          {
    value:     '',
    isValid:   false,
    isLoading: false,
    discount:  '0'
  },
  comment:       '',
  ableTimeslots: {
    collect:       [],
    delivery:      [],
    urgentCollect: {
      available: true,
      checked:   false,
      message:   ''
    },
    urgentDelivery: {
      available: true,
      checked:   false,
      message:   ''
    },
  },
  // When user gets timeslots first, shouldGetTimeslots is set to false
  // and when user navigate back/forth, no need to get timeslots, except he changes address
  shouldGetTimeslots: true,
  errors:             []
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case CHANGE:
      return {...state, [action.payload.prop]: action.payload.value};

    case CHANGE_TO: {

      return {
        ...state,
        dataTo: {
          ...state.dataTo,
          [action.payload.prop]: action.payload.value
        }
      };
    }

    case CHANGE_FROM: {

      return {
        ...state,
        dataFrom: {
          ...state.dataFrom,
          [action.payload.prop]: action.payload.value
        }
      };
    }

    case SELECT_TIMESLOTS:
      return {
        ...state,
        isLoading: true
      };

    case SELECT_TIMESLOTS_SUCCESS:
      return {
        ...state,
        shouldGetTimeslots: false,
        ableTimeslots:      action.payload,
        isLoading:          false
      };

    case SELECT_TIMESLOTS_FAILURE:
      return {
        ...state,
        isLoading: false
        // TODO: error handling
      };

    case SUGGEST_ADDRESSES:
      return {
        ...state,
        isLoading: true,
        // suggestedAddresses: []
      };

    case SUGGEST_ADDRESSES_SUCCESS:
      return {
        ...state,
        isLoading:          false,
        suggestedAddresses: action.payload
      };

    case SUGGEST_ADDRESSES_FAILURE:
      return {
        ...state,
        isLoading: false
        // TODO: error handling
      };

    case GET_LAT_LONG_SUCCESS:
      return {
        ...state,
        dataFrom: {
          ...state.dataFrom,
          latLong: action.payload[0]
        },
        dataTo: {
          ...state.dataTo,
          latLong: action.payload[1]
        }
      };

    case GET_LAT_LONG_FAILURE:
      return {
        ...state,
        isLoading: false,
        errors:    state.errors.concat(action.payload)
      };

    case SEND_ORDER:
      return {
        ...state,
        isLoading: true
      };

    case SEND_ORDER_SUCCESS:
      return {
        ...state,
        isLoading: false,
        orderID:   action.payload
        // TODO
      };

    case SEND_ORDER_FAILURE:
      return {
        ...state,
        isLoading: false
        // TODO error handling
      };

    case RESET_ORDER:
    case RESET_AUTH:
      return {
        ...initialState,
        // if ableTimeslots = null, then there are components that throw error
        ableTimeslots: {
          ...state.ableTimeslots,
          urgentCollect:  initialState.ableTimeslots.urgentCollect,
          urgentDelivery: initialState.ableTimeslots.urgentDelivery
        },
        shouldGetTimeslots: true
      };

    case CHANGE_PROMOCODE:
      return {
        ...state,
        isLoading: action.payload.length > 0,
        promocode: {
          ...state.promocode,
          value:     action.payload,
          isValid:   false,
          isLoading: action.payload.length > 0,
          discount:  '0'
        }
      };

    case CHANGE_PROMOCODE_SUCCESS: {
      // This is for guaranting that that promocode is valid which was requested
      // Because of race condition of requests
      const isLast = state.promocode.value.toLowerCase() === action.payload.code.toLowerCase();

      return {
        ...state,
        isLoading: false,
        promocode: {
          ...state.promocode,
          isValid:   isLast,
          isLoading: false,
          discount:  isLast ? action.payload.discount : '0'
        }
      };
    }

    case CHANGE_PROMOCODE_FAILURE:
      return {
        ...state,
        isLoading: false,
        promocode: {
          ...state.promocode,
          isLoading: false
        }
        // TODO: error handling
      };

    case CHANGE_ADDRESS_FROM: {
      const address = action.payload.address;
      return {
        ...state,
        shouldGetTimeslots: true,
        dataFrom:           {
          ...state.dataFrom,
          address:          address.address,
          kvartira:         address.details.kvartira,
          podezd:           address.details.podezd,
          etaj:             address.details.etaj,
          domofon:          address.details.domofon,
          addressComment:   address.comment,
          isAddressCorrect: action.payload.isAddressCorrect
        }
      };
    }

    case CHANGE_ADDRESS_TO: {
      const address = action.payload.address;

      return {
        ...state,
        shouldGetTimeslots: true,
        dataTo:             {
          ...state.dataTo,
          address:          address.address,
          kvartira:         address.details.kvartira,
          podezd:           address.details.podezd,
          etaj:             address.details.etaj,
          domofon:          address.details.domofon,
          addressComment:   address.comment,
          isAddressCorrect: action.payload.isAddressCorrect
        }
      };
    }

    default:
      return state;
  }
};

export default reducer;
