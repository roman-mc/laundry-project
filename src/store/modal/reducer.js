import {
  SHOW_MODAL, HIDE_MODAL, TOGGLE_MODAL
} from './actions';

const initialState = {
  visible:   false,
  component: null // react component
};

const modalReducer = (state = initialState, action) => {
  switch (action.type) {
    case SHOW_MODAL:
      return {visible: true, component: action.payload};

    case HIDE_MODAL:
      return {...state, visible: false};

    case TOGGLE_MODAL:
      return {...state, visible: !state.visible};

    default:
      return state;
  }
};

export default modalReducer;
