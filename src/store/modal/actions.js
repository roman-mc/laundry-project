export const SHOW_MODAL = 'SHOW_MODAL';
export const HIDE_MODAL = 'HIDE_MODAL';
export const TOGGLE_MODAL = 'TOGGLE_MODAL';

/**
 * @param  {Object} component an instanciated react component: <Component />
 * @return {Object} redux action
 */
export const showModal = (component) => {
  if (component === undefined) {
    throw new Error('Function show modal must have component');
  }

  return {
    type:    SHOW_MODAL,
    payload: component
  };
};

export const hideModal = () => ({
  type: HIDE_MODAL
});

export const toggleModal = () => ({
  type: TOGGLE_MODAL
});
