// @flow
import {getAddresses as apiGetUserAddresses} from '../../api';
import {Address} from '../../utils';

export const GET_USER_ADDRESSES = 'GET_USER_ADDRESSES';
export const GET_USER_ADDRESSES_SUCCESS = 'GET_USER_ADDRESSES_SUCCESS';
export const GET_USER_ADDRESSES_FAILURE = 'GET_USER_ADDRESSES_FAILURE';

export const ADD_ADDRESS = 'ADD_ADDRESS';
export const ADD_ADDRESS_SUCCESS = 'ADD_ADDRESS_SUCCESS';
export const ADD_ADDRESS_FAILURE = 'ADD_ADDRESS_FAILURE';

/**
 * @return {Function => Function => Promise} returns promise
 */
export const getAddresses = () => (dispatch: Function, getState: Function) => {
  dispatch({
    type: GET_USER_ADDRESSES
  });

  const {auth} = getState();

  return new Promise((resolve, reject) => {
    apiGetUserAddresses('', auth.token)
      .then(addresses => {
        dispatch({
          type:    GET_USER_ADDRESSES_SUCCESS,
          payload: addresses
        });
        resolve(addresses);
      })
      .catch(e => {
        dispatch({
          type:  GET_USER_ADDRESSES_FAILURE,
          error: e
        });
        reject(e);
      });
  });
};

// export const addAddress = (address: Address) => async (dispatch: Function, getState: Function) => {
//   dispatch({
//     type: ADD_ADDRESS
//   });
//
//   const {auth} = getState();
//
//   try {
//     const addressId = addAddress(address, auth.token);
//     dispatch({type: ADD_ADDRESS_SUCCESS});
//   } catch (e) {
//     dispatch({type: ADD_ADDRESS_FAILURE});
//     throw e;
//   }
//
// };
