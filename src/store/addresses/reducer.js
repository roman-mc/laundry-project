// @flow

import {
  GET_USER_ADDRESSES,
  GET_USER_ADDRESSES_SUCCESS,
  GET_USER_ADDRESSES_FAILURE,
} from './actions';

export type State = {
  addresses: Array<string>,
  isLoading: boolean
}

const initialState: State = {
  addresses: [],
  isLoading: false,
  error:     null
};

export default (state: State = initialState, action: Object) => {
  switch (action.type) {
    case GET_USER_ADDRESSES:
      return {...state, isLoading: true, error: null};

    case GET_USER_ADDRESSES_SUCCESS:
      return {...state, addresses: action.payload, isLoading: false};

    case GET_USER_ADDRESSES_FAILURE:
      return {...state, isLoading: false, error: action.error};

    default:
      return state;
  }
};
