import { REHYDRATE } from 'redux-persist/constants';
import { ROUTE_PICKED } from './actions';

const initialState = {
  storeRehydrated: false,
  routePicked:     false
  // connection: false // IDEA
};

export default (state = initialState, action) => {
  switch (action.type) {
    case REHYDRATE:
      return {
        ...state,
        storeRehydrated: true
      };

    case ROUTE_PICKED:
      return {
        ...state,
        routePicked: true
      };

    default:
      return state;
  }
};
