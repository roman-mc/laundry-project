// ROUTE_PICKED is used to highlight whether route was picked or should, used only on initial app load
export const ROUTE_PICKED = 'ROUTE_PICKED';

export const routePicked = () => ({
  type: ROUTE_PICKED
});
