import {
  requestSMS as makeRequestSMS,
  sendSmsCode as makeSendSmsCode,
  validateToken as makeValidateToken,
  sendProfileInfo as makeSendProfileInfo
} from '../../api';

export const REQUEST_SMS = 'REQUEST_SMS';
export const REQUEST_SMS_SUCCESS = 'REQUEST_SMS_SUCCESS';
export const REQUEST_SMS_FAILURE = 'REQUEST_SMS_FAILURE';

export const SEND_CODE = 'SEND_CODE';
export const SEND_CODE_SUCCESS = 'SEND_CODE_SUCCESS';
export const SEND_CODE_FAILURE = 'SEND_CODE_FAILURE';

export const VALIDATE_TOKEN = 'VALIDATE_TOKEN';
export const VALIDATE_TOKEN_SUCCESS = 'VALIDATE_TOKEN_SUCCESS';
export const VALIDATE_TOKEN_FAILURE = 'VALIDATE_TOKEN_FAILURE';

export const SEND_PROFILE_INFO = 'SEND_PROFILE_INFO';
export const SEND_PROFILE_INFO_SUCCESS = 'SEND_PROFILE_INFO_SUCCESS';
export const SEND_PROFILE_INFO_FAILURE = 'SEND_PROFILE_INFO_FAILURE';

export const RESET_AUTH = 'RESET_AUTH';

/**
 * Cleans phone number to send it to server
 * @param  {String} phoneNumber
 * @return {String}
 */
const cleanPhoneNumber = (phoneNumber) => phoneNumber.replace(/[^\d+]/g, '');

export const requestSMS = (phoneNumber) => (dispatch, getState) => {
  const phone = cleanPhoneNumber(phoneNumber);

  dispatch({
    type:    REQUEST_SMS,
    payload: phoneNumber
  });

  makeRequestSMS(phone)
    .then(response => {
      dispatch({
        type:    REQUEST_SMS_SUCCESS,
        payload: response.data
      });
    })
    .catch(err => {
      dispatch({
        type:    REQUEST_SMS_FAILURE,
        payload: err
      });
    });
};

export const sendSmsCode = (code) => (dispatch, getState) => {
  const {auth} = getState();
  const phone = cleanPhoneNumber(auth.phone);

  dispatch({
    type: SEND_CODE
  });

  return new Promise((resolve, reject) => {
    makeSendSmsCode(phone, code)
      .then(profile => {
        dispatch({
          type:    SEND_CODE_SUCCESS,
          payload: profile
        });

        resolve(profile);
      })
      .catch(err => {
        dispatch({
          type:    SEND_CODE_FAILURE,
          payload: err
        });
        reject(err);
      });
  });
};

export const resetAuth = () => ({
  type: RESET_AUTH
});

export const validateToken = () => (dispatch, getState) => {
  dispatch({
    type: VALIDATE_TOKEN
  });

  const {auth} = getState();

  return new Promise((resolve, reject) => {
    makeValidateToken(auth.token)
      .then(profile => {
        dispatch({
          type:    VALIDATE_TOKEN_SUCCESS,
          payload: profile
        });
        resolve(profile);
      })
      .catch(err => {
        // TODO error handlir
        dispatch({
          type:    VALIDATE_TOKEN_FAILURE,
          payload: err
        });
        reject(err);
      });
  });
};

export const sendProfileInfo = data => async (dispatch, getState) => {
  dispatch({
    type: SEND_PROFILE_INFO
  });

  const {auth} = getState();

  try {
    await makeSendProfileInfo(data, auth.token);

    dispatch({
      type:    SEND_PROFILE_INFO_SUCCESS,
      payload: data
    });
  }
  catch (e) {
    dispatch({
      type:    SEND_PROFILE_INFO_FAILURE,
      payload: e
    });

    throw e;
  }

  return;
};
