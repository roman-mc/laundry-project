import { REHYDRATE } from 'redux-persist/constants';

import {
  REQUEST_SMS, REQUEST_SMS_SUCCESS, REQUEST_SMS_FAILURE,
  SEND_CODE, SEND_CODE_SUCCESS, SEND_CODE_FAILURE,
  RESET_AUTH,
  VALIDATE_TOKEN, VALIDATE_TOKEN_SUCCESS, VALIDATE_TOKEN_FAILURE,
  SEND_PROFILE_INFO, SEND_PROFILE_INFO_SUCCESS, SEND_PROFILE_INFO_FAILURE
} from './actions';

const initialState= {
  token:           '',
  name:            '',
  email:           '',
  phone:           '',
  isAuthenticated: false,
  isLoading:       false,
  errors:          []
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case REQUEST_SMS: {

      return {...state, isLoading: true, phone: action.payload, errors: []};
    }

    case REQUEST_SMS_SUCCESS:
      return {...state, isLoading: false};

    case REQUEST_SMS_FAILURE:
      return {
        ...state,
        isLoading: false,
        errors:    state.errors.concat(action.payload)
      };



    case SEND_CODE:
      return {...state, isLoading: true, errors: []};

    case SEND_CODE_SUCCESS:
      return {
        ...state,
        token:           action.payload.token,
        phone:           action.payload.phone,
        // TODO: after send order redirect on profile
        email:           action.payload.email,
        name:            action.payload.name,
        isLoading:       false,
        isAuthenticated: true
      };


    case SEND_CODE_FAILURE:
      return {
        ...state,
        isLoading: false,
        errors:    state.errors.concat(action.payload)
      };

    case VALIDATE_TOKEN:
      return {
        ...state,
        isLoading: true,
        errors:    []
      };

    case VALIDATE_TOKEN_SUCCESS:
      return {
        ...state,
        isAuthenticated: true,
        token:           action.payload.token,
        phone:           action.payload.phone,
        email:           action.payload.email,
        name:            action.payload.name,
        isLoading:       false
      };

    case VALIDATE_TOKEN_FAILURE:
      return {
        ...initialState
      };

    case SEND_PROFILE_INFO:
      return {
        ...state,
        isLoading: true,
        errors:    []
      };

    case SEND_PROFILE_INFO_SUCCESS:
      return {
        ...state,
        email:     action.payload.email,
        name:      action.payload.name,
        isLoading: false
      };

    case SEND_PROFILE_INFO_FAILURE:
      return {
        ...state,
        isLoading: false,
        errors:    [action.payload]
      };

    case RESET_AUTH:
      return {...initialState};

    case REHYDRATE: {
      const incoming = action.payload.auth;
      if (incoming) return {...state, ...incoming, isAuthenticated: false, isLoading: false, errors: []};

      return state;
    }

    default:
      return state;
  }
};

export default reducer;
