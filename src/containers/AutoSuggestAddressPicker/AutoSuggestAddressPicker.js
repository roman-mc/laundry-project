import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {
  View,
  TouchableOpacity,
  FlatList,
  Text,
  Image,
  StyleSheet,
  Platform
} from 'react-native';
// import KeyboardSpacer from 'react-native-keyboard-spacer';
const debounce = require('lodash/debounce');
const uniq = require('lodash/uniq');

import Hr from '../../components/Hr';
import Input from '../../components/Input';
import TextHint from '../../components/TextHint';

import {suggestAddresses} from '../../api';

class AutoSuggestAddressPicker extends Component {
  constructor (props) {
    super(props);

    this.state = {
      addresses:  [],
      hintShowed: false,
      inputValue: ''
    };

    this.renderItem = this.renderItem.bind(this);
    this.getSuggestedAddresses = debounce(this.getSuggestedAddresses.bind(this), 250);
    this.onChangeText = this.onChangeText.bind(this);
  }

  onSuccess (address) {
    // TODO
    // console.log('onSUCCESS!!');
    this.props.navigation.state.params.onAddressPick(address);
    this.props.navigation.goBack();
  }

  getSuggestedAddresses (text) {
    return new Promise((resolve, reject) => {
      suggestAddresses(text)
        .then(addresses => {
          const filteredAddresses = uniq(addresses);

          this.setState({
            addresses: filteredAddresses
          });

          resolve(filteredAddresses);
        })
        .catch(e => {
          console.log(e);
          reject(e);
        });
    });
  }

  onChangeText (text) {
    this.setState({inputValue: text, hintShowed: false});

    this.getSuggestedAddresses(text);
  }

  onAddressPress (address) {
    const streetRegExp = /улица|проезд|шоссе|проспект|переулок|набережная|бульвар|площадь/i;
    // Address without country and city
    const validAddress = address
      .replace('Москва, ', '')
      .replace('Московская область, ', '');

    const numberOfAddress = validAddress
      .slice(validAddress.lastIndexOf(',') + 1)
      .trim(); // Check if it is number


    if (numberOfAddress.search(/^\d|\d$/) !== -1 &&
        numberOfAddress.search(streetRegExp) === -1 &&
        validAddress.search(streetRegExp) !== -1) {
      this.onSuccess(validAddress);
    }
    else {
      this.setState({
        inputValue: validAddress,
        hintShowed: true
      });

      this.getSuggestedAddresses(address)
        .then(addresses => {
          const reqAddress = address;

          for (const a of addresses) {
            const resAddress = a;

            if (resAddress.indexOf(reqAddress) !== -1) {
              const numberOfAddress = resAddress
                .slice(resAddress.lastIndexOf(',') + 1)
                .trim();

              if (numberOfAddress.search(/^\d|\d$/) !== -1 &&
                    numberOfAddress.search(streetRegExp) === -1 &&
                    resAddress.search(streetRegExp) !== -1) {
                break;
              }
              else {
                this.getSuggestedAddresses(address + ' 1');
                break;
              }
            }
          }

          if (addresses.length === 0) {
            this.getSuggestedAddresses(address + ' 1');
          }
        });
    }

  }

  renderItem ({item, index}) {
    return (
      <View
        style={styles.listItemContainer}
        key={item}
      >
        <TouchableOpacity
          onPress={() => this.onAddressPress(item)}
          style={styles.listItem}
        >
          <Text numberOfLines={1}>{item}</Text>
        </TouchableOpacity>
      </View>
    );
  }

  keyExtractor (item) {
    return item;
  }

  render () {
    return (
      <View style={styles.container}>
        <Input
          onChangeText={this.onChangeText}
          style={styles.input}
          placeholder='Улица и дом'
          autoFocus
          autoCorrect={false}
          value={this.state.inputValue}
        />
        <Image
          source={require('../../icons/Search_Icon.png')}
          style={styles.inputIcon}
        />
        <FlatList
          style={{flex: 1}}
          data={this.state.addresses}
          renderItem={this.renderItem}
          // ItemSeparatorComponent={Hr}
          ref={(ref) => this.flat = ref}
          keyExtractor={this.keyExtractor}
          keyboardShouldPersistTaps='handled'
          ListHeaderComponent={
            this.state.hintShowed ?
              <View style={styles.hintContainer}>
                <Text style={styles.hintText}>
                  Выберите дом!
                </Text>
              </View>
            : null
          }
          // ListFooterComponent={Platform.OS === 'ios' ? <KeyboardSpacer /> : null}
        />

      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    width:           '100%',
    backgroundColor: '#f0f0f0',
    flex:            1
  },
  input: {
    width:             '100%',
    textAlign:         'left',
    backgroundColor:   '#fff',
    borderBottomWidth: 0,
    marginBottom:      0,
    paddingLeft:       50
  },
  inputIcon: {
    position: 'absolute',
    top:      10,
    left:     15,
  },
  hintContainer: {
    backgroundColor: '#E7E7E7'
  },
  hintText: {
    marginVertical: 8,
    fontSize:       12,
    color:          '#000',
    textAlign:      'center'
  },
  listItem: {
    alignItems:     'center',
    justifyContent: 'center',
    padding:        10,
    paddingTop:     20,
    paddingBottom:  20,
  },
  listItemContainer: {
    borderBottomColor: '#d6d6d6',
    borderBottomWidth: 1
  }
});

AutoSuggestAddressPicker.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default AutoSuggestAddressPicker;
