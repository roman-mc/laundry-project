import React, {Component} from 'react';
import {
    View,
    Text,
    StyleSheet,
    ScrollView,
    Keyboard,
    TouchableWithoutFeedback,
    Platform,
    findNodeHandle,
    Animated
} from 'react-native';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {asYouType, isValidNumber, parse} from 'libphonenumber-js';

import Input from '../../components/Input';
import Button from '../../components/Button';

import {requestSMS} from '../../store/actions';
import {width, color} from '../../styles';

class Login extends Component {
  constructor (props) {
    super(props);

    this.state = {
      phoneNumber:  '',
      phoneCode:    '+7',
      isValidPhone: false,
      isSmsSent:    false
    };

    this.onInputPhone = this.onInputPhone.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  componentDidMount () {
    // console.log(findNodeHandle(this.content));
    // console.log(this.container);
    // console.log(this.content);
    // setTimeout(() => {
    //   this.content.measure((...a) => console.log(a));
    //   this.content.measureInWindow((...a) => console.log(a));
    //
    // }, 300);
    // Keyboard.addListener('keyboardDidShow', (...a) => {
    //   console.log(a);
    // });
    // Keyboard.addListener('keyboardDidHide', ({nativeEvent}) => {
    //   console.log(nativeEvent);
    // });
    // this.content.measureLayout(findNodeHandle(this.content), console.log);
  }

  handleKeyboardOpen () {
    
  }

  onSubmit () {
    if (this.state.isValidPhone) {
      this.props.requestSMS(this.state.phoneCode + ' ' + this.state.phoneNumber);
    }
  }

  onInputPhone (text) {
    const plainNumbers = text.replace(/[^\d]/g, '');
    if (plainNumbers.length > 10) return;

    const phoneNumber = new asYouType('RU').input(text);

    this.setState({
      phoneNumber,
      isValidPhone: isValidNumber(phoneNumber, 'RU')
    });
  }

  render () {
    const isValidPhone = this.state.isValidPhone;
    const emptyPhoneInput = this.state.phoneNumber.length === 0;
    const phoneInputStyle = isValidPhone && !emptyPhoneInput ? styles.inputSuccess :
          !isValidPhone && this.state.phoneNumber.length > 0 ? styles.inputError :
          null;

    return (
      <TouchableWithoutFeedback
        onPress={() => Keyboard.dismiss()}
      >
        <View style={styles.container} ref={(r) => this.container = r}>
          <Text style={[styles.text, styles.h1]}>Введите номер телефона</Text>
          <Text style={[styles.text, styles.content]}>Мы отправим Вам СМС с кодом</Text>

            {/* <ScrollView scrollEnabled={false}
              contentContainerStyle={styles.smsBlock}
              keyboardShouldPersistTaps='handled'
              > */}
              <View style={{flex: 1, marginTop: '30%'}} ref={(r) => this.content = r}>
                <TouchableWithoutFeedback
                  onPress={() => this.phoneInput.focus()}
                >
                  <View
                    style={styles.inputContainer}
                  >
                    <Text style={styles.inputPhonePrefix}>+7</Text>
                    <Input
                      inputRef={ref => this.phoneInput = ref}
                      style={[
                        styles.phoneInput,
                        phoneInputStyle
                      ]}
                      keyboardType='phone-pad'
                      onChangeText={this.onInputPhone}
                      value={this.state.phoneNumber}
                      placeholder='(999) 999 99 99'
                      onSubmitEditing={this.onSubmit}
                      blurOnSubmit
                    />
                  </View>
                </TouchableWithoutFeedback>

                <Button
                  style={styles.button}
                  disabledStyle={styles.buttonDisabled}
                  onPress={this.onSubmit}
                  isDisabled={!this.state.isValidPhone}
                  isLoading={this.props.isLoading}
                >
                  Получить код
                </Button>
              </View>
                {/* {Platform.OS === 'ios'
                  ? <KeyboardSpacer />
                  : null
                } */}
            {/* </ScrollView> */}
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex:       1,
    alignItems: 'center',
    width:      '100%'
  },
  text: {
    marginTop: 15,
    fontSize:  20
  },
  h1: {
    fontWeight: 'bold'
  },
  content: {
    fontSize: 16
  },
  smsBlock: {
    justifyContent: 'flex-end',
    alignItems:     'flex-start',
    flex:           1,
    width:          '100%'
  },
  button: {
    marginBottom:   10,
    borderRadius:   10,
    // width:          '70%',
    width:          width.full * 0.7,
    alignSelf:      'center',
    alignItems:     'center',
    justifyContent: 'center'
  },
  buttonDisabled: {
    backgroundColor: color.white,
    borderColor:     color.disabled,
    borderWidth:     1
  },
  inputContainer: {
    // width:             '70%',
    width:             width.full * 0.7,
    borderBottomWidth: 1,
    marginBottom:      10,
    paddingVertical:   10,
    flexDirection:     'row',
    justifyContent:    'center'
  },
  inputPhonePrefix: {
    fontSize:  16,
    // width:     '10%',
    textAlign: 'right',
    flexGrow:  Platform.OS === 'ios' ? 2 : 1
  },
  phoneInput: {
    borderBottomWidth: 0,
      // alignSelf:         'center',
    textAlign:         'left',
    textAlignVertical: 'top',
      // width:             'auto',
    paddingTop:        0,
    paddingBottom:     0,
    fontSize:          16,
    marginLeft:        4,
    marginRight:       0,
    marginBottom:      0,
    flexGrow:          Platform.OS === 'ios' ? 3 : 2,
      // flexDirection:     'row',
    width:             'auto',
  }
});

Login.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired,
  navigation:      PropTypes.object.isRequired,
  isLoading:       PropTypes.bool.isRequired,
  requestSMS:      PropTypes.func.isRequired,
  errors:          PropTypes.array.isRequired
};

const mapStateToProps = (state) => ({
  isAuthenticated: state.auth.isAuthenticated,
  isLoading:       state.auth.isLoading,
  errors:          state.auth.errors
});

const mapDispatchToProps = {
  requestSMS
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
