import React, {Component} from 'react';
import {
    View,
    Text,
    StyleSheet,
    ScrollView,
    Keyboard,
    Alert,
    Platform,
    ActivityIndicator,
    TouchableWithoutFeedback
} from 'react-native';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {NavigationActions} from 'react-navigation';

import {
  getOrders
} from '../../api';
import {
  sendSmsCode,
  getCards,
  getAddresses,
  resetNavigation
} from '../../store/actions';
import Button from '../../components/Button';
import Input from '../../components/Input';
import {color, width, buttonDisabled, button} from '../../styles';
import analytics from '../../modules/analytics';
import crashlytics from '../../modules/crashlytics';

import {isClosedOrderStatus} from '../../utils';


class InsertCode extends Component {
  constructor (props) {
    super(props);

    this.state = {
      smsCode:   '',
      showError: false
    };
    this.phoneNumber = props.auth.phone;

    this.input = null;
    this.sendSmsCode = this.sendSmsCode.bind(this);
  }

  sendSmsCode (code) {
    this.props.sendSmsCode(code)
      .then(profile => {

        this.props.getCards();
        this.props.getAddresses();

        analytics.setUserId(profile.token);
        analytics.setUserProperty('phone_number', profile.phone);
        analytics.logEvent('user_logged', {when: 'initial'});
        crashlytics.setUserName(profile.name);
        crashlytics.setUserIdentifier(profile.phone);

        return getOrders(['limit=10'], profile.token)
          .then((ordersResponse) => {
            this.props.resetNavigation('Login');
            if (ordersResponse.totalCount === 0) {
              this.props.navigation.navigate('MakeOrder');
            }
            else {
              const activeOrder = ordersResponse.orders.find(order =>
                order.statusText !== '' && (isClosedOrderStatus(order.statusText) === false)
              );
              if (activeOrder === undefined) {
                return this.props.navigation.navigate('MakeOrder');
              }

              const navigationToOrder = NavigationActions.navigate({
                routeName: 'OrdersStack',

                params: {},
                action: NavigationActions.navigate(
                  {routeName: 'Order', params: {orderId: activeOrder.id}}
                ),
              });

              this.props.navigation.dispatch(navigationToOrder);
            }
          });
      })
      .catch(e => {
        this.setState({
          showError: true
        });
        this.input.focus();
      });
    Keyboard.dismiss();
  }

  render () {
    const isValidCode = this.state.smsCode.length === 4;
    const inputStyle =  isValidCode ? styles.inputSuccess : null;

    return (
      <TouchableWithoutFeedback
        onPress={() => Keyboard.dismiss()}
      >
        <View style={styles.container}>
          <Text style={[styles.text, styles.h1]}>Введите код</Text>
          <Text style={[styles.text, styles.content]}>Мы отправили его на номер</Text>
          <Text style={[styles.text, styles.content]}>{this.phoneNumber}</Text>

              <View style={{flex: 1, marginTop: '12%'}}>
                {
                  <Text style={{textAlign: 'center', fontSize: 15}}>
                    {
                      this.state.showError
                        ? 'Неправильный код'
                        : ' '
                    }
                  </Text>
                }
                <Input
                  style={[
                    styles.input,
                    inputStyle
                  ]}
                  onChangeText={(smsCode) => {
                    if (smsCode.length > 4) {
                      return;
                    }
                    if (smsCode.length === 4) {
                      this.sendSmsCode(smsCode);
                    }
                    this.setState({smsCode, showError: false});
                  }}
                  value={this.state.smsCode}
                  placeholder='Ваш код'
                  autoCorrect={false}
                  keyboardType='numeric'
                  multiline={false}
                  inputRef={ref => this.input = ref}
                />
              </View>
            {
              this.props.isLoading
              ? <ActivityIndicator
                  style={{
                    flex:            1,
                    position:        'absolute',
                    backgroundColor: 'rgba(0, 0, 0, 0.15)',
                    width:           '100%',
                    height:          '100%'
                  }}
                  color='white'
                  size='large'
                />
              : null
            }
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex:       1,
    alignItems: 'center',
    width:      '100%'
  },
  text: {
    marginTop: 15,
    fontSize:  20
  },
  h1: {
    fontWeight: 'bold'
  },
  content: {
    fontSize: 16
  },
  smsBlock: {
    justifyContent: 'flex-end',
    alignItems:     'flex-start',
    flex:           1,
    width:          '100%'
  },
  input: {
    borderBottomWidth: 1,
    alignSelf:         'center',
    textAlign:         'center',
    width:             width.full * 0.7,
    paddingTop:        10,
    paddingBottom:     10,
    marginLeft:        10,
    marginRight:       10,
    marginBottom:      10,
    fontSize:          16
  }
});

// TODO: implement these methods
InsertCode.propTypes = {
  navigation:      PropTypes.object.isRequired,
  isLoading:       PropTypes.bool.isRequired,
  phoneNumber:     PropTypes.string.isRequired,
  sendSmsCode:     PropTypes.func.isRequired,
  getCards:        PropTypes.func.isRequired,
  getAddresses:    PropTypes.func.isRequired,
  resetNavigation: PropTypes.func.isRequired,
  auth:            PropTypes.object.isRequired
};

const mapStateToProps = (state, ownProps) => ({
  auth:        state.auth,
  isLoading:   state.auth.isLoading,
  phoneNumber: state.auth.phone,
});

const mapDispatchToProps = {
  sendSmsCode,
  getCards,
  getAddresses,
  resetNavigation
};

export default connect(mapStateToProps, mapDispatchToProps)(InsertCode);
