import React, {Component} from 'react';
import {
  FlatList,
  StyleSheet,
  View,
  Switch,
  TouchableOpacity,
  Alert,
  Text,
  Image
} from 'react-native';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';

import {
  getCards,
  activateCard, deactivateCard, deleteCard
} from '../../store/actions';
import Button from '../../components/Button';
import {color, width} from '../../styles';

const formatCardPan = string => {
  let formattedPan = '';

  formattedPan += string.slice(0, 4);
  formattedPan += ' ';
  formattedPan += string.slice(4, 8);
  formattedPan += ' ';
  formattedPan += string.slice(8, 12);
  formattedPan += ' ';
  formattedPan += string.slice(12);

  return formattedPan;
};

class Cards extends Component {
  constructor (props) {
    super(props);

    this.renderCard = this.renderCard.bind(this);
    this.navigateToAddCard = this.navigateToAddCard.bind(this);
    this.activateCard = this.activateCard.bind(this);
    this.deactivateCard = this.deactivateCard.bind(this);
    this.deleteCard = this.deleteCard.bind(this);
  }

  componentDidMount () {
    this.props.getCards();
  }

  deleteCard (bindingHash) {
    Alert.alert(
      'Удалить карту ?',
      undefined,
      [
        {text: 'Нет', onPress: () => undefined},
        {text: 'Да', onPress: () => this.props.deleteCard(bindingHash), style: 'destructive'}
      ]
    );

  }

  activateCard (card) {
    this.props.activateCard(card);

  }

  deactivateCard (card) {
    this.props.deactivateCard(card);
  }

  navigateToAddCard () {
    this.props.navigation.navigate(
      'AddCreditCard',
      {
        onSuccess: () => {
          this.props.getCards();
        },
        onError: () => {
          this.props.getCards();
        }
      }
    );
  }

  renderCard (data) {
    const {item, index} = data;

    return (
      <View
        key={item.bindingHash}
        style={[
          styles.listItemContainer,
          index === 0 ? {borderTopWidth: 1} : null
        ]}
      >
          {/* <Image source={require('../../icons/vis_card_icon.png')} /> */}
          <Text style={{fontSize: 17}} numberOfLines={1}>
            {formatCardPan(item.pan)}

          </Text>
          <View style={styles.cardRightContent}>
            <Switch
              style={{marginRight: 20}}
              disabled={this.props.isLoading}
              value={item.active}
              onChange={() => item.active
                ? this.deactivateCard(item) : this.activateCard(item)
              }
            />
            <TouchableOpacity
              style={{padding: 10}}
              onPress={() => !this.props.isLoading ? this.deleteCard(item.bindingHash) : null}
            >
              <Image source={require('../../icons/delete_icon.png')} />
            </TouchableOpacity>
          </View>
      </View>
    );
  }

  cardKeyExtractor (item) {
    return item.bindingHash;
  }

  render () {
    return (
      <View style={styles.container}>
        <Button
          onPress={() =>
            this.navigateToAddCard()
          }
          style={styles.buttonAdd}
        >
          <Text style={styles.buttonAddText}>Добавить карту</Text>
        </Button>

        <FlatList
          style={styles.list}
          data={this.props.cards}
          extraData={this.props.isLoading}
          renderItem={this.renderCard}
          keyExtractor={this.cardKeyExtractor}
          refreshing={this.props.isLoading && this.props.cards.length === 0}
          onRefresh={this.props.getCards}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  addressContainer: {
    backgroundColor:   color.white,
    borderBottomWidth: 1,
    borderColor:       '#d5d5d5',
    flexDirection:     'row',
    paddingLeft:       20,
    paddingVertical:   10,
    paddingRight:      10,
    justifyContent:    'space-between',
    alignItems:        'center'
  },
  list: {
    marginTop: 20
  },
  listItemContainer: {
    backgroundColor:   color.white,
    borderBottomWidth: 1,
    borderColor:       '#d5d5d5',
    flexDirection:     'row',
    paddingLeft:       20,
    paddingVertical:   10,
    paddingRight:      10,
    justifyContent:    'space-between',
    alignItems:        'center'
  },
  cardRightContent: {
    flexDirection:  'row',
    alignItems:     'center',
    justifyContent: 'flex-end'
  },
  header: {
    flexDirection:   'row',
    justifyContent:  'space-between',
    alignItems:      'center',
    backgroundColor: color.white,
    paddingTop:      20,
    paddingBottom:   20,
    paddingLeft:     15,
    paddingRight:    15,
  },
  profileName: {
    fontSize: 19,
    color:    '#222022'
  },
  profileEmail: {
    fontSize:  17,
    color:     '#5C5C5C',
    marginTop: 10
  },
  buttonAdd: {
    width:           width.full - 30,
    marginLeft:      15,
    marginRight:     15,
    marginTop:       20,
    marginBottom:    0,
    height:          'auto',
    paddingVertical: 10
  },
  buttonAddText: {
    fontSize: 15,
    color:    color.white,
  }
});

Cards.propTypes = {
  getCards:       PropTypes.func.isRequired,
  activateCard:   PropTypes.func.isRequired,
  deactivateCard: PropTypes.func.isRequired,
  deleteCard:     PropTypes.func.isRequired,
  navigation:     PropTypes.object.isRequired,
  cards:          PropTypes.array.isRequired,
  isLoading:      PropTypes.bool.isRequired,
};

const mapStateToProps = state => ({
  cards:     state.cards.cards,
  isLoading: state.cards.isLoading
});

const mapDispatchToProps = {
  getCards,
  activateCard,
  deactivateCard,
  deleteCard
};

export default connect(mapStateToProps, mapDispatchToProps)(Cards);
