// TODO: refactor most of these chunks put in components
import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    ScrollView,
    Text
} from 'react-native';
import PropTypes from 'prop-types';
import Button from 'apsl-react-native-button';
import {connect} from 'react-redux';
import update from 'immutability-helper';
import KeyboardSpacer from 'react-native-keyboard-spacer';

import {change, sendOrder, changePromocode} from '../../store/actions';
import {width, color} from '../../styles';

import Input from '../../components/Input';
// import { Octicons } from '@expo/vector-icons';
import InputArea from '../../components/InputArea';
import TextHint from '../../components/TextHint';

class Ordering extends Component {
  constructor (props) {
    super(props);

    this.state = {isLoading: false};
    this.onSubmit = this.onSubmit.bind(this);
    // TODO: make anotoher action in actions
    this.onChangePromocode = this.props.change.bind(null, 'promocode');
    this.onChangeComment = this.props.change.bind(null, 'comment');

  }

  componentDidUpdate (prevProps) {
    if (prevProps.isLoading && !this.props.isLoading
      && prevProps.order.orderID.length === 0 && this.props.order.orderID.length !== 0) {
      this.props.navigation.navigate('OrderInfo');
    }
  }

  onChangePromocode (text) {
    this.props.changePromocode(text);
  }

  onSubmit () {
    this.props.sendOrder();
  }

  render () {
    const {order} = this.props;
    const {promocode} = order;
    const showPromocodeHint = order.promocode.length > 0 && !order.isPromocodeValid;

    return (
      <View style={styles.container}>
        <View style={styles.contentTop}>
          {/* Question time and date START */}
          <View style={styles.promocodeContainer}>
            <View>
              <Input
                placeholder='Промокод'
                onChangeText={this.props.changePromocode}
                style={[styles.promocodeInput]}
                value={promocode.value}
              />
              <Text style={{
                marginTop: 10,
                color:     promocode.isValid ? color.airo :
                  !promocode.isLoading && promocode.value.length > 0 && !promocode.isValid ? color.error
                  : color.black
              }}>
                {
                  promocode.isValid ? `Скидка по промокоду: ${promocode.discount}` :
                  promocode.isLoading ? `Проверка промокда...` :
                  promocode.value.length > 0 && !promocode.isValid ? 'Промокод не действителен' :
                  'Скидка по промокоду: 0'
                }
              </Text>
            </View>
          </View>
          <InputArea
            placeholder='Комментарии к заказу'
            style={styles.inputArea}
            numberOfLines={4}
            multiline
            onChangeText={this.onChangeComment}
            blurOnSubmit
            value={order.comment}
          />
        </View>

        <ScrollView scrollEnabled={false}
          contentContainerStyle={styles.bottomContent}
          keyboardShouldPersistTaps='handled'
          >
            <Button
              style={[styles.button]}
              onPress={this.onSubmit}
              // onPress={}
              isLoading={order.isLoading}
            >
              Заказать
            </Button>
            <KeyboardSpacer />
        </ScrollView>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex:           1,
    alignItems:     'center',
    justifyContent: 'space-between',
    width:          width.full
  },
  contentTop: {
    width:      '100%',
    alignItems: 'center'
  },
  button: {
    width:           width.bigContainer,
    alignSelf:       'center',
    borderWidth:     0,
    backgroundColor: color.gold,
    padding:         10,
    borderRadius:    5,
  },
  promocodeContainer: {
    // flexDirection: 'row',
    width: width.bigContainer
  },
  promocodeInput: {
    fontSize:     15,
    marginLeft:   0,
    marginRight:  0,
    marginBottom: 0,
    width:        '100%',
  },
  promocodeHint: {
    marginTop: 5
  },
  inputArea: {
    fontSize:  14,
    height:    100,
    flexWrap:  'wrap',
    marginTop: 25,
    width:     width.bigContainer
  },
  bottomContent: {
    justifyContent: 'flex-end',
    alignItems:     'flex-start',
    flex:           1,
    width:          '100%'
  }
});

Ordering.propTypes = {
  isLoading:       PropTypes.bool,
  navigation:      PropTypes.object.isRequired,
  sendOrder:       PropTypes.func.isRequired,
  order:           PropTypes.object.isRequired,
  change:          PropTypes.func.isRequired,
  changePromocode: PropTypes.func.isRequired
};

const mapStateToProps = (state, ownProps) => ({
  order:      state.order,
  isLoading:  state.order.isLoading,
  navigation: ownProps.navigation,
});

const mapDispatchToProps = {
  change,
  sendOrder,
  changePromocode,
};

export default connect(mapStateToProps, mapDispatchToProps)(Ordering);
