// TODO: refactor most of these chunks put in components
import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  Switch,
  Modal,
  KeyboardAvoidingView,
  Keyboard,
  TouchableOpacity,
  Platform
} from 'react-native';
import PropTypes from 'prop-types';
import Button from 'apsl-react-native-button';
import {connect} from 'react-redux';
import Octicons from 'react-native-vector-icons/Octicons';
import update from 'immutability-helper';
import debounce from 'lodash/debounce';

// import {suggestAddresses as sugAddr} from '../../api/maps';

import {
  getLastUsedAddress
} from '../../api';
import {
  changeFrom, changeTo, change, changeAddress,
  suggestAddresses,
  getAbleTimeslots,
  getAddresses,
  getCards
} from '../../store/actions';
import {Address, convertServerAddress} from '../../utils';
import AutoSuggest from './AutoSuggest';
import Input from '../../components/Input';
import {width, color, button, buttonDefault, keyboardVerticalOffset} from '../../styles';
import Header from '../../components/Header';
import AddressInputs from '../../components/AddressInputs';
import InputArea from '../../components/InputArea';
import TextHint from '../../components/TextHint';

class PickAddress extends Component {
  constructor (props) {
    super(props);

    this.state = {
      modal: {
        hintShowed:     false,
        isVisible:      false,
        inputValue:     '',
        onChangeText:   null,   // requests autosuggest api, doesn't change from/to state
        onPress:        null,
        addressTouched: false
      }
    };

    this.onFocusBottomInputs = this.onFocusBottomInputs.bind(this);
    this.onAddressPress = this.onAddressPress.bind(this);
    this.toggleModal = this.toggleModal.bind(this);
    this.onAddressInputChange = debounce(this.onAddressInputChange.bind(this), 200);

    this.onSubmit = this.onSubmit.bind(this);
    this.onChange = this.onChange.bind(this);

    this.onChangeSameAddress = this.onChange.bind(this, 'sameDeliveryAddress');
    this.setShouldGetTimeslots = this.onChange.bind(this, 'shouldGetTimeslots', true);

    // on address change from
    this.onChangeKvartiraFrom = this.onChangeFrom.bind(this, 'kvartira');
    this.onChangePodezdFrom = this.onChangeFrom.bind(this, 'podezd');
    this.onChangeEtajFrom = this.onChangeFrom.bind(this, 'etaj');
    this.onChangeDomofonFrom = this.onChangeFrom.bind(this, 'domofon');
    this.onChangeCommentFrom = this.onChangeFrom.bind(this, 'addressComment');

    // on address change to
    this.onChangeKvartiraTo = this.onChangeTo.bind(this, 'kvartira');
    this.onChangePodezdTo = this.onChangeTo.bind(this, 'podezd');
    this.onChangeEtajTo = this.onChangeTo.bind(this, 'etaj');
    this.onChangeDomofonTo = this.onChangeTo.bind(this, 'domofon');
    this.onChangeCommentTo = this.onChangeTo.bind(this, 'addressComment');

    this.openModalAddressFrom = this.openModalAddress.bind(this, 'addressFrom');
    this.openModalAddressTo = this.openModalAddress.bind(this, 'addressTo');

    this.onChangeAddressFrom = this.onChangeAddressFrom.bind(this);
    this.onChangeAddressTo = this.onChangeAddressTo.bind(this);
  }

  componentDidMount () {
    getLastUsedAddress(this.props.auth.token)
      .then(addresses => {
        const {dataFrom} = this.props.order;

        if (
            dataFrom.address === '' &&
            dataFrom.addressComment === '' &&
            dataFrom.domofon === '' &&
            dataFrom.etaj === '' &&
            dataFrom.kvartira === '' &&
            dataFrom.podezd === ''
          ) {
            // 1 address
            // OR
            // 2 equal addresses
          if (
            addresses.length === 1 ||
            addresses.length === 2 && (addresses[0].id === addresses[1].id)
          ) {
            this.onChangeSameAddress(true);
            this.onChangeAddressFrom(convertServerAddress(addresses[0]));
          }
          // 2 different addresses
          else if (addresses.length === 2) {
            this.onChangeSameAddress(false);
            this.onChangeAddressFrom(convertServerAddress(addresses[0]));
            this.onChangeAddressTo(convertServerAddress(addresses[1]));
          }
        }

      })
      .catch(console.log);

    // Get users cards for AddCardOrSkip scene
    this.props.getCards();
  }
  // shouldComponentUpdate () {
  //   // TODO
  //   // Optimize according to index at screens stack position(if it is not last, then false)
  // }

  /**
   * @param  {Address}  value
   * @param  {Boolean} [isAddressCorrect=true]
   */
  onChangeAddressFrom (value, isAddressCorrect = true) {
    this.props.changeAddress('from', value, isAddressCorrect);
  }

  /**
   * @param  {Address}  value
   * @param  {Boolean} [isAddressCorrect=true]
   */
  onChangeAddressTo (value, isAddressCorrect = true) {
    this.props.changeAddress('to', value, isAddressCorrect);
  }

  onChangeFrom (prop, value) {
    this.props.changeFrom(prop, value);
  }

  onChangeTo (prop, value) {
    this.props.changeTo(prop, value);
  }

  onChange (prop, value) {
    this.props.change(prop, value);
  }

  onSubmit () {
    if (this.props.order.shouldGetTimeslots) {
      this.props.getAbleTimeslots((err) => {
        if (!err) {
          this.props.navigation.navigate('PickTime');
        }
      });
    }
    else {
      this.props.navigation.navigate('PickTime');
    }

  }

  /**
   * @return {String}
   */
  onAddressInputChange (address) {
    this.setState({
      modal: update(this.state.modal, {
        inputValue: {$set: address},
        hintShowed: {$set: false}
      })
    });

    this.props.getSuggestedAddresses(address);
  }

  /**
   * @param  {Object} address
   * @param  {string} address.address
   * @param  {Object} address.details <- etaj,podezd,kvartira,domofon
   */
  onAddressPress (address) {
    const streetRegExp = /улица|проезд|шоссе|проспект|переулок|набережная|бульвар|площадь/i;

    // Address without country and city
    const validAddress = address.address
      .replace('Москва, ', '')
      .replace('Московская область, ', '');

    const numberOfAddress = validAddress
      .slice(validAddress.lastIndexOf(',') + 1)
      .trim(); // Check if it is number


    if (numberOfAddress.search(/^\d|\d$/) !== -1 &&
        numberOfAddress.search(streetRegExp) === -1 &&
        validAddress.search(streetRegExp) !== -1) {
      // FIXME: create new address, or mutating this is norm ?
      address.address = validAddress;
      this.toggleModal();
      this.state.modal.onPress(address);
    }
    else {
      this.setState({
        modal: update(this.state.modal, {
          inputValue: {$set: validAddress},
          hintShowed: {$set: true}
        })
      });

      this.props.getSuggestedAddresses(address.address)
        .then(addresses => {
          const reqAddress = address.address;

          for (const a of addresses) {
            const resAddress = a.address;

            if (resAddress.indexOf(reqAddress) !== -1) {
              const numberOfAddress = resAddress
                .slice(resAddress.lastIndexOf(',') + 1)
                .trim();

              if (numberOfAddress.search(/^\d|\d$/) !== -1 &&
                    numberOfAddress.search(streetRegExp) === -1 &&
                    resAddress.search(streetRegExp) !== -1) {
                break;
              }
              else {
                this.props.getSuggestedAddresses(address.address + ' 1');
                break;
              }
            }
          }
        });
    }
  }

  openModalAddress (stateProperty) {
    const isFrom = stateProperty === 'addressFrom';
    if (this.props.order.suggestedAddresses.length === 0) {
      this.props.getSuggestedAddresses();
    }
    Keyboard.dismiss();

    this.setState({
      modal: {
        hintShowed: false,
        isVisible:  true,
        inputValue: isFrom
          ? this.props.order.dataFrom.address
          : this.props.order.dataTo.address,
        onChangeText: this.onAddressInputChange,   // requests autosuggest api, doesn't change from/to state
        onPress:      isFrom
          ? this.onChangeAddressFrom
          : this.onChangeAddressTo,
        onAcceptNotCorrectAddress: () => {
          if (isFrom) {
            this.onChangeAddressFrom(new Address(this.state.modal.inputValue), false);
          }
          else {
            this.onChangeAddressTo(new Address(this.state.modal.inputValue), false);
          }

          this.toggleModal();
        }
      }
    });
  }

  toggleModal () {
    const isVisible = !this.state.modal.isVisible;

    this.setState({
      modal: update(this.state.modal, {isVisible: {$set: isVisible}})
    });
  }

  acceptNotCorrectAddress () {
    const isVisible = !this.state.modal.isVisible;

    this.setState({
      modal: update(this.state.modal, {isVisible: {$set: isVisible}})
    });


  }

  onFocusBottomInputs () {
    setTimeout(() => this._scrollView.scrollToEnd({animated: true}), 250); // Typically 250ms is the time of keyboard animation
  }


  render () {
    const {order} = this.props;
    const {dataFrom, dataTo} = order;

    return (
      <View style={styles.container}>
        <KeyboardAvoidingView
          behavior={Platform.OS === 'ios' ? 'padding' : null}
          keyboardVerticalOffset={keyboardVerticalOffset}
          >
          <ScrollView
            contentContainerStyle={[styles.scrollViewContainer, this.state.sameDeliveryAddress
              ? {flex: 1}
              : null]}
            keyboardShouldPersistTaps='handled'
            ref={ref => this._scrollView = ref}
            >
              <View
                style={styles.topForm}
              >
                <View style={{alignItems: 'center'}}>
                  {/* Question time and date START */}
                  <View style={styles.deliveryTitleContainer}>
                    <Text style={[styles.questionText, styles.deliveryTitle]}>
                      Откуда забрать вещи
                    </Text>
                  </View>
                  <AddressInputs
                    address={dataFrom.address}
                    kvartira={dataFrom.kvartira}
                    podezd={dataFrom.podezd}
                    etaj={dataFrom.etaj}
                    domofon={dataFrom.domofon}
                    comment={dataFrom.addressComment}
                    // onChangeAddress={this.onChangeAddressFrom}
                    onChangeKvartira={this.onChangeKvartiraFrom}
                    onChangePodezd={this.onChangePodezdFrom}
                    onChangeEtaj={this.onChangeEtajFrom}
                    onChangeDomofon={this.onChangeDomofonFrom}
                    onChangeComment={this.onChangeCommentFrom}
                    // style={{marginTop: 10}}
                    onAddressPress={this.openModalAddressFrom}
                    onCommentFocus={order.sameDeliveryAddress
                      ? this.onFocusBottomInputs
                      : null
                    }
                  />
                  {/* </View> */}
                  {/* Question time and date END */}
                  {/* Question time and date START */}
                  <View style={styles.deliveryTitleContainer}>
                    <Text style={[styles.questionText, styles.deliveryTitle]}>
                      Куда привезти вещи
                    </Text>
                  </View>
                  <View style={styles.checkboxContainer}>
                    <Text style={styles.checkboxText}>
                      По тому же адресу
                    </Text>
                    <Switch
                      onValueChange={(value) => {
                        this.onChangeSameAddress(value);
                        this.setShouldGetTimeslots();
                      }}
                      value={order.sameDeliveryAddress}
                      tintColor='rgb(143, 143, 143)'
                    />

                  </View>
                  {order.sameDeliveryAddress
                    ? null
                    : <AddressInputs
                        address={dataTo.address}
                        kvartira={dataTo.kvartira}
                        podezd={dataTo.podezd}
                        etaj={dataTo.etaj}
                        domofon={dataTo.domofon}
                        comment={dataTo.addressComment}
                        // onChangeAddress={this.onChangeAddressTo}
                        onChangeKvartira={this.onChangeKvartiraTo}
                        onChangePodezd={this.onChangePodezdTo}
                        onChangeEtaj={this.onChangeEtajTo}
                        onChangeDomofon={this.onChangeDomofonTo}
                        onChangeComment={this.onChangeCommentTo}
                        onAddressPress={this.openModalAddressTo}
                        onFocus={this.onFocusBottomInputs}
                    />
                  }
                </View>
                {/* Question time and date END */}
              </View>
              <View style={{
                marginTop: 15
              }}>
                <Button
                  style={[
                    styles.button,
                    order.isLoading ? styles.buttonDisabled : null
                  ]}
                  onPress={this.onSubmit}
                  isLoading={order.isLoading}
                  textStyle={[
                    styles.buttonText,
                    order.isLoading ? styles.buttonDisabledText : null
                  ]}
                >
                  Выбрать дату и время
                </Button>
              </View>
            </ScrollView>
          </KeyboardAvoidingView>
          <Modal
            visible={this.state.modal.isVisible}
            onRequestClose={() =>
              this.setState({
                modal: update(this.state.modal, {isVisible: {$set: !this.state.modal.isVisible}})
              })
            }
            animationType='slide'
          >
            <Header
              left={
                <TouchableOpacity onPress={this.toggleModal} style={{padding: 10, marginLeft: 5}}>
                  <Octicons name='x' size={20} color='#00adf5' />
                </TouchableOpacity>
              }
              title='Укажите адрес'
              right={
                <TouchableOpacity
                  onPress={this.state.modal.onAcceptNotCorrectAddress}
                  style={{padding: 10, marginLeft: 5}}
                >
                  <Text style={{color: '#00adf5', fontSize: 16}}>Принять</Text>
                </TouchableOpacity>
              }
            />
            {
              this.state.modal.isVisible
                ? <AutoSuggest
                    onPress={this.onAddressPress}
                    data={this.props.order.suggestedAddresses}
                    inputValue={this.state.modal.inputValue}
                    onChangeText={this.state.modal.onChangeText}
                    hintShowed={this.state.modal.hintShowed}
                  />
                : null
            }
          </Modal>
    </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex:            1,
    alignItems:      'center',
    width:           width.full,
    backgroundColor: 'white'
  },
  scrollViewContainer: {
    alignItems: 'center',
    // paddingBottom: 20,
    // flex:          1
  },
  topForm: {
    width:          '100%',
    alignItems:     'center',
    justifyContent: 'space-between',
    flex:           1
  },
  button: {
    width:        width.bigContainer,
    alignSelf:    'center',
    alignItems:   'center',
    padding:      10,
    borderRadius: 5,
    ...button.container
  },
  buttonText: {
    ...button.text
  },
  buttonDisabled: {
    ...buttonDefault.container
  },
  buttonDisabledText: {
    ...buttonDefault.text
  },
  questionText: {
    marginTop: 15,
    fontSize:  18,
    alignSelf: 'flex-start'
  },
  checkboxContainer: {
    flexDirection:  'row',
    alignItems:     'center',
    marginTop:      15,
    marginBottom:   0,
    justifyContent: 'space-between',
    width:          width.bigContainer
  },
  checkboxText: {
    fontSize: 16
  },
  deliveryTitleContainer: {
    alignItems: 'flex-start',
    width:      width.bigContainer
  },
  deliveryTitle: {
    marginBottom: 0
  }
});

PickAddress.propTypes = {
  isLoading:             PropTypes.bool,
  navigation:            PropTypes.object.isRequired,
  order:                 PropTypes.object.isRequired, /* either object or null */
  auth:                  PropTypes.object.isRequired,
  changeFrom:            PropTypes.func.isRequired,
  changeTo:              PropTypes.func.isRequired,
  change:                PropTypes.func.isRequired,
  changeAddress:         PropTypes.func.isRequired,
  getSuggestedAddresses: PropTypes.func.isRequired,
  getAbleTimeslots:      PropTypes.func.isRequired,
  getCards:              PropTypes.func.isRequired
};

const mapStateToProps = (state, ownProps) => ({
  navigation: ownProps.navigation,
  nav:        state.nav,
  order:      state.order,
  auth:       state.auth
});

const mapDispatchToProps = {
  changeFrom,
  changeTo,
  change,
  changeAddress,
  getSuggestedAddresses: suggestAddresses,
  getAbleTimeslots,
  getAddresses,
  getCards
};

export default connect(mapStateToProps, mapDispatchToProps)(PickAddress);
