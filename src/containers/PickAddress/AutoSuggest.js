import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  TouchableOpacity,
  FlatList,
  Text,
  Image,
  StyleSheet,
  Platform
} from 'react-native';

import Input from '../../components/Input';

class AutoSuggest extends Component {
  constructor (props) {
    super(props);

    this.renderItem = this.renderItem.bind(this);
  }

  renderItem ({item, index}) {
    return (
      <View
        style={styles.listItemContainer}
        key={item.address}
      >
        <TouchableOpacity
          onPress={() => this.props.onPress(item)}
          style={styles.listItem}
        >
          <Text numberOfLines={1}>{item.address}</Text>
        </TouchableOpacity>
      </View>
    );
  }

  keyExtractor (item) {
    return item.address;
  }

  render () {
    return (
      <View style={styles.container}>
        <Input
          defaultValue={this.props.inputValue}
          onChangeText={this.props.onChangeText}
          style={styles.input}
          placeholder='Улица и дом'
          autoFocus
          autoCorrect={false}
        />
        <Image
          source={require('../../icons/Search_Icon.png')}
          style={styles.inputIcon}
        />
        <FlatList
          style={{flex: 1}}
          data={this.props.data}
          renderItem={this.renderItem}
          ref={(ref) => this.flat = ref}
          keyExtractor={this.keyExtractor}
          keyboardShouldPersistTaps='handled'
          ListHeaderComponent={
            this.props.hintShowed ?
              <View style={styles.hintContainer}>
                <Text style={styles.hintText}>
                  Выберите дом!
                </Text>
              </View>
            : null
          }
        />

      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    width:           '100%',
    backgroundColor: '#f0f0f0',
    flex:            1
  },
  input: {
    width:             '100%',
    textAlign:         'left',
    backgroundColor:   '#fff',
    borderBottomWidth: 0,
    marginBottom:      0,
    paddingLeft:       50
  },
  inputIcon: {
    position: 'absolute',
    top:      10,
    left:     15,
  },
  hintContainer: {
    backgroundColor: '#E7E7E7'
  },
  hintText: {
    marginVertical: 8,
    fontSize:       12,
    color:          '#000',
    textAlign:      'center'
  },
  listItem: {
    alignItems:     'center',
    justifyContent: 'center',
    padding:        10,
    paddingTop:     20,
    paddingBottom:  20,
  },
  listItemContainer: {
    borderBottomColor: '#d6d6d6',
    borderBottomWidth: 1
  }
});

AutoSuggest.propTypes = {
  data:         PropTypes.array.isRequired,
  inputValue:   PropTypes.string.isRequired,
  onChangeText: PropTypes.func.isRequired,
  onPress:      PropTypes.func.isRequired,    // On address press
  hintShowed:   PropTypes.bool.isRequired
};

export default AutoSuggest;
