import React, {Component} from 'react';
import {
  View,
  FlatList,
  InteractionManager,
  StyleSheet
} from 'react-native';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';

import {getOrders} from '../../api';

import Hr from '../../components/Hr';
import Order from './Order';

const ListEmptyComponent = (props) => (
  <View>

  </View>
);

class Orders extends Component {
  constructor (props) {
    super(props);
    this.state = {
      isLoading:  false,
      orders:     [],
      limit:      20,
      skip:       0,
      totalCount: 0
    };

    this.getOrders = this.getOrders.bind(this);
    this.renderItem = this.renderItem.bind(this);
    this.onEndReached = this.onEndReached.bind(this);
    this.onRefresh = this.onRefresh.bind(this);
  }

  componentDidMount () {
    InteractionManager.runAfterInteractions(() => {
      this.setState({isLoading: true});

      this.getOrders()
        .then(data => {
          this.setState({
            orders:     data.orders,
            totalCount: data.totalCount,
            isLoading:  false
          });
        })
        .catch((e) => {
          console.log(e);
          this.setState({
            isLoading: false
          });
        });
    });

  }

  /**
   * @return {Object?} options
   * @return {number?} options.skip
   * @return {Promise<{orders: Array, totalCount: number}>} options.limit
   */
  async getOrders (options = {}) {
    const querySkip = typeof options.skip === 'number'
      ? options.skip
      : this.state.skip;
    const queryLimit = options.limit || this.state.limit;

    const queryOptions = [
      `limit=${queryLimit}`,
      `skip=${querySkip}`
    ];
    const data = await getOrders(queryOptions, this.props.auth.token);

    return data;
  }

  renderItem ({item}) {
    // const item = args[0];
    // console.log(args);
    return (
      <Order
        order={item}
        key={item.id}
        onPress={() => this.props.navigation.navigate('Order', {orderId: item.id})}
      />
    );
  }

  onRefresh () {
    const queryOptions = {
      limit: this.state.skip + this.state.limit,
      skip:  0
    };

    this.setState({
      isLoading: true
    });

    this.getOrders(queryOptions)
      .then(data => {
        this.setState({orders: data.orders, totalCount: data.totalCount, isLoading: false});
      })
      .catch((e) => {
        console.log(e);
        this.setState({isLoading: false});
      });
  }

  onEndReached () {
    if (this.state.orders.length === this.state.totalCount) {
      return;
    }

    this.setState({isLoading: true});

    const skip = this.state.skip + this.state.limit;
    const queryOptions = {
      skip
    };

    this.getOrders(queryOptions)
      .then(data => {
        this.setState({
          orders:     this.state.orders.concat(data.orders),
          totalCount: data.totalCount,
          skip,
          isLoading:  false
        });
      })
      .catch((e) => {
        console.log(e);
        this.setState({isLoading: false});
      });
  }

  keyExtractor (order) {
    return order.id;
  }

  render () {
    return (
      <View style={styles.container}>
        <FlatList
          style={{backgroundColor: 'white'}}
          data={this.state.orders}
          renderItem={this.renderItem}
          keyExtractor={this.keyExtractor}
          ItemSeparatorComponent={() => <Hr style={{marginBottom: 0, marginTop: 0}}/>}
          ListEmptyComponent={ListEmptyComponent}
          refreshing={this.state.isLoading}
          onRefresh={this.onRefresh}
          onEndReached={this.onEndReached}
          initialNumToRender={10}
          onEndReachedThreshold={0.5}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});

Orders.propTypes = {
  navigation: PropTypes.object.isRequired,
  auth:       PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  auth: state.auth
});

const mapDispatchToProps = {
};

export default connect(mapStateToProps, mapDispatchToProps)(Orders);
