import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Image
} from 'react-native';
import PropTypes from 'prop-types';

import {color} from '../../styles';

class Order extends Component {
  render () {
    const {order} = this.props;

    return (
      <TouchableOpacity onPress={this.props.onPress} style={styles.container}>
        <View style={styles.head}>
          <View style={{flexDirection: 'row', justifyContent: 'space-between', width: '100%'}}>
            <Text style={styles.id}>№{order.id}</Text>
            <Text
              style={[styles.statusText, {backgroundColor: order.statusText === '' ? 'white' : order.statusColor}]}
            >
              {order.statusText}
            </Text>
          </View>

        </View>

        <View style={styles.datetimes}>
          <View style={[styles.datetime, styles.datetimeLeft]}>
            <Image
              source={require('../../icons/basket.png')}
            />
            <View style={[styles.datetimeContainer]}>
              <Text style={styles.date}>{order.dataFrom.date}</Text>
              <Text style={styles.time}>{order.dataFrom.time}</Text>
            </View>
          </View>
          <View style={[styles.datetime, styles.datetimeRight]}>
            <Image
              source={require('../../icons/package.png')}
            />
            <View style={styles.datetimeContainer}>
              <Text style={styles.date}>{order.dataTo.date}</Text>
              <Text style={styles.time}>{order.dataTo.time}</Text>
            </View>
          </View>
        </View>
        {
          order.goodsCount
          ? <Text style={styles.cost}>
              Стоимость: {order.totalPrice} ₽
            </Text>
          : null
        }
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    padding:      10,
    alignItems:   'flex-start',
    paddingLeft:  15,
    paddingRight: 15
  },
  id: {
    fontSize: 18
  },
  head: {
    flexDirection:  'row',
    justifyContent: 'space-between',
    alignItems:     'center',
    width:          '100%'
  },
  statusText: {
    padding:         5,
    paddingLeft:     30,
    paddingRight:    30,
    textAlign:       'center',
    backgroundColor: 'rgb(247, 168, 218)',
    color:           color.white,
  },
  datetimes: {
    width:          '100%',
    flexDirection:  'row',
    marginTop:      10,
    justifyContent: 'space-between',
    flex:           1
  },
  datetimeRight: {
    // alignSelf: 'flex-end'
    // marginLeft: 'auto'
  },
  datetime: {
    flexDirection: 'row'
  },
  datetimeContainer: {
    justifyContent: 'center',
    marginLeft:     8
  },
  // datetimeContainer__first: {
  //   marginLeft: 16
  // },
  date: {
    fontSize:  17,
    color:     '#414141',
    textAlign: 'center'
    // letterSpacing: -0.41
  },
  time: {
    fontSize:      11,
    color:         '#7E7D7E',
    letterSpacing: -0.27,
    marginTop:     4
  },
  goodsCount: {
    marginTop: 10,
    fontSize:  15,
    color:     '#5C5C5C'
  },
  cost: {
    marginTop: 10,
    fontSize:  17
  },
});

Order.propTypes = {
  order:   PropTypes.object.isRequired,
  onPress: PropTypes.func.isRequired
};

export default Order;
