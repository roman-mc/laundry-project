import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  ScrollView,
  Platform,
  Text,
} from 'react-native';
import PropTypes from 'prop-types';
import Button from 'apsl-react-native-button';
import {connect} from 'react-redux';
import update from 'immutability-helper';
import KeyboardSpacer from 'react-native-keyboard-spacer';

import {sendProfileInfo} from '../../store/actions';

import Input from '../../components/Input';
import {width, color, buttonDefault, button} from '../../styles';

class AddCardOrSkip extends Component {
  constructor (props) {
    super(props);

    this.navigateForward = this.navigateForward.bind(this);
  }

  navigateForward () {
    if (!this.props.auth.name || !this.props.auth.email) {
      this.props.navigation.navigate('ProfileEditOrSkip');
    }
    else {
      this.props.navigation.navigate('OrderInfo');
    }
  }

  render () {
    // TODO: proper navigation
    return (
      <View style={styles.container}>
        <View style={styles.content}>
          <Text style={{fontSize: 23, textAlign: 'center'}}>
            Привяжите карту
          </Text>
          <Text style={{fontSize: 17, textAlign: 'center'}}>
            для удобной оплаты заказов {'\n'}как в такси
          </Text>
          <Text style={{fontSize: 17, textAlign: 'center', marginTop: 30, marginBottom: 40}}>
            Деньги спишутся, только когда Вы примете готовый заказ и дадите согласие на оплату
          </Text>
          <Button
            style={styles.button}
            textStyle={styles.buttonText}
            onPress={() => {
              this.props.navigation.navigate(
                'AddCreditCard',
                {
                  onSuccess: this.navigateForward,
                  onError:   this.navigateForward
                }
              );
            }}
          >
            Я хочу привязать карту
          </Button>
          <Button
            style={[
              {
                // width:         width.bigContainer,
                alignSelf:     'center',
                alignItems:    'center',
                // padding:      10,
                // borderRadius: 5,
                borderWidth:   0,
                paddingLeft:   0,
                paddingRight:  0,
                paddingTop:    0,
                paddingBottom: 0,
                width:         'auto',
                height:        'auto'
              },
            ]}
            onPress={this.navigateForward}
            textStyle={[
              {
                color:              color.black,
                textDecorationLine: 'underline'
              },
            ]}
          >
            Нет, я привяжу карту позже
          </Button>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex:            1,
    alignItems:      'center',
    backgroundColor: '#ffffff'
    // justifyContent: 'flex-end',
  },
  content: {
    marginTop: '20%',
    // marginBottom: '64%',
    width:     width.smallContainer
  },
  title: {
    fontSize:  23,
    marginTop: 10
  },
  input__first: {
    // marginTop: 5
  },
  button: {
    width:        width.bigContainer,
    alignSelf:    'center',
    borderWidth:  0,
    padding:      10,
    borderRadius: 10,
    ...button.container
  },
  buttonText: {
    ...button.text
  },
  inputError: {
    borderBottomColor: color.error
  },
  bottomContent: {
    justifyContent: 'flex-end',
    alignItems:     'flex-start',
    flex:           1,
    width:          '100%'
  },
  buttonDefaultContainer: {
    backgroundColor: '#fff',
    borderRadius:    10,
    ...buttonDefault.container
  },
  buttonDefaultText: {
    ...buttonDefault.text
  }
});

// TODO: propTypes
AddCardOrSkip.propTypes = {
  auth:            PropTypes.object.isRequired,
  navigation:      PropTypes.object.isRequired,
  sendProfileInfo: PropTypes.func.isRequired,
};

AddCardOrSkip.defaultProps = {
  image: '../../icons/delivery_icon.png'
};

const mapStateToProps = (state, ownProps) => ({
  auth:       state.auth,
  navigation: ownProps.navigation
});

const mapDispatchToProps = {
  sendProfileInfo
};

export default connect(mapStateToProps, mapDispatchToProps)(AddCardOrSkip);
