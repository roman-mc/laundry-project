import React, {Component} from 'react';
import {
  View,
  WebView,
  ActivityIndicator,
  Platform
} from 'react-native';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
const Base64 = require('Base64');


import {getCards} from '../../store/actions';
import {
  getBindingCardUrl
} from '../../api';

class AddCreditCard extends Component {
  constructor (props) {
    super(props);

    this.state = {
      bindingCardUrl: null
    };

    this.onMessage = this.onMessage.bind(this);

  }

  componentDidMount () {
    // TODO: get url
    getBindingCardUrl(this.props.auth.token)
      .then(url => {
        this.setState({bindingCardUrl: url});
      })
      .catch(() => {
        this.onerror();
      });
  }

  /**
   * @param  {string} data string decoded in base64
   */
  onMessage (event) {
    // Handle success/error
    const data = event.nativeEvent.data;

    try {
      const result = Base64.atob(data);

      const resultObject = JSON.parse(result);

      if (resultObject.success) {
        this.onSuccess();
      }
      else {
        console.log(resultObject);
        this.onError();
      }
    }
    catch (e) {
      console.error(e);
      this.onError();
    }
  }

  onSuccess () {
    this.props.navigation.goBack();
    this.props.navigation.state.params.onSuccess && this.props.navigation.state.params.onSuccess();
  }

  onError () {
    this.props.navigation.goBack();
    this.props.navigation.state.params.onError && this.props.navigation.state.params.onError();
  }

  render () {
    if (this.state.bindingCardUrl === null) {
      return (
        <ActivityIndicator style={{flex: 1, justifyContent: 'center', alignItems: 'center'}} />
      );
    }

    return (
      <View style={{flex: 1}}>
        <WebView
          onMessage={this.onMessage}
          source={{uri: this.state.bindingCardUrl}}
          scalesPageToFit={Platform.OS === 'ios'} // for ios - true, for android - false
        />
      </View>
    );
  }
}

AddCreditCard.propTypes = {
  auth:       PropTypes.object.isRequired,
  navigation: PropTypes.object.isRequired,
  getCards:   PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth
});

const mapDispatchToProps = {
  getCards
};

export default connect(mapStateToProps, mapDispatchToProps)(AddCreditCard);
