import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  StyleSheet,
  Alert
} from 'react-native';
import PropTypes from 'prop-types';

import {width} from '../../styles';

class CheckboxRow extends Component {
  constructor (props) {
    super(props);

    this.showAlertInfo = this.showAlertInfo.bind(this);
  }

  showAlertInfo () {
    Alert.alert(
      '',
      this.props.notAvailableText,
      [
        {text: 'Ок', onPress: () => undefined}
      ]
    );
  }

  render () {
    return (
      <View
        style={{
          flexDirection:  'row',
          justifyContent: 'flex-end',
          alignItems:     'center',
          width:          width.bigContainer,
          marginBottom:   10,
        }}
      >
        <TouchableOpacity
          style={{
            flexDirection:  'row',
            justifyContent: 'flex-end',
            alignItems:     'center',
            opacity:        this.props.available ? 1 : 0.5
          }}
          onPress={() => {
            this.props.onChange(!this.props.checked);
          }}
          disabled={!this.props.available}
        >
          {/* <View style={styles.container}> */}
            {/* <View style={styles.mainView}> */}
              {/* Start: Tooltip */}
              {/* End: Tooltip */}
              {/* <View style={styles.anotherView} /> */}
              {/* The view with app content behind the tooltip */}
            {/* </View> */}
          {/* </View> */}
          <Image
            source={this.props.checked
              ? require('../../icons/checkto.png')
              : require('../../icons/check.png')
            }
            // style={{height: 20, width: 20, backgroundColor: 'rgb(101, 101, 101)', marginRight: 20}}
          />
          <Text style={{fontSize: 14, marginLeft: 15, marginRight: 15, width: '70%'}}>
            {this.props.text}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={this.showAlertInfo}
        >

          <Image
            source={require('../../icons/question.png')}
            style={{
              opacity: this.props.available ? 0 : 1
            }}
            // style={{height: 20, width: 20, backgroundColor: 'rgb(101, 101, 101)', marginRight: 20}}
          />

        </TouchableOpacity>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex:            1,
    justifyContent:  'center',
    alignItems:      'center',
    backgroundColor: '#F5FCFF',
    position:        'relative'
  },
  mainView: {
    flex:          1,
    flexDirection: 'row',
  },
  talkBubble: {
    backgroundColor: 'transparent',
    position:        'absolute',
    opacity:         1,
    zIndex:          2, // <- zIndex here
    // flex:            1,
    right:           0,
    //left: (Dimensions.get('window').width / 2) - 300, // Uncomment this line when test in device.
    top:             -130,
  },
  talkBubbleSquare: {
    width:                   300,
    height:                  120,
    backgroundColor:         '#2C325D',
    borderRadius:            10,
    borderBottomRightRadius: 0
  },
  talkBubbleTriangle: {
    position:          'absolute',
    // bottom:           -20,
    bottom:            -10,
    right:             0,
    width:             0,
    height:            0,
    borderLeftWidth:   10,
    borderRightWidth:  10,
    borderTopWidth:    10,
    borderLeftColor:   'transparent',
    borderRightColor:  'transparent',
    borderBottomColor: '#2C325D',
  },
  talkBubbleMessage: {
    color:       '#FFFFFF',
    marginTop:   40,
    marginLeft:  20,
    marginRight: 20,
  },
  anotherView: {
    backgroundColor: '#CCCCCC',
    width:           340,
    height:          300,
    position:        'absolute',
    zIndex:          1, // <- zIndex here
  },
});

CheckboxRow.propTypes = {
  checked:          PropTypes.bool,
  available:        PropTypes.bool,
  notAvailableText: PropTypes.string,
  onChange:         PropTypes.func,
  text:             PropTypes.string
};

export default CheckboxRow;
