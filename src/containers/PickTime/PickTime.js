// TODO: refactor most of these chunks put in components
import React, {Component} from 'react';
import {
    View,
    Text,
    StyleSheet,
    KeyboardAvoidingView,
    Keyboard,
    TouchableOpacity,
    Modal,
    ScrollView,
    Platform
} from 'react-native';
import PropTypes from 'prop-types';
import Button from 'apsl-react-native-button';
import Octicons from 'react-native-vector-icons/Octicons';
import {connect} from 'react-redux';
import update from 'immutability-helper';

import {
  selectTime,
  selectDate,
  changePromocode,
  change,
  sendOrder,
  selectOption
} from '../../store/actions';
import {getPickedDay, getPickedTime, toLocaleString2 as toLocaleString} from '../../utils';

import {width, color, button, keyboardVerticalOffset} from '../../styles';
import CheckboxRow from './CheckboxRow';
import Header from '../../components/Header';
// import DateTimePickerButtons from '../../components/DateTimePickerButtons';
import TimePicker from '../../components/TimePicker';
import DatePicker from '../../components/DatePicker';
import Input from '../../components/Input';
import InputArea from '../../components/InputArea';

const urgentCollectDay = date => {
  const collectDate = date.getDate();
  const nowDate = new Date().getDate();

  if (collectDate > nowDate || collectDate < nowDate) {
    return 'завтра';
  }

  return 'сегодня';

};

/**
 * @param  {string} time '10:00 - 12:00'
 * @return {string}      12:00
 */
const timeUntil = time => /\d\d:\d\d$/.exec(time)[0];

class PickTime extends Component {
  constructor (props) {
    super(props);


    this.state = {
      scrollEnabled: false,
      modal:         {
        isVisible: false,
        component: null,
        onClose:   null,
        onPress:   null,
        title:     null,
      },
    };

    this.layout = {
      topContent:   null,
      bottomButton: null
    };

    this.toggleModal = this.toggleModal.bind(this);
    this.onKeyboardOpen = this.onKeyboardOpen.bind(this);
    // Modal picker handlers
    // TODO
    this.openModalDateFrom = this.openModalDate.bind(this, 'from');
    this.openModalDateTo = this.openModalDate.bind(this, 'to');
    this.openModalTimeFrom = this.openModalTime.bind(this, 'from');
    this.openModalTimeTo = this.openModalTime.bind(this, 'to');

    this.onChangeComment = this.onChangeComment.bind(this);
  }

  componentWillMount () {
    this.keyboardWillShowListener = Keyboard.addListener('keyboardDidShow', this.onKeyboardOpen);
  }

  componentWillUnmount () {
    this.keyboardWillShowListener.remove();
  }

  componentDidUpdate (prevProps) {
    // navigate on send order
    if (prevProps.isLoading && !this.props.isLoading
      && prevProps.order.orderID === null && this.props.order.orderID !== null) {
      if (this.props.cards.cards.length === 0) {
        this.props.navigation.navigate('AddCardOrSkip');
      }
      else if (!this.props.auth.name || !this.props.auth.email) {
        this.props.navigation.navigate('ProfileEditOrSkip');
      }
      else {
        this.props.navigation.navigate('OrderInfo');
      }
    }
  }

  onKeyboardOpen () {
    setTimeout(() => this.scrollView.scrollToEnd({animated: true}), 50);
  }
  // componentWillMount () {
  //   this.props.getAbleTimeslots();
  // }
  onChangeComment (text) {
    this.props.change('comment', text);
  }


  onInputChange (prop, value) {
    this.setState({[prop]: value});
  }

  openModalDate (property) {
    Keyboard.dismiss();
    this.setState({
      modal: {
        isVisible: true,
        component: DatePicker,
        data:      property === 'from' ? this.props.order.ableTimeslots.collect : this.props.order.ableTimeslots.delivery,
        title:     'Выберите дату',
        onClose:   this.toggleModal,
        onPress:   value => this.onSelectDate(property, value)
      }
    });
  }

  openModalTime (property) {
    Keyboard.dismiss();
    this.setState({
      modal: {
        isVisible: true,
        component: TimePicker,
        data:      property === 'from' ? this.props.order.ableTimeslots.collect : this.props.order.ableTimeslots.delivery,
        title:     'Выберите время',
        onClose:   this.toggleModal,
        onPress:   value => this.onSelectTime(property, value)
      }
    });
  }

  onSelectTime (property, value) {
    // TODO: close modal
    this.toggleModal();
    this.props.selectTime(property, value);
  }

  onSelectDate (property, value) {
    // TODO: close modal
    this.toggleModal();
    this.props.selectDate(property, value);
  }

  toggleModal () {
    this.setState({
      modal: update(this.state.modal, {isVisible: {$set: !this.state.modal.isVisible}})
    });
  }

  render () {
    const isOrderValid = false;

    const ModalComponent = this.state.modal.component;
    const {order, isLoading} = this.props;
    const {ableTimeslots, promocode} = order;

    if (ableTimeslots === null) return null;

    const collectDay = getPickedDay(ableTimeslots.collect);
    const deliveryDay = getPickedDay(ableTimeslots.delivery);
    const collectDate = collectDay.date;
    const deliveryDate = deliveryDay.date;
    const collectTime = getPickedTime(collectDay);
    const deliveryTime = getPickedTime(deliveryDay);

    return (
      <KeyboardAvoidingView
        keyboardVerticalOffset={keyboardVerticalOffset} behavior={Platform.OS === 'ios' ? 'padding': undefined} style={{flex: 1}}
      >

      <ScrollView

        contentContainerStyle={[
          styles.container,
          // {flex: 1}
        ]}
        style={{
          backgroundColor: 'white'
        }}
        // scrollEnabled
        // scrollEnabled={this.state.scrollEnabled}
        ref={ref => this.scrollView = ref}
        keyboardShouldPersistTaps='handled'
      >
          <View style={{width: '100%', alignItems: 'center'}}
          >
            <View style={styles.questionTextContainer}>
              <Text style={[styles.questionText]}>
                Когда забрать вещи
              </Text>
            </View>
            <CheckboxRow
              text='Забрать все вещи в течение часа (+490₽)'
              onChange={(checked) => {
                this.props.selectOption('urgentCollect', checked);
              }}
              checked={ableTimeslots.urgentCollect.checked}
              available={ableTimeslots.urgentCollect.available}
              notAvailableText={
                ableTimeslots.urgentCollect.message
              }
              isLoading={isLoading}
            />
            {
              ableTimeslots.urgentCollect.checked === true
                ? <Text
                    style={{
                      fontSize:          17,
                      marginTop:         10,
                      marginBottom:      10,
                      width:             width.bigContainer,
                      paddingHorizontal: 20,
                      height:            88,
                      paddingTop:        20,
                      textAlign:         'center'
                    }}
                  >
                    Курьер заберет вещи {urgentCollectDay(collectDate)} {'\n'}
                    до {timeUntil(collectTime)}
                  </Text>
                : <View>
                    <Button style={styles.button} onPress={this.openModalDateFrom}>
                      <Text>{toLocaleString(collectDate)}</Text>
                    </Button>
                    <Button style={[styles.button, styles.bottomButton]} onPress={this.openModalTimeFrom}>
                      <Text>{collectTime}</Text>
                    </Button>
                  </View>
            }

            {/* Question time and date END */}

            {/* Question time and date START */}
            <View style={styles.questionTextContainer}>
              <Text style={[styles.questionText, {marginTop: 0}]}>
                Когда привезти вещи
              </Text>
            </View>
            <CheckboxRow
              text='Экспресс-чистка за 24 часа (+100% к стоимости)'
              onChange={(checked) => {
                this.props.selectOption('urgentDelivery', checked);
              }}
              checked={ableTimeslots.urgentDelivery.checked}
              available={ableTimeslots.urgentDelivery.available}
              notAvailableText={ableTimeslots.urgentDelivery.message}
              isLoading={isLoading}
            />
            <View>
              <Button
                style={styles.button}
                onPress={this.openModalDateTo}
                isDisabled={ableTimeslots.urgentDelivery.checked}
              >
                <Text>{toLocaleString(deliveryDate)}</Text>
              </Button>
              <Button style={[styles.button, styles.bottomButton]} onPress={this.openModalTimeTo}>
                <Text>{deliveryTime}</Text>
              </Button>
            </View>

            <View
              style={{width: width.bigContainer}}
              // onLayout={({nativeEvent}) => console.log(nativeEvent)}
            >
              <Input
                placeholder='Промокод'
                onChangeText={this.props.changePromocode}
                style={[styles.promocodeInput]}
                value={promocode.value}
              />
              <Text style={{
                marginTop: 10,
                color:     promocode.isValid ? color.airo :
                  !promocode.isLoading && promocode.value.length > 0 && !promocode.isValid ? color.error
                  : color.black
              }}>
                {
                  promocode.isValid ? `Скидка по промокоду: ${promocode.discount}` :
                  promocode.isLoading ? `Проверка промокода...` :
                  promocode.value.length > 0 && !promocode.isValid ? 'Промокод не действителен' :
                  'Скидка по промокоду: 0'
                }
              </Text>
              <InputArea
                placeholder='Комментарии к заказу'
                style={styles.inputArea}
                multiline
                onChangeText={this.onChangeComment}
                blurOnSubmit
                value={order.comment}
                autoGrow
                maxHeight={116}
                numberOfLines={6}
              />

            </View>
          </View>

          <Button
            style={[styles.button, styles.buttonSubmit]}
            onPress={this.props.sendOrder}
            isDisabled={isOrderValid}
            isLoading={isLoading}
            textStyle={styles.buttonSubmitText}
          >
            Подтвердить заказ
          </Button>
          {/* Question time and date END */}
        <Modal
          visible={this.state.modal.isVisible}
          onRequestClose={() =>
            this.setState({
              modal: update(this.state.modal, {isVisible: {$set: !this.state.modal.isVisible}})
            })
          }
          animationType='slide'
        >
          <Header
            left={
              <TouchableOpacity onPress={this.state.modal.onClose} style={{padding: 15}}>
                <Octicons name='x' size={20} color='#00adf5' />
              </TouchableOpacity>
            }
            title={this.state.modal.title}
          />
          {/* TODO: remove checking isVisible */}
          {
            this.state.modal.isVisible
              ? <ModalComponent onPress={this.state.modal.onPress} data={this.state.modal.data} />
              : null
          }

        </Modal>
      </ScrollView>
    </KeyboardAvoidingView>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    // flex:           1,
    alignItems:     'center',
    justifyContent: 'space-between',
    width:          width.full
  },
  button: {
    width:        width.bigContainer,
    alignSelf:    'center',
    alignItems:   'center',
    padding:      10,
    borderRadius: 5,
    borderWidth:  1,
    borderColor:  '#0c0d0e',
    marginTop:    20
    // marginBottom: 10
  },
  buttonSubmit: {
    backgroundColor: color.gold,
    borderWidth:     0
  },
  buttonSubmitText: {
    color: color.black
  },
  bottomButton: {
    // marginTop: 10
  },
  questionTextContainer: {
    alignSelf:  'flex-start',
    alignItems: 'center',
    width:      '100%'
  },
  questionText: {
    marginTop:    10,
    marginBottom: 10,
    fontSize:     18,
    alignSelf:    'center',
    width:        width.bigContainer
  },
  promocodeInput: {
    fontSize:     15,
    marginLeft:   0,
    marginRight:  0,
    marginBottom: 0,
    width:        '100%',
  },
  inputArea: {
    fontSize:  14,
    minHeight: 72,
    flexWrap:  'wrap',
    marginTop: 10,
    // marginBottom: 10,
    width:     width.bigContainer
  },
});

PickTime.propTypes = {
  isLoading:       PropTypes.bool.isRequired,
  navigation:      PropTypes.object.isRequired,
  order:           PropTypes.object.isRequired,
  selectTime:      PropTypes.func.isRequired,
  selectDate:      PropTypes.func.isRequired,
  selectOption:    PropTypes.func.isRequired,
  changePromocode: PropTypes.func.isRequired,
  change:          PropTypes.func.isRequired,
  sendOrder:       PropTypes.func.isRequired,
  auth:            PropTypes.object.isRequired,
  cards:           PropTypes.object.isRequired
};

const mapStateToProps = (state, ownProps) => ({
  isLoading:  state.order.isLoading,
  navigation: ownProps.navigation,
  order:      state.order,
  auth:       state.auth,
  cards:      state.cards
});

const mapDispatchToProps = {
  selectTime,
  selectDate,
  changePromocode,
  change,
  sendOrder,
  selectOption
};

export default connect(mapStateToProps, mapDispatchToProps)(PickTime);


// bla bla bla
