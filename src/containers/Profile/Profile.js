import React, {Component} from 'react';
import {
  View,
  FlatList,
  Text,
  TouchableOpacity,
  StyleSheet,
  Image,
  Switch,
  Alert
} from 'react-native';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
// import Button from 'apsl-react-native-button';

import {color, width} from '../../styles';

import Hr from '../../components/Hr';
import Tabs from '../../components/Tabs';
import Button from '../../components/Button';

const formatCardPan = string => {
  let formattedPan = '';

  formattedPan += string.slice(0, 4);
  formattedPan += ' ';
  formattedPan += string.slice(4, 8);
  formattedPan += ' ';
  formattedPan += string.slice(8, 12);
  formattedPan += ' ';
  formattedPan += string.slice(12);

  return formattedPan;
};

class Profile extends Component {
  constructor (props) {
    super(props);
  }

  render () {
    const name = this.props.auth.name || 'Ваше имя';
    const email = this.props.auth.email || 'Ваш email';


    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <View style={styles.profileInfo}>
            <Text style={styles.profileName}>{name}</Text>
            <Text style={styles.profileEmail}>{email}</Text>
          </View>
          <TouchableOpacity
            style={styles.profileSideButton}
            onPress={() => this.props.navigation.navigate('ProfileEditing')}
            hitSlop={{left: 10, top: 10, right: 10, bottom: 10}}
          >
            <Image source={require('../../icons/edit_icon.png')} />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex:            1,
    backgroundColor: 'white'
  },
  addressContainer: {
    backgroundColor:   color.white,
    borderBottomWidth: 1,
    borderColor:       '#d5d5d5',
    flexDirection:     'row',
    paddingLeft:       20,
    paddingVertical:   10,
    paddingRight:      10,
    justifyContent:    'space-between',
    alignItems:        'center'
  },
  list: {
    marginTop: 20
  },
  listItemContainer: {
    backgroundColor:   color.white,
    borderBottomWidth: 1,
    borderColor:       '#d5d5d5',
    flexDirection:     'row',
    paddingLeft:       20,
    paddingVertical:   10,
    paddingRight:      10,
    justifyContent:    'space-between',
    alignItems:        'center'
  },
  cardRightContent: {
    flexDirection:  'row',
    alignItems:     'center',
    justifyContent: 'flex-end'
  },
  header: {
    flexDirection:   'row',
    justifyContent:  'space-between',
    alignItems:      'center',
    backgroundColor: color.white,
    paddingTop:      20,
    paddingBottom:   20,
    paddingLeft:     15,
    paddingRight:    15,
  },
  profileName: {
    fontSize: 19,
    color:    '#222022'
  },
  profileEmail: {
    fontSize:  17,
    color:     '#5C5C5C',
    marginTop: 10
  },
  buttonAdd: {
    width:           width.full - 30,
    marginLeft:      15,
    marginRight:     15,
    marginTop:       20,
    marginBottom:    0,
    height:          'auto',
    paddingVertical: 10
  },
  buttonAddText: {
    fontSize: 15,
    color:    color.white,
  }
});

Profile.propTypes = {
  navigation: PropTypes.object.isRequired,
  auth:       PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  addresses:          state.addresses.addresses,
  isLoadingAddresses: state.addresses.isLoading,
  cards:              state.cards.cards,
  isLoadingCards:     state.cards.isLoading,
  auth:               state.auth
});

export default connect(mapStateToProps)(Profile);
