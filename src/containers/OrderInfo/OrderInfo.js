import React, {Component} from 'react';
import {
    View,
    Text,
    StyleSheet,
    ScrollView
} from 'react-native';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {NavigationActions} from 'react-navigation';

import {toLocaleString, getPickedDay, getPickedTime, getShortDateTime} from '../../utils';

import {resetNavigation, resetOrder} from '../../store/actions';
import Button from '../../components/Button';
import Input from '../../components/Input';
import Hr from '../../components/Hr';
import {width, color} from '../../styles';

/**
 * @param  {Object} data order.dataFrom or order.dataTo
 * @return {string}
 */
const getFullAddress = data => {
   /**
    * @type {Array<string|boolean>}
    */
  const addressDetails = [];

  addressDetails.push(data.kvartira && 'кв/офис ' + data.kvartira);
  addressDetails.push(data.podezd && 'подъезд ' + data.podezd);
  addressDetails.push(data.etaj && 'этаж ' + data.etaj);
  addressDetails.push(data.domofon && 'домофон ' + data.domofon);

  return addressDetails
     .filter(s => Boolean(s))
     .join(', ');
};

class OrderInfo extends Component {
  constructor (props) {
    super(props);

    this.state = {
    };

    this.onSubmit = this.onSubmit.bind(this);
  }

  onSubmit () {
    this.props.resetNavigation('PickAddress'); // TODO: handle action in router reducer
    this.props.resetOrder();

    const navigationToOrder = NavigationActions.navigate({
      routeName: 'OrdersStack',

      params: {},
      action: NavigationActions.navigate(
        {routeName: 'Order', params: {orderId: this.props.order.orderID}}
      ),
    });

    this.props.navigation.dispatch(navigationToOrder);
    // this.props.navigation.navigate('OrdersStack');

  }

  render () {
    const {order, auth} = this.props;
    const collectDay = getPickedDay(order.ableTimeslots.collect);
    const deliveryDay = getPickedDay(order.ableTimeslots.delivery);
    const collectTime = getPickedTime(collectDay);
    const deliveryTime = getPickedTime(deliveryDay);

    const addressFromDetails = getFullAddress(order.dataFrom);
    const addressToDetails = getFullAddress(order.dataTo);


    return (
      <View style={styles.container}>
        <View style={{
          width:      width.bigContainer,
          marginTop:  15,
          alignItems: 'flex-start',
          flex:       1
        }}>
          <Text style={styles.titleText}>
            Спасибо! Ваш заказ принят {'\n'}
            Вам придет СМС с подтверждением
          </Text>
          <Hr />
          <ScrollView>
            <Text style={styles.titleText}>
              Детали вашего заказа:
            </Text>

            <Text style={styles.orderNumberText}>
              № заказа: {order.orderID}
            </Text>
            <Text style={styles.infoText}>
              Телефон: {auth.phone}
            </Text>
            {auth.name
              ? <Text style={styles.infoText}>
                  Имя: {auth.name}
                </Text>
              : null}
            {auth.email
              ? <Text style={styles.infoText}>
                  E-mail: {auth.email}
                </Text>
              : null
            }

            <Text style={[styles.infoText, styles.marginedText]}>
              Когда забрать: {getShortDateTime(collectDay.date, collectTime)}
            </Text>
            <Text style={[styles.infoText]}>
              Откуда забрать: {order.dataFrom.address}
              {'\n'}
              {addressFromDetails}
            </Text>
            {
              order.dataFrom.addressComment !== ''
                ? <Text>
                    Комментарий к адресу: {order.dataFrom.addressComment}
                  </Text>

                : null
            }
            <Text style={[styles.infoText, styles.marginedText]}>
              Когда привезти: {getShortDateTime(deliveryDay.date, deliveryTime)}
            </Text>

            <Text style={[styles.infoText]}>
              Куда привезти: {order.sameDeliveryAddress ? 'На тот же адрес' : (order.dataTo.address + '\n' + addressToDetails)}
            </Text>
            {
              !order.sameDeliveryAddress && order.dataTo.addressComment !== ''
                ? <Text>
                    Комментарий к адресу: {order.dataTo.addressComment}
                  </Text>
                : null
            }

            {order.promocode.isValid
              ? (<Text style={[styles.infoText, styles.marginedText]}>
                  Промокод: {order.promocode.value}
                </Text>
              )
              : null}
            {order.promocode.isValid
              ? <Text style={[styles.infoText]}>
                  Скидка: {order.promocode.discount}
                </Text>
              : null
            }

            {order.comment.length > 0
              ? <Text style={[styles.infoText, styles.marginedText]}>
                  Комментарий: {order.comment}
                </Text>
              : null}
          </ScrollView>
        </View>

        <View>
          <Button
            style={{marginTop: 15}}
            onPress={this.onSubmit}
          >
            Ок
          </Button>
        </View>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex:           1,
    alignItems:     'center',
    justifyContent: 'space-between'
  },
  input: {
    paddingBottom: 5
  },
  titleText: {
    fontSize: 18
  },
  orderNumberText: {
    marginTop:    15,
    marginBottom: 15,
    fontSize:     16,
    fontWeight:   'bold'
  },
  infoText: {

  },
  marginedText: {
    marginTop: 15
  }
});

// TODO: propTypes
OrderInfo.propTypes = {
  resetNavigation: PropTypes.func.isRequired,
  resetOrder:      PropTypes.func.isRequired,
  order:           PropTypes.object.isRequired,
  auth:            PropTypes.object.isRequired,
  navigation:      PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  order: state.order,
  auth:  state.auth
});

const mapDispatchToProps = {
  resetNavigation,
  resetOrder
};

export default connect(mapStateToProps, mapDispatchToProps)(OrderInfo);
