import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  Image,
  Text
} from 'react-native';

import {color} from '../../styles';

import Button from '../../components/Button';
import Input from '../../components/Input';

class CreditCardAdd extends Component {
  // TODO: onChangeText
  render () {
    return (
      <View style={styles.container}>
        <View style={styles.topContent}>
          <View
            style={[styles.row, styles.cardNumberContainer, {
              borderBottomWidth: StyleSheet.hairlineWidth,
              borderBottomColor: '#D8D8D8',
              marginBottom:      10
            }]}
          >
            <Input
              placeholder='0000 0000 0000 0000'
              onChangeText={() => void 2}
              style={[styles.input, {textAlign: 'left', width: 'auto', flexBasis: '80%', borderBottomWidth: 0, marginBottom: 0}]}
            />
            <View style={{width: 50, height: 30, backgroundColor: 'rgb(93, 187, 227)'}}></View>
          </View>
          <View style={[styles.row, {
            flexDirection:  'row',
            justifyContent: 'space-between'
          }]}>
            <Input
              placeholder='Expiration date'
              style={[styles.input,{width: 'auto', flexBasis: '45%', marginLeft: 0}]}
            />
            <Input
              placeholder='CVS/CVV'
              style={[styles.input,{width: 'auto', flexBasis: '45%'}]}
            />
          </View>
          <View style={styles.row}>
            <Input
              style={styles.input}
              placeholder='Card holder'
            />
          </View>
        </View>

        <Button>Добавить карту</Button>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex:           1,
    justifyContent: 'space-between'
  },
  topContent: {

  },
  cardNumberContainer: {
    flexDirection: 'row',
    alignItems:    'center'
  },
  input: {
    marginLeft:        0,
    marginRight:       0,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: '#D8D8D8',
    fontSize:          16,
    textAlign:         'left'
  },
  inputError: {
    borderBottomColor: color.error
  }
});

export default CreditCardAdd;
