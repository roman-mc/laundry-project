import React, {Component} from 'react';
import {
  View,
  ScrollView,
  StyleSheet,
  RefreshControl,
  Image
} from 'react-native';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';

import Text from '../../components/Text';

// #38a4f4
import {getOrder} from '../../api';
import {
  months, shortWeekDays
} from '../../utils';
import {width,color} from '../../styles';

const Hr = props => <View style={[styles.hr, props.style]} />;

/**
 * @param  {Object} data object that has .kvartira, .podezd, .etaj, .domofon
 * @return {string}
 */
const getFullAddress = data => {
   /**
    * @type {Array<string|boolean>}
    */
  const addressDetails = [];

  addressDetails.push(data.kvartira && 'кв/офис ' + data.kvartira);
  addressDetails.push(data.podezd && 'подъезд ' + data.podezd);
  addressDetails.push(data.etaj && 'этаж ' + data.etaj);
  addressDetails.push(data.domofon && 'домофон ' + data.domofon);

  return addressDetails
     .filter(s => Boolean(s))
     .join(', ');
};

class Order extends Component {
  constructor (props) {
    super(props);

    this.state = {
      order:     null,
      isLoading: false
    };

    this.getOrder = this.getOrder.bind(this);
  }

  componentDidMount () {
    this.getOrder();
  }

  getOrder () {
    this.setState({isLoading: true});

    getOrder(this.props.navigation.state.params.orderId, this.props.auth.token)
      .then(order => {
        this.setState({order, isLoading: false});
      })
      .catch(e => {
        this.setState({isLoading: false});
      });
  }

  renderCommentSection () {
    if (!this.state.order.comment) {
      return null;
    }

    return (
      <View style={{width: '100%'}}>

        <Text style={styles.title}>Комментарий к заказу</Text>
        <Text style={styles.textInfo}>{this.state.order.comment}</Text>

        <Hr />
      </View>
    );
  }

  renderGoodsSection () {
    if (!this.state.order.goodsCount) {
      return null;
    }

    return (
      <View style={{width: '100%'}}>

        <Text style={[styles.headTitle, styles.title]}>
          Вещи
        </Text>
        <View style={[styles.textInfoContainer, {flexDirection: 'row', alignItems: 'center'}]}>
          <Text style={{flexBasis: '45%', fontWeight: 'bold'}} numberOfLines={2}>Название</Text>
          {/* <View> */}
            <Text style={{textAlign: 'right', flexBasis: '20%', fontWeight: 'bold'}} numberOfLines={1}>Кол-во</Text>
            <Text style={{textAlign: 'right', flexBasis: '15%', fontWeight: 'bold'}} numberOfLines={1}>Цена</Text>
            <Text style={{textAlign: 'right', flexBasis: '20%', fontWeight: 'bold'}} numberOfLines={1}>Сумма</Text>
          {/* </View> */}
        </View>
        {
          this.state.order.goods.map(good => (
              <View style={[styles.textInfoContainer, {flexDirection: 'row', alignItems: 'center'}]} key={good.id}>
                <Text style={{flexBasis: '45%'}} numberOfLines={2}>{good.name}</Text>
                {/* <View> */}
                  <Text style={{textAlign: 'right', flexBasis: '20%'}} numberOfLines={1}>{Number.parseFloat(good.quantity.toFixed(2))} {good.unit}</Text>
                  {
                    good.price
                    ? [
                      <Text key={'good_price'} style={{textAlign: 'right', flexBasis: '15%'}} numberOfLines={1}>{`${Math.round(good.price)} ₽`}</Text>,
                      <Text key={'good_total_price'} style={{textAlign: 'right', flexBasis: '20%', fontFamily: 'FSElliotPro'}} numberOfLines={1}>{`${Math.round(good.price * good.quantity)} ₽`}</Text>
                    ]
                    : null
                  }
                {/* </View> */}
              </View>
            ))
        }
        <Hr />

      </View>
    );
  }

  renderPromocodeSection () {
    if (!this.state.order.promocode) {
      return null;
    }

    return (
      <View>
        {
          this.state.order.promocode
          ? <Text style={styles.title}>
              Промокод: {this.state.order.promocode}
            </Text>
          : null

        }
        <Hr />
      </View>
    );
  }

  renderDeliveryPriceSection () {

    // if (this.state.order.deliveryPrice === 0) {
    if (this.state.order.deliveryPrice === 0 || this.state.order.goodsCount === 0) {
      return null;
    }

    return (
      <View style={{width: '100%'}}>

        <Text style={styles.title}>Стоимость доставки: {this.state.order.deliveryPrice} ₽</Text>

        <Hr />
      </View>

    );
  }

  /**
   * @param  {string} title
   * @param  {Object} data
   * @return {Object} React component
   */
  renderDeliveryInfo (title, data) {
    const addressDetails = getFullAddress(data);
    return (
      <View style={{width: '100%'}}>
        <View style={styles.deliveryHead}>
          <Text style={[styles.title, styles.headTitle]}>{title}</Text>
          <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 8}}>
            <Image source={require('../../icons/small_calendar.png')} />
            <Text style={[styles.date, {marginLeft: 8}]}>{data.date}</Text>
          </View>
          <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 8}}>
            <Image source={require('../../icons/small_watch.png')} />
            <Text style={[styles.time, {marginLeft: 8}]}>{data.time}</Text>
          </View>
        </View>
        <Text style={styles.address}>{data.address}</Text>
        {
          addressDetails !== ''
          ? <Text style={styles.address}>{getFullAddress(data)}</Text>
          : null
        }
        {
          data.addressComment !== ''
          ? <Text style={styles.addressComment}>{data.addressComment}</Text>
          : null
        }

        <Hr />
      </View>
    );
  }

  /**
   * renders info about delivery price, promocode, discount
   * @return {Object|null} React element
   */
  renderConclusionSection () {
    /**
     * @type {string|null}
     */
    const promocode = this.state.order.promocode;
    /**
     * @type {number|null}
     */
    const discount = this.state.order.discount;
    /**
     * @type {number|null}
     */
    const deliveryPrice = this.state.order.deliveryPrice;

    if (!promocode && !discount && !deliveryPrice) {
      return null;
    }

    return (
      <View
        style={{
          width: '100%',
        }}
      >
        <View
          style={{
            width:          '100%',
            justifyContent: 'flex-end',
            flexDirection:  'row',
            marginTop:      15
          }}
        >
          <View
            style={{
              flexBasis: '28%'
            }}
          >
            {
              promocode
              ? <Text
                  style={styles.conclusionText}
                >
                Промокод:
              </Text>
              : null
            }

            {
              discount
              ? <Text
                  style={styles.conclusionText}
                >
                  Скидка:
                </Text>
              : null
            }
            {
              deliveryPrice
              ? <Text
                  style={styles.conclusionText}
                >
                  Доставка:
                </Text>
              : null
            }
          </View>
          <View
            style={{
              flexBasis:  '20%',
              alignItems: 'flex-end'
            }}
          >
            {
              promocode
              ? <Text
                  style={styles.conclusionText}
                >
                {promocode}
              </Text>
              : null
            }

            {
              discount
              ? <Text
                  style={styles.conclusionText}
                >
                  {discount} ₽
                </Text>
              : null
            }
            {
              deliveryPrice
              ? <Text
                  style={styles.conclusionText}
                >
                  {deliveryPrice} ₽
                </Text>
              : null
            }

          </View>
        </View>
        {/* For rendering price
            Price will be rendered if goodsCount > 0  */}
        {
          this.state.order.goodsCount
          ? <Hr />
          : null
        }
      </View>
    );
  }

  render () {
    if (this.state.order === null) {
      return null;
    }

    const {order} = this.state;
    const orderId = this.props.navigation.state.params.orderId;
    console.log(order);

    return (
      <ScrollView
        style={styles.containerWrapper}
        contentContainerStyle={styles.container}
        refreshControl={
          <RefreshControl
            refreshing={this.state.isLoading}
            onRefresh={this.getOrder}
          />
        }
      >
        {/* <Text style={styles.id}>№{orderId}</Text> */}
        {
          order.statusText
          ? <Text
              style={[
                styles.statusText,
                {
                  backgroundColor: order.statusText !== ''
                    ? order.statusColor
                    : 'white'
                }
              ]}>
              {order.statusText}
            </Text>
          : null
        }

        {/*  */}
        {this.renderDeliveryInfo('Когда забрать', order.dataFrom)}
        {/*  */}
        {this.renderDeliveryInfo('Когда привезти', order.dataTo)}


        {this.renderCommentSection()}
        {this.renderGoodsSection()}
        {/* {this.renderPromocodeSection()} */}
        {/* {this.renderDeliveryPriceSection()} */}
        {this.renderConclusionSection()}
        {/* {
          order.discount
          ? <Text style={styles.summary}>Скидка: {order.discount} ₽</Text>
          : null
        } */}

        {
          order.goodsCount
          ? <View
              style={{
                width:          '100%',
                flexDirection:  'row',
                justifyContent: 'flex-end',
                marginTop:      10
              }}
            >
              <Text
                style={[styles.summaryImportant,
                  {
                    flexBasis: '28%',
                  }]}
              >
                Итого:
              </Text>
              <Text
                style={[styles.summaryImportant,
                  {
                    flexBasis: '20%',
                    textAlign: 'right'
                  }]}
              >
                  {`${Math.round(order.totalPrice)} ₽`}
              </Text>
            </View>
          : null
        }
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  containerWrapper: {
    backgroundColor: color.white
  },
  conclusionText: {
    // fontFamily: 'FSElliotPro-Bold'
    fontWeight: 'bold',
    fontSize:   16,
    marginTop:  4,
    lineHeight: 16
  },
  container: {
    paddingLeft:   20,
    paddingRight:  20,
    paddingTop:    15,
    paddingBottom: 15,
    alignItems:    'flex-start',
  },
  id: {
    fontSize:   18,
    fontWeight: 'bold'
  },
  statusText: {
    padding:         5,
    paddingLeft:     30,
    paddingRight:    30,
    textAlign:       'center',
    backgroundColor: 'rgb(247, 168, 218)',
    color:           color.white,
    marginTop:       4
    // alignSelf:       'flex-start'
    // flexBasis:       '40%'
  },
  deliveryHead: {
    // flexDirection:  'row',
    // justifyContent: 'space-between',
    width:     '100%',
    marginTop: 15
  },
  headTitle: {
    marginTop:  0,
    fontWeight: 'bold'
  },
  title: {
    marginTop: 15,
    fontSize:  16,
    color:     '#222022'
  },
  date: {
    fontSize: 12,
    color:    '#414141',
  },
  time: {
    fontSize: 11,
    color:    '#7E7D7E',
  },
  address: {
    fontSize:  15,
    color:     '#5C5C5C',
    marginTop: 10
  },
  addressComment: {
    fontSize:  14,
    color:     '#5C5C5C',
    marginTop: 10
  },
  textInfoContainer: {
    marginTop: 10
  },
  textInfo: {
    fontSize:  16,
    color:     '#5C5C5C',
    marginTop: 10
  },
  summary: {
    // alignSelf: 'flex-end',
    fontSize:  17,
    color:     color.black,
    marginTop: 10
  },
  summaryImportant: {
    fontSize:   18,
    fontWeight: 'bold',
  },
  hr: {
    marginTop:       15,
    height:          StyleSheet.hairlineWidth,
    backgroundColor: '#bbbbbb',
    width:           '100%'
  }
});

Order.propTypes = {
  navigation: PropTypes.object.isRequired,
  auth:       PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(mapStateToProps)(Order);
