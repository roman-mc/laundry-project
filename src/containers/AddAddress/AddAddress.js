import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  TouchableWithoutFeedback,
  Keyboard,
  ScrollView,
  Platform
} from 'react-native';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import {addAddress, updateAddress} from '../../api';
import {getAddresses} from '../../store/actions';

import Button from '../../components/Button';
import AddressInputs from '../../components/AddressInputs';

class AddAddress extends Component {
  constructor (props) {
    super(props);
    const address = props.navigation.state.params && props.navigation.state.params.address
      ? props.navigation.state.params.address
      : {};

    this.state = {
      address:   address.address || '',
      kvartira:  address['кв/офис'] || '',
      podezd:    address['подъезд'] || '',
      etaj:      address['этаж'] || '',
      domofon:   address['домофон'] || '',
      comment:   address.comment || '',
      addressId: address.id || null,
      isLoading: false
    };

    this.onAddressPress = this.onAddressPress.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

    this.onChangeAddress = this.onChange.bind(this, 'address');
    this.onChangeKvartira = this.onChange.bind(this, 'kvartira');
    this.onChangePodezd = this.onChange.bind(this, 'podezd');
    this.onChangeEtaj = this.onChange.bind(this, 'etaj');
    this.onChangeDomofon = this.onChange.bind(this, 'domofon');
    this.onChangeComment = this.onChange.bind(this, 'comment');
  }

  onAddressPress () {
    this.props.navigation.navigate(
      'AutoSuggestAddressPicker',
      {
        onAddressPick: this.onChangeAddress
      }
    );
  }

  onSubmit () {
    this.setState({isLoading: true});
    const address = {
      address: this.state.address,
      details: {
        'этаж':    this.state.etaj,
        'кв/офис': this.state.kvartira,
        'подъезд': this.state.podezd,
        'домофон': this.state.domofon,
      },
      comment: this.state.comment
    };

    if (this.state.addressId !== null) {
      updateAddress(address, this.state.addressId, this.props.auth.token)
        .then(() => {
          this.props.getAddresses();
          this.props.navigation.goBack();
        })
        .catch((e) => {
          this.setState({isLoading: false});
        });
    }
    else {
      addAddress(address, this.props.auth.token)
        .then(() => {
          this.props.getAddresses();
          this.props.navigation.goBack();
        })
        .catch((e) => {
          this.setState({isLoading: false});
        });
    }
  }

  onChange (property, value) {
    this.setState({
      [property]: value
    });
  }

  render () {
    const {
      address,
      kvartira,
      podezd,
      etaj,
      domofon,
      comment
    } = this.state;

    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View style={styles.container}>
          <AddressInputs
            address={address}
            kvartira={kvartira}
            podezd={podezd}
            etaj={etaj}
            domofon={domofon}
            comment={comment}
            onChangeKvartira={this.onChangeKvartira}
            onChangePodezd={this.onChangePodezd}
            onChangeEtaj={this.onChangeEtaj}
            onChangeDomofon={this.onChangeDomofon}
            onChangeComment={this.onChangeComment}
            onAddressPress={this.onAddressPress}
          />

          <ScrollView scrollEnabled={false}
            contentContainerStyle={styles.bottomContent}
            keyboardShouldPersistTaps='handled'
          >
            <Button
              onPress={this.onSubmit}
              style={{
                marginBottom: 10
              }}
              isDisabled={this.state.address === ''}

            >
              {this.state.addressId === null ? 'Добавить' : 'Сохранить'}
            </Button>
            {Platform.OS === 'ios'
              ? <KeyboardSpacer />
              : null
            }
        </ScrollView>
        </View>
      </TouchableWithoutFeedback>
    );
  }

}

const styles = StyleSheet.create({
  container: {
    flex:           1,
    alignItems:     'center',
    justifyContent: 'space-between'
  },
  bottomContent: {
    justifyContent: 'flex-end',
    alignItems:     'flex-start',
    flex:           1,
    width:          '100%'
  }
});

AddAddress.propTypes = {
  navigation:   PropTypes.object.isRequired,
  auth:         PropTypes.object.isRequired,
  getAddresses: PropTypes.func.isRequired
};
const mapStateToProps = state => ({
  auth: state.auth
});

const mapDispatchToProps = {
  getAddresses
};

export default connect(mapStateToProps, mapDispatchToProps)(AddAddress);
