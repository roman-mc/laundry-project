import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  ScrollView,
  Platform,
  Text,
  Image
} from 'react-native';
import PropTypes from 'prop-types';
import Button from 'apsl-react-native-button';
import {connect} from 'react-redux';
import update from 'immutability-helper';
import KeyboardSpacer from 'react-native-keyboard-spacer';

import {validate as validateEmail} from 'email-validator';

import {sendProfileInfo} from '../../store/actions';

import Input from '../../components/Input';
import {width, color, buttonDefault, button} from '../../styles';

class ProfileEditOrSkip extends Component {
  constructor (props) {
    super(props);

    this.state = {
      name:  props.auth.name || '',
      email: props.auth.email || ''
    };

    this.sendProfileInfo = this.sendProfileInfo.bind(this);
    this.successNavigate = this.successNavigate.bind(this);
  }

  // componentDidUpdate (prevProps) {
  //   if (prevProps.auth.isLoading && this.props.auth.isLoading === false && this.props.auth.errors.length === 0) {
  //     // this.props.navigation.goBack();
  //   }
  // }

  sendProfileInfo () {
    const data = {
      name:  this.state.name,
      email: this.state.email
    };

    this.props.sendProfileInfo(data)
      .then(() => {
        this.successNavigate();
      })
      .catch(console.error);
  }

  successNavigate () {
    this.props.navigation.navigate('OrderInfo');
  }

  render () {
    const isProfileEditing = this.state.name.length !== 0 || this.state.email.length !== 0
      && (this.state.name !== this.props.auth.name || this.state.email !== this.props.auth.name);

    const validEmail = this.state.email.length === 0 || validateEmail(this.state.email);
    const emailInputStyle = validEmail === false ? styles.inputError : null;

    const buttonStyle = !isProfileEditing ? styles.buttonDefaultContainer : null;
    const buttonTextStyle = !isProfileEditing ? styles.buttonDefaultText : null;
    // TODO: sendProfileInfo
    return (
      <View style={styles.container}>
        <View style={styles.contentTop}>
          {/* Question time and date START */}
          <Text style={styles.title}>
            Контактные данные
          </Text>

          <Input
            style={[styles.input, styles.input__first]}
            onChangeText={(name) => this.setState({name})}
            value={this.state.name}
            placeholder='Ваше имя'
            autoCapitalize='words'
            autocomplete={false}
          />
          {
            this.props.auth.email
            ? null
            : <Input
                style={[styles.input, emailInputStyle]}
                keyboardType='email-address'
                onChangeText={(email) => this.setState({email})}
                value={this.state.email}
                placeholder='E-mail'
              />

          }
          <Text style={{marginTop: 30, fontSize: 17, textAlign: 'center', width: width.bigContainer}}>
            Для получения электронных чеков и уведомлений о статусе, составе и стоимости заказов
          </Text>
        </View>

        <ScrollView scrollEnabled={false}
          contentContainerStyle={styles.bottomContent}
          keyboardShouldPersistTaps='handled'
        >
            <Button
              style={[styles.button, buttonStyle]}
              textStyle={[styles.buttonText, buttonTextStyle]}
              onPress={this.sendProfileInfo}
              isDisabled={!isProfileEditing || this.props.auth.isLoading || !validEmail}
            >
              Сохранить
            </Button>
            {
              this.props.auth.email
                ? <Button
                    style={styles.buttonDefaultContainer}
                    textStyle={styles.buttonDefaultText}
                    onPress={this.successNavigate}
                  >
                    Пропустить
                  </Button>
                : null
            }
            {Platform.OS === 'ios'
              ? <KeyboardSpacer />
              : null
            }
        </ScrollView>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  input: {
    paddingBottom: 5,
    marginTop:     2,
    marginBottom:  0,
    fontSize:      16
  },
  container: {
    flex:           1,
    alignItems:     'center',
    justifyContent: 'space-between',
    width:          width.full
  },
  contentTop: {
    width:      '100%',
    alignItems: 'center'
  },
  title: {
    fontSize:  20,
    marginTop: 10
  },
  input__first: {
    // marginTop: 5
  },
  button: {
    width:        width.bigContainer,
    alignSelf:    'center',
    borderWidth:  0,
    padding:      10,
    borderRadius: 5,
    ...button.container
  },
  buttonText: {
    ...button.text
  },
  inputError: {
    borderBottomColor: color.error
  },
  bottomContent: {
    justifyContent: 'flex-end',
    alignItems:     'flex-start',
    flex:           1,
    width:          '100%'
  },
  buttonDefaultContainer: {
    ...buttonDefault.container
  },
  buttonDefaultText: {
    ...buttonDefault.text
  }
});

// TODO: propTypes
ProfileEditOrSkip.propTypes = {
  auth:            PropTypes.object.isRequired,
  cards:           PropTypes.object.isRequired,
  navigation:      PropTypes.object.isRequired,
  sendProfileInfo: PropTypes.func.isRequired,
};

ProfileEditOrSkip.defaultProps = {
  image: '../../icons/delivery_icon.png'
};

const mapStateToProps = (state, ownProps) => ({
  auth:       state.auth,
  cards:      state.cards,
  navigation: ownProps.navigation
});

const mapDispatchToProps = {
  sendProfileInfo
};

export default connect(mapStateToProps, mapDispatchToProps)(ProfileEditOrSkip);
