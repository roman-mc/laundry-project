import React, {Component} from 'react';
import {
  FlatList,
  StyleSheet,
  View,
  Switch,
  TouchableOpacity,
  Alert,
  Text,
  Image,
  ScrollView
} from 'react-native';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {NavigationActions} from 'react-navigation';
import {
  deleteAddress
} from '../../api';
import {
  getAddresses
} from '../../store/actions';
import Button from '../../components/Button';
import {color, width} from '../../styles';

class Addresses extends Component {
  constructor (props) {
    super(props);

    this.deleteAddress = this.deleteAddress.bind(this);
    this.navigateToAddAddress = this.navigateToAddAddress.bind(this);
    this.renderAddress = this.renderAddress.bind(this);
  }

  componentDidMount () {
    this.props.getAddresses();
  }


  /**
   * @param  {number} addressId
   */
  deleteAddress (addressId) {
    Alert.alert(
      'Удалить адрес ?',
      undefined,
      [
        {text: 'Нет', onPress: () => undefined},
        {text:    'Да', style:   'destructive', onPress: () => {
          deleteAddress(addressId, this.props.auth.token)
            .then(() => {
              this.props.getAddresses();
            })
            .catch((e) => {
              console.log('ERROR ADDRESSES DELETE AADRESS');
              console.log(e);
            });
        }}
      ]
      );
  }

  navigateToAddAddress (address) {
    const navigateAction = NavigationActions.navigate({
      // routeName: 'AutoSuggestAddressPicker',
      routeName: 'AddAddress',
      params:    address ? {address} : undefined,
    });

    this.props.navigation.dispatch(navigateAction);
  }

  renderAddress ({item, index}) {
    return (
      <TouchableOpacity
        key={item.id}
        onPress={() => {
          this.navigateToAddAddress(item);
          // this.props.navigation.navigate('AddAddress', {address: item});
        }}
        style={[
          styles.addressContainer,
          index === 0 ? {borderTopWidth: 1} : null
        ]}
      >
          <Text style={{fontSize: 17, flexBasis: '70%'}} numberOfLines={1}>
            {item.address}
          </Text>
          {/* <TouchableOpacity style={{padding: 10}} onPress={() => this.deleteAddress()}> */}
          <TouchableOpacity style={{padding: 10}} onPress={() => this.deleteAddress(item.id)}>
            <Image source={require('../../icons/delete_icon.png')} />
          </TouchableOpacity>
      </TouchableOpacity>
    );
  }

  addressKeyExtractor (item) {
    return item.id;
  }

  render () {
    console.log(this.props);
    console.log(this.props.navigation);
    return (
      <View style={styles.container}>
        <Button
          onPress={() =>
            this.navigateToAddAddress()
          }
          style={styles.buttonAdd}
        >
          <Text style={styles.buttonAddText}>Добавить адрес</Text>
        </Button>
        {/* <ScrollView horizontal>
          <Text>
            {JSON.stringify({error: this.props.error, loading: this.props.isLoading})}
          </Text>
        </ScrollView> */}
        <FlatList
          style={styles.list}
          data={this.props.addresses}
          // extraData={this.props.addresses}
          renderItem={this.renderAddress}
          keyExtractor={this.addressKeyExtractor}
          refreshing={this.props.isLoading}
          onRefresh={this.props.getAddresses}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  addressContainer: {
    backgroundColor:   color.white,
    borderBottomWidth: 1,
    borderColor:       '#d5d5d5',
    flexDirection:     'row',
    paddingLeft:       20,
    paddingVertical:   10,
    paddingRight:      10,
    justifyContent:    'space-between',
    alignItems:        'center'
  },
  list: {
    marginTop: 20
  },
  listItemContainer: {
    backgroundColor:   color.white,
    borderBottomWidth: 1,
    borderColor:       '#d5d5d5',
    flexDirection:     'row',
    paddingLeft:       20,
    paddingVertical:   10,
    paddingRight:      10,
    justifyContent:    'space-between',
    alignItems:        'center'
  },
  cardRightContent: {
    flexDirection:  'row',
    alignItems:     'center',
    justifyContent: 'flex-end'
  },
  header: {
    flexDirection:   'row',
    justifyContent:  'space-between',
    alignItems:      'center',
    backgroundColor: color.white,
    paddingTop:      20,
    paddingBottom:   20,
    paddingLeft:     15,
    paddingRight:    15,
  },
  profileName: {
    fontSize: 19,
    color:    '#222022'
  },
  profileEmail: {
    fontSize:  17,
    color:     '#5C5C5C',
    marginTop: 10
  },
  buttonAdd: {
    width:           width.full - 30,
    marginLeft:      15,
    marginRight:     15,
    marginTop:       20,
    marginBottom:    0,
    height:          'auto',
    paddingVertical: 10
  },
  buttonAddText: {
    fontSize: 15,
    color:    color.white,
  }
});

Addresses.propTypes = {
  navigation:   PropTypes.object.isRequired,
  auth:         PropTypes.object.isRequired,
  getAddresses: PropTypes.func.isRequired,
  isLoading:    PropTypes.bool.isRequired,
  addresses:    PropTypes.array.isRequired,
};

const mapStateToProps = state => ({
  addresses: state.addresses.addresses,
  isLoading: state.addresses.isLoading,
  error:     state.addresses.error,
  auth:      state.auth
});

const mapDispatchToProps = {
  getAddresses,
};

export default connect(mapStateToProps, mapDispatchToProps)(Addresses);
