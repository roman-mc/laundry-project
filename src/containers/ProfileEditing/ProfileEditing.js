import React, {Component} from 'react';
import {
    View,
    StyleSheet,
    ScrollView,
    Platform,
    Text,
    Keyboard
} from 'react-native';
import PropTypes from 'prop-types';
import Button from 'apsl-react-native-button';
import {connect} from 'react-redux';
import update from 'immutability-helper';
import KeyboardSpacer from 'react-native-keyboard-spacer';

import {validate as validateEmail} from 'email-validator';

import {sendProfileInfo} from '../../store/actions';

import Input from '../../components/Input';
import {width, color, buttonDefault, button} from '../../styles';

class ProfileEditing extends Component {
  constructor (props) {
    super(props);

    this.state = {
      name:               props.auth.name || '',
      email:              props.auth.email || '',
      saveButtonVisible:  false,
      successTextVisible: false
    };

    this.initialProfileData = {
      name:  props.auth.name || '',
      email: props.auth.email || '',
    };

    this.successTextTimeout = null;

    this.sendProfileInfo = this.sendProfileInfo.bind(this);
  }

  componentWillUnmount () {
    clearTimeout(this.successTextTimeout);
  }

  sendProfileInfo () {
    const data = {
      name:  this.state.name,
      email: this.state.email
    };

    this.props.sendProfileInfo(data)
      .then(() => {
        this.setState({saveButtonVisible: false, successTextVisible: true});
        this.initialProfileData = data;

        this.successTextTimeout = setTimeout(() => {
          this.setState({successTextVisible: false});
        }, 2000);
      });

    Keyboard.dismiss();
  }
  render () {
    const validEmail = this.state.email.length !== 0 && validateEmail(this.state.email);
    const emailInputStyle = validEmail === false ? styles.inputError : null;

    // TODO: sendProfileInfo
    return (
      <View style={styles.container}>
        <View style={styles.contentTop}>
          {/* Question time and date START */}
          <Input
            style={styles.input}
            onChangeText={(name) => {
              const saveButtonVisible = this.initialProfileData.name !== name;
              this.setState({name, saveButtonVisible});
            }}
            value={this.state.name}
            placeholder='Ваше имя'
            autoCapitalize='words'
          />
          <Input
            style={[styles.input, emailInputStyle]}
            keyboardType='email-address'
            onChangeText={(email) => {
              const saveButtonVisible = this.initialProfileData.email !== email;
              this.setState({email, saveButtonVisible});
            }}
            value={this.state.email}
            placeholder='E-mail'
          />
        </View>

        <ScrollView scrollEnabled={false}
          contentContainerStyle={styles.bottomContent}
          keyboardShouldPersistTaps='handled'
          >
            {
              this.state.successTextVisible
                ? <Text
                    style={[
                      styles.messageSaved,
                      {
                        marginBottom: this.state.saveButtonVisible ? 8 : 20
                      }
                    ]}
                  >
                    Изменения сохранены
                  </Text>
                : null
            }
            {this.state.saveButtonVisible
              ? <Button
                  style={styles.button}
                  textStyle={styles.buttonText}
                  onPress={this.sendProfileInfo}
                  isDisabled={this.props.auth.isLoading || !validEmail}
                >
                  Сохранить
                </Button>
              : null
            }
            {Platform.OS === 'ios'
              ? <KeyboardSpacer />
              : null
            }
        </ScrollView>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  input: {
    paddingBottom: 5,
    marginTop:     2,
    marginBottom:  0
  },
  container: {
    flex:            1,
    alignItems:      'center',
    justifyContent:  'space-between',
    width:           width.full,
    backgroundColor: 'white'
  },
  contentTop: {
    width:      '100%',
    alignItems: 'center'
  },
  button: {
    width:        width.bigContainer,
    alignSelf:    'center',
    borderWidth:  0,
    padding:      10,
    borderRadius: 5,
    ...button.container
  },
  buttonText: {
    ...button.text
  },
  inputError: {
    borderBottomColor: color.error
  },
  bottomContent: {
    justifyContent: 'flex-end',
    alignItems:     'center',
    flex:           1,
    width:          '100%'
  },
  messageSaved: {
    color:        color.airo,
    fontSize:     15,
    marginBottom: 8
  }
});

// TODO: propTypes
ProfileEditing.propTypes = {
  auth:            PropTypes.object.isRequired,
  navigation:      PropTypes.object.isRequired,
  sendProfileInfo: PropTypes.func.isRequired,
};

const mapStateToProps = (state, ownProps) => ({
  auth:       state.auth,
  navigation: ownProps.navigation
});

const mapDispatchToProps = {
  sendProfileInfo
};

export default connect(mapStateToProps, mapDispatchToProps)(ProfileEditing);
