import {Platform} from 'react-native';

export const serverURL = 'http://test.getairo.ru/v1/';
// export const serverURL = 'https://getairo.ru/v1/';

export const appUrlInStore = Platform.select({
  ios: 'https://itunes.apple.com/ru/app/airo/id1312852936',
  android: 'https://play.google.com/store/apps/details?id=com.airoclient'
});
