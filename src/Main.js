import React, {Component} from 'react';
import {
  View,
  Platform,
  StatusBar,
  BackHandler,
  Modal,
  Linking
} from 'react-native';
import {connect} from 'react-redux';
import {addNavigationHelpers, NavigationActions} from 'react-navigation';
import PropTypes from 'prop-types';
import SplashScreen from 'react-native-splash-screen';

import analytics from './modules/analytics';
import crashlytics from './modules/crashlytics';
import {
  appUrlInStore
} from './config';

import {
  validateToken, resetNavigation, navigate,
  routePicked,
  getCards,
  getAddresses,
  hideModal, showModal
} from './store/actions';
import {getLatestAppVersion} from './api';
import Router from './router';
import ModalUpdateApp from './components/ModalUpdateApp';
const DeviceInfo = require('react-native-device-info');

class Main extends Component {
  componentDidMount () {
    setTimeout(SplashScreen.hide, 700);

    BackHandler.addEventListener('hardwareBackPress', () => {
      // Can not go back/exit app when modal is open
      if (this.props.modal.visible) {
        return;
      }

      this.props.dispatch(NavigationActions.back({
        key: null
      }));

      return true;
    });
  }

  render () {
    return (
      <View style={{
        flex:      1,
        width:     '100%', // TODO: remove it ?
        marginTop: 0
      }}>
        <StatusBar
          barStyle={Platform.OS === 'ios' ? 'dark-content' : 'light-content'}
          // fucking android
          backgroundColor={'black'}
        />
        <Router
          navigation={
            addNavigationHelpers({
              dispatch: this.props.dispatch,
              state:    this.props.nav
            })
          }
        />
        <Modal
          // visible={this.props.modal.visible}
          visible={this.props.modal.visible}
          animationType={'fade'}
          transparent
          onRequestClose={() => this.props.dispatch(hideModal())}
        >
          {this.props.modal.component}
        </Modal>
      </View>
    );
  }
}

Main.propTypes = {
  nav:             PropTypes.object.isRequired,
  dispatch:        PropTypes.func.isRequired,
  auth:            PropTypes.object.isRequired,
  modal:           PropTypes.object.isRequired,
  storeRehydrated: PropTypes.bool.isRequired,
  isRoutePicked:   PropTypes.bool.isRequired
};

const mapStateToProps = (state) => ({
  nav:             state.nav,
  auth:            state.auth,
  modal:           state.modal,
  storeRehydrated: state.app.storeRehydrated,
  isRoutePicked:   state.app.routePicked
});

export default connect(mapStateToProps)(Main);
