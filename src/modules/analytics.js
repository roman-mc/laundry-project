import firebase from 'react-native-firebase';

const analytics = firebase.analytics();

export default analytics;
