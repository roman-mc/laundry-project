const Fabric = require('react-native-fabric');
const { Crashlytics } = Fabric;

// analytics.setAnalyticsCollectionEnabled(true);
Crashlytics.setUserName('test_user_name_roman');
Crashlytics.setUserEmail('laconty.r@gmail.com');

export default Crashlytics;
