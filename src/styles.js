import {Dimensions, Platform} from 'react-native';

const {height: y, width: x} = Dimensions.get('window');

export const color = {
  airo:            '#38A4F4',
  blue:            '#345189',
  lightBlue:       '#0094c2',
  iosLinks:        '#0076FF',
  lightPrettyBlue: '#00adf5', // TODO: use it 'text link color'
  error:           'rgb(209, 45, 45)',
  disabled:        '#b9b9b9',
  white:           'rgb(255, 255, 255)',
  black:           '#0F0F0F',
  black1:          '#2B2B2B',
  black2:          '#2B2B2B',
  black3:          '#474747',
  black4:          '#636363',
  black5:          '#7F7F7F',
  black6:          '#9C9C9C',
  black7:          '#B8B8B8',
  black8:          '#D4D4D4',
  black9:          '#E9E9E9',
  black10:         '#F0F0F0',
  gold:            '#ffcc00',
  inputBorders:    '#4e4e4e'
};

export const width = {
  full:           x,
  half:           x / 2,
  bigContainer:   x * 0.92,
  smallContainer: x * 0.8
};

export const buttonDisabled = {
  text: {
    color: color.disabled
  },
  container: {
    backgroundColor: color.white,
    borderColor:     color.disabled,
    borderWidth:     1
  }
};

export const button = {
  text: {
    color: color.white
  },
  container: {
    borderWidth:     0,
    backgroundColor: color.airo

  }
};

export const buttonDefault = {
  text: {
    color: '#383838'
  },
  container: {
    borderWidth:     1,
    borderColor:     '#383838',
    backgroundColor: color.white

  }
};

export const keyboardVerticalOffset = Platform.OS === 'ios' ? 64 : 0;
