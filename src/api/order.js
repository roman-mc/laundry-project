import axios from 'axios';

import {
  convertFromServerDate,
  getShortDate,
  getColorByStatusType,
  months,
  shortWeekDays
} from '../utils';
import {serverURL} from '../config';

// This is static key for timeslots request, who the fuck invent this
const key = 'o9AmnsNnXaGco5gYz7X6ZugfA0FnTdDq';
const notDefined = '';


const lowerCaseMonths = months.map(s => s.toLowerCase());
const firstUpperCaseShortWeekDays = shortWeekDays.map(s => s.slice(0, 1).toUpperCase() + s.slice(1));

/**
 * @param  {Date} date
 * @return {string} return `22.10.2017`
 */
export const _getShortDate = date => {
  const day = date.getDate();
  const month = date.getMonth();
  const year = date.getFullYear();
  const weekyDay = date.getDay();

  return `${firstUpperCaseShortWeekDays[weekyDay]}, ${day} ${lowerCaseMonths[month]} ${year}`;
};

/**
 * This function is used for
 *  1. Initial request timeslots with no time selected
 *  2. Afterward requests.
 *
 * @param  {Object} data
 *
 * @param {Object} data.collect
 * @param {String} data.collect.date
 * @param {String} data.collect.time
 * @param {String} data.collect.latLong
 * @param {Object} data.delivery
 * @param {String} data.delivery.date
 * @param {String} data.delivery.time
 * @param {String} data.delivery.latLong
 * @param {Boolean} data.delivery.sameAddress
 *
 * @return {Promise<response, error>}
 */
export const selectTimeslots = (data) => axios.post(`${serverURL}timeslot?key=${key}`, data);



/**
 * Return promocode object {success: number, message, code, discount, count}
 * @param  {String}  promocode
 * @param  {String}  token
 * @return {Promise<promocode, promocode>}
 */
export const checkPromocode = async (promocode, token) => {
  const response = await axios.get(`${serverURL}order/validate-promocode?token=${token}&promoCode=${promocode}`);

  if (response.data.success === 0) {
    throw response.data;
  }

  return response.data;
};

export const sendOrder = async (data, token) => {
  const config = {
    method: 'post',
    data,
    url:    `${serverURL}order/create?token=${token}`
  };

  const response = await axios.request(config);


  return response.data;
};

/**
 * @param  {String} string
 * @return {Date}
 */
const _convertFromServerDate = (string) => {
  const [year, month, day] = string.split('-');

  return new Date(year, month - 1, day);
};

/**
 * Allwoed options: [limit=number, skip=number]
 * @param {Array} queryOptions
 * @param  {string}  token
 * @return {Promise<Array<Object>>}
 */
export const getOrders = async (queryOptions, token) => {

  const response = await axios.get(`${serverURL}orders?token=${token}&${queryOptions.join('&')}`);
  const {data} = response;
  const {orders: serverOrders = [], totalCount} = data;

  const orders = serverOrders.map(order => ({
    id:       order.order_id,
    dataFrom: {
      date: getShortDate(_convertFromServerDate(order.order_date_from)),
      time: order.order_time_from,
    },
    dataTo: {
      date: getShortDate(_convertFromServerDate(order.order_date_to)),
      time: order.order_time_to,
    },
    goodsCount:  Number(order.goodsCount),
    totalPrice:  order.order_result_price,
    statusText:  order.status_text,
    statusColor: getColorByStatusType(order.status_type),
  }));

  return {orders, totalCount: Number(totalCount)};
};

export const getOrder = async (id, token) => {
  const response = await axios.get(`${serverURL}orders/${id}?token=${token}`);
  const {data} = response;

  const goods = [];
  let goodsCount = 0;
  for (const i in data.goods) {
    const good = {
      name:     data.goods[i].name,
      quantity: data.goods[i].qty,
      unit:     data.goods[i].unit,
      price:    data.goods[i].price,
      id:       data.goods[i].id
    };
    goodsCount += good.quantity;

    goods.push(good);
  }

  const addressDetailsFrom = JSON.parse(data.address_from[0].details);
  const addressDetailsTo = JSON.parse(data.address_to[0].details);

  const order = {
    id:       data.order_id,
    dataFrom: {
      date:           _getShortDate(_convertFromServerDate(data.order_date_from)),
      time:           data.order_time_from,
      address:        data.address_from[0].address || 'Адрес не указан',
      addressComment: data.address_from[0].comment || '',
      kvartira:       addressDetailsFrom['кв/офис'],
      podezd:         addressDetailsFrom['подъезд'],
      etaj:           addressDetailsFrom['этаж'],
      domofon:        addressDetailsFrom['домофон'],
    },
    dataTo: {
      date:           _getShortDate(_convertFromServerDate(data.order_date_to)),
      time:           data.order_time_to,
      address:        data.address_to[0].address || 'Адрес не указан',
      addressComment: data.address_to[0].comment || '',
      kvartira:       addressDetailsTo["кв/офис"],
      podezd:         addressDetailsTo['подъезд'],
      etaj:           addressDetailsTo['этаж'],
      domofon:        addressDetailsTo['домофон'],
    },
    statusText:    data.status_text,
    statusColor:   getColorByStatusType(data.status_type),
    comment:       data.order_comment,
    totalPrice:    data.order_result_price,
    promocode:     data.client_promo_code,
    discount:      data.discount,
    deliveryPrice: data.delivery_price,
    goods,
    goodsCount
  };

  return order;
};
