import axios from 'axios';

import {serverURL} from '../config';

const city = 'Москва, ';
const cityRegExp = new RegExp(city);

export const requestSMS = phone => {
  const data = {
    phone
  };

  return axios.post(`${serverURL}auth/request-sms`, data);
};

/**
 * @param {String} phone
 * @param  {String}  token
 * @return {Promise<{name: string?, email: string?, token: string, phone: string}, err>}
 */
export const sendSmsCode = async (phone, code) => {
  const data = {
    phone,
    code
  };

  const response = await axios.post(`${serverURL}auth/login`, data);

  return response.data;
};

/**
 * @param  {String}  token
 * @return {Promise<{name: string?, email: string?, token: string, phone: string}, err>}
 */
export const validateToken = async token => {

  const response = await axios.get(`${serverURL}auth/validate?token=${token}`);

  return response.data;
};

/**
 * @param  {Object}  data
 * @param  {String}  data.name
 * @param  {String}  data.email
 * @return {Promise<{success: number}, err>}
 */
export const sendProfileInfo = (data, token) =>
  axios.put(`${serverURL}auth/profile?token=${token}`, data);
