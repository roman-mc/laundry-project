// @flow
import axios from 'axios';

import {serverURL} from '../config';

const city = 'Москва, ';
const cityRegExp = new RegExp(city);

/**
 * @param  {string}  token
 * @return {Promise<Array<string>, err>}
 */
export const getAddresses = async (address:string, token:string) => {
  const response = await axios.get(`${serverURL}addresses?token=${token}&text=${address}`);
  const {data} = response;

  return data;
};

/**
 * @param  {string}  token
 * @return {Promise<Array<{address}>}
 */
export const getLastUsedAddress = async (token: string) => {
  const response = await axios.get(`${serverURL}addresses/last-used?token=${token}`);

  const {data} = response;

  return data;
};

/**
 * @param  {Object}  address
 * @param  {string}  address.address
 * @param  {Object}  address.details 'этаж', 'подъезд', 'кв/офис', 'домофон'
 * @param  {string}  token
 * @return {Promise}
 */
export const addAddress = async (address: Object, token: string) => {
  const response = await axios.post(`${serverURL}addresses?token=${token}`, address);

  return response.data;
};

export const updateAddress = async (address: Object, addressId: number, token: string) => {
  const response = await axios.patch(`${serverURL}addresses?token=${token}&id=${addressId}`, address);

  return response.data;
};

export const deleteAddress = async (addressId: number, token: string):Promise<Object> => {
  const response = await axios.delete(`${serverURL}addresses/${addressId}?token=${token}`);

  return response.data;
};
