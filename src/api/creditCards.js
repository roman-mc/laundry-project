import axios from 'axios';
import {serverURL} from '../config';

/**
 * @param  {string}  token
 * @return {Promise<Array<Object>>}
 */
export const getCards = async token => {
  const response = await axios.get(`${serverURL}client-cards?token=${token}`);

  return response.data.data;
};

/**
 * @param  {string} token
 * @return {Promise<string>}
 */
export const getBindingCardUrl = async token => {
  const response = await axios.post(`${serverURL}client-cards?token=${token}`);
  if (response.data.success !== true) {
    throw response.data;
  }

  return response.data.redirectUrl;
};

export const deleteCard = async (bindingHash, token) => {
  const response = await axios.delete(`${serverURL}client-cards/${bindingHash}?token=${token}`);

  if (response.data.success !== true) {
    throw response.data;
  }

  return response.data;
};

export const activateCard = async (bindingHash, token) => {
  const response = await axios.put(`${serverURL}client-cards/${bindingHash}/on?token=${token}`);

  if (response.data.success !== true) {
    throw response.data;
  }

  return response.data;
};

export const deactivateCard = async (bindingHash, token) => {
  const response = await axios.delete(`${serverURL}client-cards/${bindingHash}/off?token=${token}`);

  if (response.data.success !== true) {
    throw response.data;
  }

  return response.data;
};
