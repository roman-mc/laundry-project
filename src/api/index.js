export {
  suggestAddresses,
  getLatLongForAddresses
} from './maps';

export {
  selectTimeslots,
  sendOrder,
  checkPromocode,
  getOrders,
  getOrder
} from './order';

export {
  requestSMS,
  sendSmsCode,
  validateToken,
  sendProfileInfo
} from './auth';

export {
  addAddress,
  getAddresses,
  getLastUsedAddress,
  updateAddress,
  deleteAddress
} from './address';

export {
  getBindingCardUrl,
  getCards,
  deleteCard,
  activateCard,
  deactivateCard
} from './creditCards';

export {
  getLatestAppVersion
} from './versions';
