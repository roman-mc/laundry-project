import axios from 'axios';
import {
  Platform
} from 'react-native';
import {serverURL} from '../config';

/**
 * @return {Promise<string>} string like '1.2.3'
 */
export const getLatestAppVersion = async () => {
  const res = await axios.get(`${serverURL}mobile-versions/${Platform.OS}`);

  return JSON.parse(res.data);
};
