import axios from 'axios';
import {parseSuggestedAddresses} from '../utils';
// const boundedBy = '56.009657,37.9456611~55.48992699999999,37.3193289';
// const boundedBy = [[56.009657,37.9456611], [55.48992699999999,37.3193289]];
const mapsSuggestUrl = 'https://suggest-maps.yandex.ru/suggest-geo';
const mapsCoordsUrl = 'https://geocode-maps.yandex.ru/1.x';

const city = 'Москва, ';
/**
 * @param  {String} string
 * @return {Promise}
 */
export const suggestAddresses = async (string) => {
  const queryAddress = string.trim();
  const queryArray = [
    'lang=ru-RU',
    'search_type=all',
    'v=9',
    'fullpath=1',
    'format=json',
    'll=37.59,55.87',
    // 'spn=0.2,0.2',
    // 'bbox=37.31,55.57~37.83,55.91',
    'rspn=1',
    'results=12',
    'part=' + queryAddress
  ];
  // TODO: replace 'Россия, ' from each address
  const response = await axios.get(mapsSuggestUrl + '?' + queryArray.join('&'));
  // console.log(response.data);
  const {data} = response;
  const dataString = data.slice(data.indexOf('(') + 1, data.lastIndexOf(')'));
  const jsonedResponse = JSON.parse(dataString);

  const results = jsonedResponse.results;

  const suggested = results
    .filter(res => res.type === 'toponym')
    .map(result => result.text.replace('Россия, ', ''));

  return suggested;
};

/**
 * Gets latlong in format 'lat long' by address
 * @param  {String}  string address string without city
 * @return {Promise}
 */
export const getLatLong = async string => {
  const queryArray = [
    'format=json',
    'geocode=' + city + string,
  ];

  const response = await axios.get(mapsCoordsUrl + '?' + queryArray.join('&'));
  // Fucking yandex
  const mapObject = response.data.response.GeoObjectCollection.featureMember[0];
  // as it is promise, error handling there is outside in .catch() ;)
  if (mapObject === undefined) throw Error('Адрес не имеет координат!');
  const longLat = mapObject.GeoObject.Point.pos;
  const latLong = longLat.split(' ').reverse().join(' '); // Why this is so, who the fuck knows

  return latLong;
};

/**
 * @param  {...String}  addresses
 * @return {Promise<Array<string>>}
 */
export const getLatLongForAddresses = async (addresses) => {
  if (!addresses[0]) {
    return ['', ''];
  }
  
  const latLongs = Promise.all(addresses.map(getLatLong));

  return latLongs;
};
