import React, {Component} from 'react';
import {StackNavigator, DrawerNavigator, NavigationActions} from 'react-navigation';
import {
  Text,
  TouchableOpacity,
  Image,
  StyleSheet,
  Alert
} from 'react-native';

import {
  resetOrder,
  resetAuth
} from './store/actions';

import Login from './containers/Login';
import ProfileEditing from './containers/ProfileEditing';
import OrderInfo from './containers/OrderInfo';
import InsertCode from './containers/InsertCode';
import PickAddress from './containers/PickAddress';
import PickTime from './containers/PickTime';
import Blank from './components/Blank';
import ProfileEditOrSkip from './containers/ProfileEditOrSkip';
import AddCardOrSkip from './containers/AddCardOrSkip';

import Orders from './containers/Orders';
import Order from './containers/Order';
import Profile from './containers/Profile';
import Addresses from './containers/Addresses';
import Cards from './containers/Cards';
import AutoSuggestAddressPicker from './containers/AutoSuggestAddressPicker';
import AddAddress from './containers/AddAddress';
import AddCreditCard from './containers/AddCreditCard';
import DrawerContent from './components/DrawerContent';

const HamburgerMenu = props =>
  (<TouchableOpacity style={styles.sideButton} onPress={props.onPress}>
    <Image source={require('./icons/hamburger_menu_button.png')} />
  </TouchableOpacity>);

const Auth = StackNavigator({
  Login: {
    screen:            Login,
    navigationOptions: {
      title: 'Телефон'
    }
  },
  InsertCode: {
    screen:            InsertCode,
    navigationOptions: {
      title: 'Вход'
    }
  }
});

const MakeOrder = StackNavigator({
  ProfileEditOrSkip: {
    screen:            ProfileEditOrSkip,
    navigationOptions: {
      title:      'Профиль',
      headerLeft: null
    }
  },
  AddCardOrSkip: {
    screen:            AddCardOrSkip,
    navigationOptions: {
      title:      'Карта',
      headerLeft: null
    }
  },
  AddCreditCard: {
    screen:            AddCreditCard,
    navigationOptions: {
      title:           'Привязка карты',
      headerBackTitle: 'Назад'
    }
  },
  PickAddress: {
    screen:            PickAddress,
    navigationOptions: ({navigation}) => ({
      title:           'Выберите адрес',
      headerBackTitle: 'Адрес',
      headerLeft:      (
        <HamburgerMenu onPress={() => navigation.navigate('DrawerToggle')} />
      )
    })
  },
  PickTime: {
    screen:            PickTime,
    navigationOptions: ({navigation}) => ({
      title:           'Выберите время',
      headerBackTitle: 'Время',
      drawerLockMode:  'locked-closed'

    })
  },
  // AcceptOrder: {
  //   screen:            AcceptOrder,
  //   navigationOptions: ({navigation}) => ({
  //     title: 'Подтверждение заказа',
  //   })
  // },
  OrderInfo: {
    screen:            OrderInfo,
    navigationOptions: {
      title:           'Информация о заказе',
      headerLeft:      null,
      gesturesEnabled: false,
    }
  },

}, {
  initialRouteName: 'PickAddress'
});

const OrdersStack = StackNavigator({
  // TODO
  Orders: {
    screen:            Orders,
    navigationOptions: ({navigation}) => ({
      title:      'Заказы',
      headerLeft: (
        <HamburgerMenu onPress={() => navigation.navigate('DrawerToggle')} />
      ),
      headerRight: (
        <TouchableOpacity
          style={styles.sideButton}
          onPress={() => {
            navigation.dispatch(resetOrder());
            navigation.navigate('MakeOrder');
          }}
        >
          <Image source={require('./icons/plus.png')} />
        </TouchableOpacity>
      )
    })
  },
  Order: {
    screen:            Order,
    navigationOptions: ({navigation}) => ({
      title:          `Заказ №${navigation.state.params.orderId}`,
      drawerLockMode: 'locked-closed'
    })
  }
});

const ProfileStack = StackNavigator({
  // Profile: {
  //   screen:            Profile,
  //   navigationOptions: ({navigation}) => {
  //     return ({
  //       title:      'Профиль',
  //       headerLeft: (
  //         <HamburgerMenu onPress={() => navigation.navigate('DrawerToggle')} />
  //       ),
  //       headerBackTitle: 'Назад'
  //     });
  //   }
  // },
  ProfileEditing: {
    screen:            ProfileEditing,
    navigationOptions: ({navigation}) => ({
      title:      'Профиль',
      // drawerLockMode: 'locked-closed',
      headerLeft: (
        <HamburgerMenu onPress={() => navigation.navigate('DrawerToggle')} />
      ),
      headerRight: (
        <TouchableOpacity
          style={styles.sideButton}
          onPress={() => {
            Alert.alert(
              'Выход',
              'Действительно выйти?',
              [
                {
                  text:    'Нет', onPress: () => undefined
                },
                {
                  text:    'Да', onPress: () => {
                    navigation.dispatch(resetAuth());
                    navigation.navigate(
                      'Auth'
                    );
                  }
                }
              ]
            );
          }}
        >
          <Image source={require('./icons/exit.png')} />
        </TouchableOpacity>
      )

    })
  },
});

const AddressesStack = StackNavigator({
  Addresses: {
    screen:            Addresses,
    navigationOptions: ({navigation}) => ({
      title:      'Адреса',
      headerLeft: (
        <HamburgerMenu onPress={() => navigation.navigate('DrawerToggle')} />
      ),
      headerBackTitle: 'Назад'
    })
  },
  AutoSuggestAddressPicker: {
    screen:            AutoSuggestAddressPicker,
    navigationOptions: {
      title:          'Выберите адрес',
      drawerLockMode: 'locked-closed'
    }
  },
  AddAddress: {
    screen:            AddAddress,
    navigationOptions: {
      title:           'Укажите адрес',
      headerBackTitle: 'Назад',
      drawerLockMode:  'locked-closed'
    }
  },
});

const CardsStack = StackNavigator({
  Cards: {
    screen:            Cards,
    navigationOptions: ({navigation}) => ({
      title:      'Карты',
      headerLeft: (
        <HamburgerMenu onPress={() => navigation.navigate('DrawerToggle')} />
      ),
      headerBackTitle: 'Назад'
    })
  },
  AddCreditCard: {
    screen:            AddCreditCard,
    navigationOptions: {
      title:          'Добавление карты',
      drawerLockMode: 'locked-closed'
    }
  }

});

const BlankStack = StackNavigator({
  Blank: {
    screen: Blank,
    // navigationOptions: {
    //   header: null
    // }
  }
});

const Drawer = DrawerNavigator({
  Auth: {
    screen:            Auth,
    navigationOptions: {
      drawerLockMode: 'locked-closed'
    }
  },
  MakeOrder: {
    screen: MakeOrder
  },
  OrdersStack: {
    screen: OrdersStack,
  },
  ProfileStack: {
    screen: ProfileStack,
    // navigationOptions: {
    //   drawerLockMode: 'locked-closed'
    // }
  },
  AddressesStack: {
    screen: AddressesStack
  },
  CardsStack: {
    screen: CardsStack
  },
  BlankStack: {
    screen: BlankStack
  }
}, {
  contentComponent: DrawerContent
});

const styles = StyleSheet.create({
  sideButton: {
    padding: 15
  }
});

export default Drawer;
